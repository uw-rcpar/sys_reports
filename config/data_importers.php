<?php

return [

  /*
  |--------------------------------------------------------------------------
  | CSV Importer Settings
  |--------------------------------------------------------------------------
  |
  | RCPAR settings related to CSV imports.
  |
  */

  'csv_to_import_path' => env('CSV_TO_IMPORT_PATH', 'uploaded_csv/'),
  'imported_csv_path' => env('IMPORTED_CSV_PATH', 'uploaded_csv_processed/'),
];