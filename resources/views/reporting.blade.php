<!-- Stored in resources/views/dashboard.blade.php -->
@extends('layouts.dashboard')

@section('title', 'Reporting')

@section('content')

    <div class="row">
        <div class="col-xs-6 col-md-4">
            <div class="panel panel-default">
                <div class="panel-heading">Free Trial Orders: Last 7 Days</div>
                <div class="panel-body">

                    <table class="table table-striped task-table">
                        <!-- Table Headings -->
                        <thead>
                        <th>Day Created</th>
                        <th>Total Orders</th>
                        </thead>
                        <!-- Table Body -->
                        <tbody>
                        @foreach ($ft_daily_snapshot as $o)
                            <tr>
                                <td class="table-text">
                                    <div>{{ $o->create_day }}</div>
                                </td>
                                <td class="table-text">
                                    <div>{{ $o->total_count }}</div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>

                </div>
            </div>
        </div>

        <div class="col-xs-6 col-md-4">
            <div class="panel panel-default">
                <div class="panel-heading"># Free Trials Per User</div>
                <div class="panel-body">

                    <table class="table table-striped task-table">
                        <!-- Table Headings -->
                        <thead>
                        <th>Total Users</th>
                        <th>Free Trial Orders Each</th>
                        </thead>
                        <!-- Table Body -->
                        <tbody>
                        @foreach ($ft_users_snapshot as $ftu)
                            <tr>
                                <td class="table-text">
                                    <div>{{ $ftu->users_total }}</div>
                                </td>
                                <td class="table-text">
                                    <div>{{ $ftu->total_free_trials }}</div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>

                </div>
            </div>
        </div>

        <div class="col-xs-6 col-md-4">
            <div class="panel panel-default">
                <div class="panel-heading">Free Trials (Purchases)</div>
                <div class="panel-body">

                    <table class="table table-striped task-table">
                        <!-- Table Headings -->
                        <thead>
                        <th>Time Groups</th>
                        <th>Total Users</th>
                        <!-- th>Purchases</th>
                        <th>FT Date</th>
                        <th>Purchased</th -->
                        </thead>
                        <!-- Table Body -->
                        <tbody>
                        @foreach ($ft_paid_snapshot as $ftp)
                            <tr>
                                <td class="table-text">
                                    <div>{{ $ftp->groups }}</div>
                                </td>
                                <td class="table-text">
                                    <div>{{ $ftp->total_users }}</div>
                                </td>
                            <!-- td class="table-text">
                                  <div>{{ $ftp->purchase_count }}</div>
                              </td>
                              <td class="table-text">
                                  <div>{{ $ftp->ft }}</div>
                              </td>
                              <td class="table-text">
                                  <div>{{ $ftp->purchase }}</div>
                              </td -->
                            </tr>
                        @endforeach
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12 col-md-8">
            <div class="panel panel-default">
                <div class="panel-heading">LinkConnector Transactions</div>
                <div class="panel-body">

                    <table class="table table-striped task-table">
                        <!-- Table Headings -->
                        <thead>
                        <th>Created</th>
                        <th>Affiliate</th>
                        <th>Amount</th>
                        <th>Commission</th>
                        <th>LCFees</th>
                        <th>Total Cost</th>
                        <th>Event Type</th>
                        </thead>
                        <!-- Table Body -->

                        <tbody>
                        @foreach ($affiliates_snapshot as $o)
                            <tr>
                                <td class="table-text">
                                    <div>{{ $o->OriginalDate }}</div>
                                </td>
                                <td class="table-text">
                                    <div>{{ $o->Affiliate }}</div>
                                </td>
                                <td class="table-text">
                                    <div>${{ $o->Amount }}</div>
                                </td>
                                <td class="table-text">
                                    <div>${{ $o->Commission }}</div>
                                </td>
                                <td class="table-text">
                                    <div>${{ $o->LCFees }}</div>
                                </td>
                                <td class="table-text">
                                    <div>${{ $o->TotalCost }}</div>
                                </td>
                                <td class="table-text">
                                    <div>{{ $o->EventType }}</div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>

                </div>
            </div>
        </div>

        <div class="col-xs-6 col-md-4">
            <div class="panel panel-default">
                <div class="panel-heading">LinkConnector Performance (last 3 months)</div>
                <div class="panel-body">

                    <table class="table table-striped task-table">
                        <!-- Table Headings -->
                        <thead>
                        <th>Affiliate</th>
                        <th>Orders</th>
                        <th>Cost</th>
                        </thead>
                        <!-- Table Body -->

                        <tbody>
                        @foreach ($affiliates_stats_snapshot as $o)
                            <tr>
                                <td class="table-text">
                                    <div>{{ $o->Affiliate }}</div>
                                </td>
                                <td class="table-text">
                                    <div>{{ $o->OrderCount }}</div>
                                </td>
                                <td class="table-text">
                                    <div>@convertCurrency($o->TotalCost)</div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>

                </div>
            </div>
        </div>

    </div>

    <div class="row">
        <div class="col-xs-6 col-md-4">
            <div class="panel panel-default">
                <div class="panel-heading">IPQ Session Totals: All Time</div>
                <div class="panel-body">

                    <table class="table table-striped task-table">
                        <!-- Table Headings -->
                        <thead>
                        <th>SmartPath Quizes (completed)</th>
                        <th>SmartPath Quizes (incomplete)</th>
                        </thead>
                        <!-- Table Body -->
                        <tbody>
                        @foreach ($ipq_snapshot_stats as $ipastat)
                            <tr>
                                <td class="table-text">
                                    <div>{{ $ipastat->total_completed }}</div>
                                </td>
                                <td class="table-text">
                                    <div>{{ $ipastat->total_incomplete }}</div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>

                </div>
            </div>
        </div>

        <div class="col-xs-6 col-md-4">
            <div class="panel panel-default">
                <div class="panel-heading">IPQ Top 10 Users: (completed)</div>
                <div class="panel-body">

                    <table class="table table-striped task-table">
                        <!-- Table Headings -->
                        <thead>
                        <th>User ID</th>
                        <th># SmartPath</th>
                        </thead>
                        <!-- Table Body -->
                        <tbody>
                        @foreach ($ipq_stats_completed as $ipastat_c)
                        <tr>
                                <td class="table-text">
                                    <div>{{ $ipastat_c->uid }}</div>
                                </td>
                                <td class="table-text">
                                    <div>{{ $ipastat_c->total_quizes_finished }}</div>
                                </td>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>

                </div>
            </div>
        </div>

        <div class="col-xs-6 col-md-4">
            <div class="panel panel-default">
                <div class="panel-heading">IPQ Top 10 Users: (incomplete)</div>
                <div class="panel-body">

                    <table class="table table-striped task-table">
                        <!-- Table Headings -->
                        <thead>
                        <th>User ID</th>
                        <th># SmartPath</th>
                        </thead>
                        <!-- Table Body -->
                        <tbody>
                        @foreach ($ipq_stats_incomplete as $ipastat_inc)
                        <tr>
                            <td class="table-text">
                                <div>{{ $ipastat_inc->uid }}</div>
                            </td>
                            <td class="table-text">
                                <div>{{ $ipastat_inc->total_quizes_unfinished }}</div>
                            </td>
                            @endforeach
                        </tr>
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12 col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">IPQ Sessions</div>
                <div class="panel-body">

                    <table class="table table-striped task-table">
                        <!-- Table Headings -->
                        <thead>
                        <th>User ID</th>
                        <th>Type</th>
                        <th>Quizes (finished)</th>
                        <th>Avg % Completed</th>
                        <th>Avg % Correct</th>
                        <th>First Quiz (finished)</th>
                        <th>Last Quiz (finished)</th>

                        <th>Quizes (unfinished)</th>
                        <th>Avg % Completed</th>
                        <th>Avg % Correct</th>
                        <th>First Quiz (unfinished)</th>
                        <th>Last Quiz (unfinished)</th>
                        </thead>
                        <!-- Table Body -->

                        <tbody>
                        @php ($row = 1)
                        @foreach ($ipq_snapshot as $ipq)
                            <tr class="accordion"  data-toggle="collapse" data-target="#collapse-{{$row}}">
                                <td class="table-text">
                                    <div>{{ $ipq->uid }}</div>
                                </td>
                                <td class="table-text">
                                    <div>{{ $ipq->quiz_type }}</div>
                                </td>
                                <td class="table-text">
                                    <div>{{ $ipq->total_quizes_finished }}</div>
                                </td>
                                <!-- td class="table-text">
                                    <div>${{ $ipq->avg_time_finished }}</div>
                                </td -->
                                <td class="table-text">
                                    <div>{{ $ipq->avg_perc_complete_finished }}</div>
                                </td>
                                <td class="table-text">
                                    <div>{{ $ipq->avg_perc_correct_finished }}</div>
                                </td>
                                <td class="table-text">
                                    <div>{{ $ipq->first_date_finished }}</div>
                                </td>
                                <td class="table-text">
                                    <div>{{ $ipq->last_date_finished }}</div>
                                </td>

                                <td class="table-text">
                                    <div>{{ $ipq->total_quizes_unfinished }}</div>
                                </td>
                                <!-- td class="table-text">
                                    <div>{{ $ipq->avg_time_unfinished }}</div>
                                </td -->
                                <td class="table-text">
                                    <div>{{ $ipq->avg_perc_complete_unfinished }}</div>
                                </td>
                                <td class="table-text">
                                    <div>{{ $ipq->avg_perc_correct_unfinished }}</div>
                                </td>
                                <td class="table-text">
                                    <div>{{ $ipq->first_date_unfinished }}</div>
                                </td>
                                <td class="table-text">
                                    <div>{{ $ipq->last_date_unfinished }}</div>
                                </td>
                            </tr>
                            <tr id="collapse-{{$row}}" class="success collapse">
                                <td></td>
                                <td colspan="11">
                                More details coming soon...
                                </td>
                            </tr>
                            @php ($row++)
                        @endforeach
                        </tbody>
                    </table>

                </div>
            </div>
        </div>

    </div>

@endsection