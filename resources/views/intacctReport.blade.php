<!-- Stored in resources/views/dashboard.blade.php -->
@extends('layouts.dashboard')

@section('title', 'Intacct Report')

@section('content')

    <link href="{{ URL::asset('css/reports.css') }}" rel="stylesheet">

    <link href="{{ URL::asset('css/bootstrap-datepicker.min.css') }}" rel="stylesheet">

    <script type="text/javascript" src="{{ URL::asset('js/bootstrap-datepicker.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/reports.js') }}"></script>

    <div class="container filters">
        {{ Form::open(array('url' => '#')) }}
        <div class="row">
            <div class="col-md-2">
                {{ Form::label('month', 'Month') }}
                {{ Form::text('month', $filters['month'], [
                    'data-date-format' => 'mm',
                    'data-date-autoclose' => 'true'
                ]) }}
            </div>
            <div class="col-md-2">
                {{ Form::label('year', 'Year') }}
                {{ Form::text('year', $filters['year'], [
                    'data-date-format' => 'YYYY',
                    'data-date-autoclose' => 'true'
                ]) }}
            </div>
            <div class="col-md-8">
                {{ Form::label('order_status', 'Order Status') }}
                {{ Form::select('order_status', [
                    "shipped" => "Shipped",
                    "completed" => "Completed",
                    "pending_approval" => "Pending Approval",
                    "pending" => "Pending",
                   ], $filters['order_status'] ,array('multiple'=>'multiple', 'name'=>'order_status[]')) }}
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                {{Form::submit('submit', ['class'=>'btn btn-primary'])}}
                <a href="{{$csv_export_url}}" target="_blank">Export as CSV</a>
            </div>
        </div>
        {{ Form::close() }}
    </div>

    @if ($products->count() > 0)
        <div id="orders-list">
            <table class="results-table col-md-12">
                <thead>
                <tr>
                    <th class="">Intacct Customer ID</th>
                    <th class="">Terms</th>
                    <th class="">Total Amount</th>
                    <th class="">Course/Product</th>
                    <th class="">Sales Price</th>
                    <th class="">Shipping</th>
                    <th class="">Taxes</th>
                    <th class="">Order Date</th>
                    <th class="">Student Name</th>
                    <th class="">Office Location</th>
                    <th class="">Shipping Via</th>
                    <th class="">Shipping State</th>
                    <th class="">Free Trial</th>
                    <th class="">Order ID</th>
                </tr>
                </thead>
                <tbody>
                @foreach ($products as $product)
                    <tr class="
                            @if ($loop->iteration  % 2 == 0)
                                even
                            @else
                                odd
                            @endif
                            ">
                        <td>{{$product->Intacct_Customer_ID}}</td>
                        <td>{{$product->Terms}}</td>
                        <td>${{number_format($product->Total_Amount, 2)}}</td>
                        <td>{{$product->Course_Product}}</td>
                        <td>${{number_format($product->Sales_Price,2)}}</td>
                        <td>${{number_format($product->Shipping,2)}}</td>
                        <td>${{number_format($product->Taxes,2)}}</td>
                        <td>{{$product->Order_Date}}</td>
                        <td>{{$product->Student_Name}}</td>
                        <td>{{$product->Office_Location}}</td>
                        <td>{{$product->Shipping_Via}}</td>
                        <td>{{$product->Shipping_State}}</td>
                        <td>{{$product->free_trial}}</td>
                        <td>{{$product->order_id}}</td>
                    </tr>
                @endforeach
                </tbody>
                <tfoot>

                </tfoot>
            </table>
        </div>
    @endif

@endsection
