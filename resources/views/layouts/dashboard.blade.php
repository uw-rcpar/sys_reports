<!-- Stored in resources/views/layouts/.blade.php -->
<html lang="en">
<head>
    <title>@yield('title')</title>
    <link rel="shortcut icon" href="/images/favicon.png" type="image/png" />
    <link href="{{ URL::to('/css/reports.css') }}" rel="stylesheet">
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>
<nav class="navbar navbar-default">
    <div class="container-fluid">
        <div class="navbar-header">
            <a class="navbar-brand" href="#">@yield('title') Dashboard:</a>

            <ul class="nav navbar-nav">
                <li><a href="{{ URL::to('dashboard') }}">Systems</a></li>

                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Reporting<span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="{{ URL::to('dashboard/reporting') }}">Dashboard</a></li>
                        <li role="separator" class="divider"></li>
			            <li><a href="{{ URL::to('reporting/intacct-report') }}"> Intacct Report</a></li>
            			<li><a href="{{ URL::to('reporting/CA-use-tax-report') }}"> CA Use Tax Report</a></li>
                        <li><a href="{{ URL::to('reporting/taxable_per_state_and_product') }}">Taxable Per State & Product</a></li>
                        <li><a href="{{ URL::to('reporting/products_detailed') }}">Products Detailed</a></li>
                        <li><a href="{{ URL::to('reporting/taxes_by_city') }}">Taxes by City</a></li>
                        <li role="separator" class="divider"></li>
                        <li><a href="{{ URL::to('reporting/rcpar_nps_log') }}">NPS Report</a></li>
                    </ul>
                </li>

                <li><a href="{{ URL::to('partners') }}">Partners</a></li>

                <li><a href="{{ URL::to('system/logs') }}">Logs</a></li>
            </ul>

        </div>
    </div>
</nav>

@if (session('status'))
    <div class="alert alert-success">
        {{ session('status') }}
    </div>
@endif

@section('sidebar')
    <p>This is the master sidebar.</p>
@endsection

<div class="dw-container">
    @yield('content')
</div>

</body>
</html>
