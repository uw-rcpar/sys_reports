<!-- Stored in resources/views/dashboard.blade.php -->
@extends('layouts.dashboard')

@section('title', 'Net Promoter Score')

@section('content')

    <link href="{{ URL::asset('css/reports.css') }}" rel="stylesheet">

    <link href="{{ URL::asset('css/bootstrap-datepicker.min.css') }}" rel="stylesheet">

    <script type="text/javascript" src="{{ URL::asset('js/bootstrap-datepicker.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/reports.js') }}"></script>

    <div class="container filters">
        {{ Form::open(array('url' => '#')) }}
            <div class="row">
                <div class="col-md-2">
                    {{ Form::label('email', 'Email') }}
                    {{ Form::text('email', $filters['email']) }}
                </div>
            </div>
            <div class="row">
                <div class="col-md-2">
                    {{Form::submit('submit', ['class'=>'btn btn-primary'])}}
                    <a href="{{$csv_export_url}}" target="_blank">Export as CSV</a>
                </div>
            </div>
        {{ Form::close() }}
    </div>

    <div >
        <table class="results-table col-md-12">
            <thead>
            <tr>
                <th class="">
                    <a href="{{$sorting_url}}&sort_by=uid&sort_dir={{
                                ($sorting['sort_by'] == 'uid' && $sorting['sort_dir'] == 'desc')? 'asc' : 'desc'
                            }}">User ID</a>
                </th>
                <th class="">Email</th>
                <th class="">
                    <a href="{{$sorting_url}}&sort_by=date&sort_dir={{
                                ($sorting['sort_by'] == 'date' && $sorting['sort_dir'] == 'desc')? 'asc' : 'desc'
                            }}">Date</a>
                </th>
                <th class="">
                    <a href="{{$sorting_url}}&sort_by=score&sort_dir={{
                                ($sorting['sort_by'] == 'score' && $sorting['sort_dir'] == 'desc')? 'asc' : 'desc'
                            }}">Score</a>
                </th>
                <th class="">Comment</th>
                <th class="">Purchaser Type</th>
                <th class="">Skus</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($promoterScores as $score)
                <tr class="
                        @if ($loop->iteration  % 2 == 0)
                           even
                        @else
                           odd
                        @endif
                        ">
                    <td>{{$score->uid}}</td>
                    <td>{{$score->mail}}</td>
                    <td>{{date('D, m/d/Y - H:i', strtotime($score->date))}}</td>
                    <td>{{$score->score}}</td>
                    <td>{{$score->comment}}</td>
                    <td>{{$score->purchaser_type}}</td>
                    <td>{{$score->skus}}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
        <div class="pagination">
            {{$promoterScores->appends([
                'email' => $filters['email'],
                'sort_by' => $sorting['sort_by'],
                'sort_dir' => $sorting['sort_dir'],
             ])->links()}}
        </div>
    </div>

@endsection
