<!-- Stored in resources/views/dashboard.blade.php -->
@extends('layouts.dashboard')

@section('title', 'CA - Use tax report')

@section('content')

    <link href="{{ URL::asset('css/reports.css') }}" rel="stylesheet">

    <link href="{{ URL::asset('css/bootstrap-datepicker.min.css') }}" rel="stylesheet">

    <script type="text/javascript" src="{{ URL::asset('js/bootstrap-datepicker.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/reports.js') }}"></script>

    <div class="container filters">
        {{ Form::open(array('url' => '#')) }}
            <div class="row">
                <div class="col-md-2">
                    {{ Form::label('start_date', 'Start date') }}
                    {{ Form::text('start_date', $filters['start_date'], [
                        'data-provide' => 'datepicker',
                        'data-date-format' => 'yyyy-mm-dd',
                        'data-date-autoclose' => 'true'
                    ]) }}
                </div>
                <div class="col-md-2">
                    {{ Form::label('end_date', 'End date') }}
                    {{ Form::text('end_date', $filters['end_date'], [
                        'data-provide' => 'datepicker',
                        'data-date-format' => 'yyyy-mm-dd',
                        'data-date-autoclose' => 'true'
                    ]) }}
                </div>
                <div class="col-md-2">
                    {{ Form::label('billing_type', 'Billing type') }}
                    {{ Form::select('billing_type', [
                            '' => "-Any-",
                            'CC-affirm' => "Credit Card/Affirm",
                            'DB' => "Direct Bill",
                            'freetrial' => "Free Access",
                            'prepaid' => "Prepaid",
                            'Check' => "Check",
                        ], $filters['billing_type']) }}
                </div>
            </div>
            <div class="row">
                <div class="col-md-2">
                    {{ Form::label('sku', 'Search products (SKU)') }}
                    {{ Form::text('sku', $filters['sku']) }}
                </div>
                <div class="col-md-2">
                    {{ Form::label('order_state', 'Order State') }}
                    {{ Form::select('order_state', [
                        "canceled" => "Canceled",
                        "cart" => "Shopping cart",
                        "checkout" => "Checkout",
                        "pending" => "Pending",
                        "completed" => "Completed",
                        "shipped" => "Shipped",
                       ], $filters['order_state'] ,array('multiple'=>'multiple', 'name'=>'order_state[]')) }}
                </div>
            </div>
            <div class="row">
                <div class="col-md-2">
                    {{Form::submit('submit', ['class'=>'btn btn-primary'])}}
                </div>
            </div>
        {{ Form::close() }}
    </div>

    <div >
        <table class="results-table col-md-12">
            <thead>
            <tr>
                <th class="">Type</th>
                <th class="">Product</th>
                <th class="">Title</th>
                <th class="">Sold</th>
            </tr>
            </thead>
            <tbody>

            @foreach ($products as $product)
                <tr class="
                        @if ($loop->iteration  % 2 == 0)
                           even
                        @else
                           odd
                        @endif
                        ">
                    <td>{{$product->product_type}}</td>
                    <td>{{$product->sku}}</td>
                    <td>{{$product->product_name}}</td>
                    <td>{{$product->quantity}}</td>
                </tr>
            @endforeach

            </tbody>
            <tfoot>
            <tr class="summary">
                <td></td>
                <td></td>
                <td><div class="total">Total:</div>{{$products_total_quantity}}</td>
            </tr>
            </tfoot>
        </table>
    </div>

    <div>
        <div>
            <a class="toggler" data-target="order-list" id="order-toggle" href="">Show/hide Order list</a>
        </div>
        <div id="order-list" class="toggler-target hidden">
            <table class="results-table col-md-12">
                <thead>
                <tr>
                    <th class="">Order Id</th>
                    <th class="">Payment updated date</th>
                    <th class="">Order State</th>
                    <th class="">Pre-tax Revenue</th>
                    <th class="">Taxed</th>
                    <th class="">Total</th>
                    <th class="">Total+Shipping</th>
                    <th class="">Payment method</th>
                </tr>
                </thead>
                <tbody>
                    @foreach ($orders as $order)
                    <tr class="
                            @if ($loop->iteration  % 2 == 0)
                                even
                            @else
                                odd
                            @endif
                            ">
                        <td>{{$order->order_id}}</td>
                        <td>{{date('l, F j, Y - H:i', strtotime($order->payment_updated_at))}}</td>
                        <td>{{$order->status}}</td>
                        <td>${{number_format($order->total - $order->taxed,2)}}</td>
                        <td>${{number_format($order->taxed,2)}}</td>
                        <td>${{number_format($order->total,2)}}</td>
                        <td>${{number_format($order->shipping+$order->total,2)}}</td>
                        <td>{{$order->payment_method}}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>

@endsection
