@extends('layouts.dashboard')

@section('content')
    <h1>Order Information</h1>

    <div class="panel panel-default">
        @if (session('status'))
        <div class="alert alert-success">
        {{ session('status') }}
        </div>
        @endif
        <div class="panel-heading">
              Order: {{ $order->order_id }}
          </div>
          <div class="panel-body">
              <div class="panel-body">
                  <table class="table table-striped task-table">
                      <tbody>
                      <tr>
                          <td class="table-text">Created:</td>
                          <td class="table-text">{{$order->create_date}}</td>
                      </tr>
                      <tr>
                          <td class="table-text">User:</td>
                          <td class="table-text"><a href="{{ route('user_profiles.show', $user->uid) }}">{{ $user->first_name }} {{ $user->last_name }}</a></td>
                      </tr>
                      <tr>
                          <td class="table-text">Partner:</td>
                          <td class="table-text"><a href="{{ route('partners.show', $order->partner_nid) }}">{{ $order->partner_nid }}</a></td>
                      </tr>
                      <tr>
                          <td class="table-text">Country:</td>
                          <td class="table-text">{{$order->country}}</td>
                      </tr>
                      <tr>
                          <td class="table-text">State:</td>
                          <td class="table-text">{{$order->state}}</td>
                      </tr>
                      <tr>
                          <td class="table-text">Revenue:</td>
                          <td class="table-text">${{number_format($order->net_order_revenue,2)}}</td>
                      </tr>
                      <tr>
                          <td class="table-text">Refunded Amount:</td>
                          <td class="table-text">${{number_format($order->refund_amount,2)}} </td>
                      </tr>
                      <tr>
                          <td class="table-text">Discounts:</td>
                          <td class="table-text">${{number_format($order->partner_discount_total,2)}} </td>
                      </tr>
                      </tbody>
                  </table>
              </div>
          </div>
    </div>
@endsection