<!-- Stored in resources/views/dashboard.blade.php -->
@extends('layouts.dashboard')

@section('title', 'Systems')

@section('content')

    <div class="row">
        <div class="col-xs-8 col-md-8">
            <div class="panel panel-default">
                <div class="panel-heading">New Relic Site Performance</div>
                <div class="panel-body">
                <iframe src="https://rpm.newrelic.com/public/charts/94Wc5XNtVs5" width="700" height="300" scrolling="no" frameborder="no"></iframe>
                </div>
            </div>
        </div>

        <div class="col-xs-4 col-md-4">
            <div class="panel panel-default">
                <div class="panel-heading">New Relic Site Errors</div>
                <div class="panel-body">
                <iframe src="https://rpm.newrelic.com/public/charts/7A18VPhQIDg" width="320" height="300" scrolling="no" frameborder="no"></iframe>
		</div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12 col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">New Relic Broswer Performance</div>
                <div class="panel-body">
		<iframe src="https://rpm.newrelic.com/public/charts/cMDzIh38fgg" width="900" height="300" scrolling="no" frameborder="no"></iframe>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12 col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">New Relic Page Throughput</div>
                <div class="panel-body">
		<iframe src="https://rpm.newrelic.com/public/charts/bjxnzwqMswF" width="900" height="300" scrolling="no" frameborder="no"></iframe>
                </div>
            </div>
        </div>
    </div>

@endsection
