<!-- Stored in resources/views/dashboard.blade.php -->
@extends('layouts.dashboard')

@section('title', 'Taxes by city')

@section('content')

    <link href="{{ URL::asset('css/reports.css') }}" rel="stylesheet">

    <link href="{{ URL::asset('css/bootstrap-datepicker.min.css') }}" rel="stylesheet">

    <script type="text/javascript" src="{{ URL::asset('js/bootstrap-datepicker.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/reports.js') }}"></script>

    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <div class="container filters">
        {{ Form::open(array('url' => '#')) }}
            <div class="row">
                <div class="col-md-2">
                    {{ Form::label('start_date', 'Start date') }}
                    {{ Form::text('start_date', $filters['start_date'], [
                        'data-provide' => 'datepicker',
                        'data-date-format' => 'yyyy-mm-dd',
                        'data-date-autoclose' => 'true'
                    ]) }}
                </div>
                <div class="col-md-2">
                    {{ Form::label('state', 'State') }}
                    {{ Form::text('state', $filters['state']) }}
                </div>
                <div class="col-md-3">
                    {{ Form::label('payment_methods', 'Payment method') }}
                    {{ Form::select('payment_methods', [
                        "affirm" => "Affirm Payment",
                        "CC" => "Authorize.Net AIM - Credit Card",
                        "DB" => "Direct Bill Payment",
                        "PPD CR" => "Prepaid Credit Payment",
                        "PPD DR" => "Prepaid Debit Payment",
                        "freetrial" => "Partner Free Trial",
                        "Check" => "Pay by Check",
                       ], $filters['payment_methods'] ,array('multiple'=>'multiple', 'name'=>'payment_methods[]')) }}
                </div>
            </div>
            <div class="row">
                <div class="col-md-2">
                    {{ Form::label('end_date', 'End date') }}
                    {{ Form::text('end_date', $filters['end_date'], [
                        'data-provide' => 'datepicker',
                        'data-date-format' => 'yyyy-mm-dd',
                        'data-date-autoclose' => 'true'
                    ]) }}
                </div>
                <div class="col-md-2">
                    {{ Form::label('order_state', 'Order status') }}
                    {{ Form::select('order_state', [
                        "canceled" => "Canceled",
                        "cart" => "Shopping cart",
                        "checkout" => "Checkout",
                        "pending" => "Pending",
                        "completed" => "Completed",
                        "shipped" => "Shipped",
                       ], $filters['order_state'] ,array('multiple'=>'multiple', 'name'=>'order_state[]')) }}
                </div>
            </div>
            <div class="row">
                <div class="col-md-2">
                    {{Form::submit('submit', ['class'=>'btn btn-primary'])}}
                    <a href="{{$csv_export_url}}" target="_blank">Export as CSV</a>
                </div>
            </div>
        {{ Form::close() }}
    </div>

    @if ($cities_info->count() > 0)
        <div id="orders-list">
            <table class="results-table col-md-12">
                <thead>
                <tr>
                    <th class="">City</th>
                    <th class="">Order total</th>
                    <th class="">Taxable</th>
                    <th class="">Taxed</th>
                    <th class="">Order Ids</th>
                </tr>
                </thead>
                <tbody>
                    @foreach ($cities_info as $city_info)
                    <tr data-target="order-list-{{$loop->iteration}}"  class="toggler
                            @if ($loop->iteration  % 2 == 0)
                                even
                            @else
                                odd
                            @endif
                            ">
                        <td>{{$city_info->city_name}}</td>
                        <td>${{number_format($city_info->total,2)}}</td>
                        <td>${{number_format($city_info->taxable,2)}}</td>
                        <td>${{number_format($city_info->taxed,2)}}</td>
                        <td>{{$city_info->order_ids}}</td>
                    </tr>
                    <tr id="order-list-{{$loop->iteration}}" class="toggler-target hidden" >
                        <td colspan="5">
                            <table class="results-table col-md-6">
                                <thead>
                                <tr>
                                    <th class="">Order ID</th>
                                    <th class="">Order total</th>
                                    <th class="">Taxable</th>
                                    <th class="">Taxed</th>
                                </tr>
                                </thead>
                                @foreach ($city_info->orders_tax_info as $ord_tax_info)
                                    <tr class="
                                        @if ($loop->iteration  % 2 == 0)
                                            even
                                        @else
                                            odd
                                        @endif
                                            ">
                                        <td>{{$ord_tax_info->order_id}}</td>
                                        <td>${{number_format($ord_tax_info->order_total,2)}}</td>
                                        <td>${{number_format($ord_tax_info->taxable,2)}}</td>
                                        <td>${{number_format($ord_tax_info->taxes,2)}}</td>
                                    </tr>
                                @endforeach
                            </table>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
                <tfoot>
                <tr class="summary">
                    <td></td>
                    <td><div class="total">Total:</div>${{number_format($state_totals['total'],2)}}</td>
                    <td><div class="total">Total:</div>${{number_format($state_totals['taxable'],2)}}</td>
                    <td><div class="total">Total:</div>${{number_format($state_totals['taxed'],2)}}</td>
                    <td></td>
                </tr>
                </tfoot>
            </table>
        </div>
    @endif

    @if ($refunds->count() > 0)
        <div class="block-container">
            <div>
                <a class="toggler" data-target="refunds-list" href="">Show/hide Refunds list</a>
            </div>
            <div class="toggler-target hidden" id="refunds-list">
                <table class="results-table col-md-12">
                    <thead>
                    <tr>
                        <th class="">Order Id</th>
                        <th class="">Order status</th>
                        <th class="">Credit date</th>
                        <th class="">Paid date</th>
                        <th class="">Taxable</th>
                        <th class="">Taxed</th>
                        <th class="">Order total</th>
                        <th class="">Amount</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($refunds as $order)
                        <tr class="
                            @if ($loop->iteration  % 2 == 0)
                                even
                            @else
                                odd
                            @endif
                                ">
                            <td>{{$order->order_id}}</td>
                            <td>{{ucwords($order->status)}}</td>
                            <td>{{date('m/d/Y', strtotime($order->refund_date))}}</td>
                            <td>{{date('m/d/Y', strtotime($order->payment_updated_at))}}</td>
                            <td>${{number_format($order->taxable,2)}}</td>
                            <td>${{number_format($order->tax_total,2)}}</td>
                            <td>${{number_format($order->order_total,2)}}</td>
                            <td>${{number_format($order->refund_amount,2)}}</td>
                        </tr>
                    @endforeach
                    </tbody>
                    <tfoot>
                    <tr class="summary">
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td><div class="total">Total:</div>${{number_format($refunds_totals['taxable'],2)}}</td>
                        <td><div class="total">Total:</div>${{number_format($refunds_totals['taxed'],2)}}</td>
                        <td><div class="total">Total:</div>${{number_format($refunds_totals['total'],2)}}</td>
                        <td><div class="total">Total:</div>${{number_format($refunds_totals['refund_amount'],2)}}</td>
                    </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    @endif

@endsection
