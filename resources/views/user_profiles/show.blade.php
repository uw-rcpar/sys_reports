@extends('layouts.dashboard')

@section('content')

<h1>User Profile: {{ $user->first_name }} {{ $user->last_name }}</h1>

  <div class="panel panel-default">

    @if (session('status'))
    <div class="alert alert-success">
      {{ session('status') }}
    </div>
    @endif
        <div class="col-md-9">

            <div class="panel-heading">
                Learning:
            </div>
            <div class="panel-body">
                @if (count($learning_stats) > 0)
                    <table class="table table-striped task-table">
                        <!-- Table Headings -->
                        <thead>
                        <th>Product SKU</th>
                        <th>Ent</th>
                        <th>%</th>
                        <th>Restricted access</th>
                        <th>IPQ</th>
                        <th>Unlimited</th>
                        <th>Mobile offline</th>
                        <th>Expires</th>
                        </thead>
                        <tbody>
                        @foreach ($learning_stats as $l)
                            <tr>
                                <!-- Task Name -->
                                <td class="table-text">
                                    <div>{{ $l->sku }}</div>
                                </td>
                                <td class="table-text">
                                    <div>
                                        {{ $l->nid }}
                                    </div>
                                </td>
                                <td class="table-text">
                                    <div>
                                        @if ($l->viewed === NULL)
                                          n/a
                                        @elseif ($l->videos_total_duration)
                                          {{ round(($l->viewed * 100) / $l->videos_total_duration) }}%
                                        @else
                                          0%
                                        @endif
                                    </div>
                                </td>
                                <td class="table-text">
                                    <div>
                                        @if ($l->content_restricted)
                                            Yes
                                        @else
                                            No
                                        @endif
                                    </div>
                                </td>
                                <td class="table-text">
                                    <div>
                                        @if ($l->ipq_access)
                                        Yes
                                        @else
                                        No
                                        @endif
                                    </div>
                                </td>
                                <td class="table-text">
                                    <div>
                                        @if ($l->unlimited_access)
                                        Yes
                                        @else
                                        No
                                        @endif
                                    </div>
                                </td>
                                <td class="table-text">
                                    <div>
                                        @if ($l->mobile_offline_access)
                                        Yes
                                        @else
                                        No
                                        @endif
                                    </div>
                                </td>
                                <td class="table-text">
                                    <div>{{ $l->expiration_date }}</div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    <div class="pagination">
                        {{$orders->appends([])->links()}}
                    </div>
                @endif
            </div>

            <div class="panel-heading">
                Orders:
            </div>
            <div class="panel-body">
              @if (count($orders) > 0)
                  <table class="table table-striped task-table">
                      <!-- Table Headings -->
                      <thead>
                      <th>Order ID</th>
                      <th>Amount</th>
                      <th>Status</th>
                      <th>Date</th>
                      </thead>
                      <!-- Table Body -->
                      <tbody>
                      @foreach ($orders as $o)
                          <tr>
                              <!-- Task Name -->
                              <td class="table-text">
                                  <div><a href="{{ route('orders.show', $o->order_id) }}">#{{ $o->order_id }}</a></div>
                              </td>
                              <td class="table-text">
                                  <div><a href="{{ route('orders.show', $o->order_id) }}">${{number_format($o->order_total,2)}} </a></div>
                              </td>
                              <td class="table-text">
                                  <div>{{ $o->order_status }}</div>
                              </td>
                              <td class="table-text">
                                  <div>{{date('l, F j, Y', strtotime($o->create_date))}}</div>
                              </td>
                          </tr>
                      @endforeach
                      </tbody>
                  </table>
                  <div class="pagination">
                      {{$orders->appends([])->links()}}
                  </div>
              @endif
            </div>

        </div>
        <div class="col-md-3">
            <div class="panel-heading">
                Timelines
            </div>
            <div class="panel-body">
                <table class="table table-striped task-table">
                    <tbody>
                        <tr>
                            <td class="table-text">Registered:</td>
                            <td class="table-text">{{date('d/m/Y', strtotime($user->registration_date))}}</td>
                        </tr>
                        <tr>
                            <td class="table-text">Graduation:</td>
                            <td class="table-text">{{date('d/m/Y', strtotime($user->grad_date))}}</td>
                        </tr>
                        <tr>
                            <td class="table-text">AUD Exam:</td>
                            <td class="table-text">
                            @if ($user->aud_exam_date != '0000-00-00')
                            {{date('d/m/Y', strtotime($user->aud_exam_date)) }}
                            @else
                                n/a
                            @endif
                            </td>
                        </tr>
                        <tr>
                            <td class="table-text">BEC Exam:</td>
                            <td class="table-text">
                            @if ($user->bec_exam_date != '0000-00-00')
                                {{date('d/m/Y', strtotime($user->bec_exam_date)) }}
                            @else
                                n/a
                            @endif
                            </td>
                        </tr>
                        <tr>
                            <td class="table-text">FAR Exam:</td>
                            <td class="table-text">
                            @if ($user->far_exam_date != '0000-00-00')
                                {{date('d/m/Y', strtotime($user->far_exam_date)) }}
                            @else
                                n/a
                            @endif
                            </td>
                        </tr>
                        <tr>
                            <td class="table-text">REG Exam:</td>
                            <td class="table-text">
                            @if ($user->reg_exam_date != '0000-00-00')
                                {{date('d/m/Y', strtotime($user->reg_exam_date)) }}
                            @else
                                n/a
                            @endif
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
  </div>

@endsection