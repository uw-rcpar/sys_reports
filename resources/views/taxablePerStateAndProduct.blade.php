<!-- Stored in resources/views/dashboard.blade.php -->
@extends('layouts.dashboard')

@section('title', 'Taxable Per State And Product')

@section('content')

    <link href="{{ URL::asset('css/reports.css') }}" rel="stylesheet">

    <link href="{{ URL::asset('css/bootstrap-datepicker.min.css') }}" rel="stylesheet">

    <script type="text/javascript" src="{{ URL::asset('js/bootstrap-datepicker.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/reports.js') }}"></script>

    <div class="container filters">
        {{ Form::open(array('url' => '#')) }}
            <div class="row">
                <div class="col-md-2">
                    {{ Form::label('start_date', 'Start date') }}
                    {{ Form::text('start_date', $filters['start_date'], [
                        'data-provide' => 'datepicker',
                        'data-date-format' => 'yyyy-mm-dd',
                        'data-date-autoclose' => 'true'
                    ]) }}
                </div>
                <div class="col-md-2">
                    {{ Form::label('state', 'State') }}
                    {{ Form::text('state', $filters['state']) }}
                </div>

                <div class="col-md-2">
                    {{ Form::label('order_state', 'Order status') }}
                    {{ Form::select('order_state', [
                        "canceled" => "Canceled",
                        "cart" => "Shopping cart",
                        "checkout" => "Checkout",
                        "pending" => "Pending",
                        "completed" => "Completed",
                        "shipped" => "Shipped",
                       ], $filters['order_state'] ,array('multiple'=>'multiple', 'name'=>'order_state[]')) }}
                </div>

            </div>
            <div class="row">
                <div class="col-md-2">
                    {{ Form::label('end_date', 'End date') }}
                    {{ Form::text('end_date', $filters['end_date'], [
                        'data-provide' => 'datepicker',
                        'data-date-format' => 'yyyy-mm-dd',
                        'data-date-autoclose' => 'true'
                    ]) }}
                </div>

                <div class="col-md-2">
                    {{ Form::label('payment_method', 'Payment method') }}
                    {{ Form::select('payment_method', [
                        "affirm" => "Affirm Payment",
                        "CC" => "Authorize.Net AIM - Credit Card",
                        "DB" => "Direct Bill Payment",
                        "PPD CR" => "Prepaid Credit Payment",
                        "PPD DR" => "Prepaid Debit Payment",
                        "" => "Partner Free Trial",
                        "Check" => "Pay by Check",
                       ], $filters['payment_method'] ,array('multiple'=>'multiple', 'name'=>'payment_method[]')) }}
                </div>


            </div>
            <div class="row">
                <div class="col-md-2">
                    {{Form::submit('submit', ['class'=>'btn btn-primary'])}}
                </div>
            </div>
        {{ Form::close() }}
    </div>

    @if ($orders->count() > 0)
        <div id="orders-list">
            <table class="results-table col-md-12">
                <thead>
                <tr>
                    <th class="">Order status</th>
                    <th class="">Order ID</th>
                    <th class="">City</th>
                    <th class="">State</th>
                    <th class="">Zip code</th>
                    <th class="">Order total</th>
                    <th class="">Shipping</th>
                    <th class="">Taxable</th>
                    <th class="">Order date</th>

                    @foreach ($products as $product)
                        <th>{{$product}}</th>
                    @endforeach

                    <th class="">Customer first name</th>
                    <th class="">Customer last name</th>
                </tr>
                </thead>
                <tbody>
                    @foreach ($orders as $order)
                    <tr class="
                            @if ($loop->iteration  % 2 == 0)
                                even
                            @else
                                odd
                            @endif
                            ">
                        <td>{{ucwords($order->status)}}</td>
                        <td>{{$order->order_id}}</td>
                        <td>{{$order->city}}</td>
                        <td>{{$order->state}}</td>
                        <td>{{$order->postal_code}}</td>
                        <td>${{number_format($order->total,2)}}</td>
                        <td>${{number_format($order->shipping,2)}}</td>
                        <td>${{number_format($order->taxed,2)}}</td>
                        <td>{{date('l, F j, Y', strtotime($order->created_date))}}</td>

                        @foreach ($products as $sku => $product)
                            <td>${{number_format($order->$sku, 2)}}</td>
                        @endforeach
                        <td>{{$order->customer_first_name}}</td>
                        <td>{{$order->customer_last_name}}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            <div class="pagination">
                {{$orders->appends([
                    'order_state' => $filters['order_state'],
                    'start_date' => $filters['start_date'],
                    'end_date' => $filters['end_date'],
                    'state' => $filters['state'],
                    'payment_method' => $filters['payment_method'],
                 ])->links()}}
            </div>
        </div>

        <div>
            <a href="{{$csv_export_url}}" target="_blank">Export as CSV</a>
        </div>

    @endif

@endsection
