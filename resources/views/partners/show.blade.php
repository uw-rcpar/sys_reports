@extends('layouts.dashboard')

@section('content')

<h1>Partner: {{ $partner->partner_name }}</h1>

  <div class="panel panel-default">

    @if (session('status'))
    <div class="alert alert-success">
      {{ session('status') }}
    </div>
    @endif

      <div class="panel-heading">
          Users:
      </div>

      <div class="panel-body">
          @if (count($users) > 0)
              <table class="table table-striped task-table">
                  <!-- Table Headings -->
                  <thead>
                  <th>ID</th>
                  <th>Name</th>
                  <th>Email</th>
                  <th>Roles</th>
                  <th>Registration Date</th>
                  </thead>
                  <!-- Table Body -->
                  <tbody>
                  @foreach ($users as $u)
                      <tr>
                          <!-- Task Name -->
                          <td class="table-text">
                              <div><a href="{{ route('user_profiles.show', $u->uid) }}">#{{ $u->uid }}</a></div>
                          </td>
                          <td class="table-text">
                              <div>
                                  <a href="{{ route('user_profiles.show', $u->uid) }}">
                                      {{ $u->first_name }} {{ $u->last_name }}
                                  </a>
                              </div>
                          </td>
                          <td class="table-text">
                              <div>{{ $u->mail }}</div>
                          </td>
                          <td class="table-text">
                              <div>{{ $u->roles }}</div>
                          </td>
                          <td class="table-text">
                              <div>{{date('d/m/Y - h:m', strtotime($u->registration_date)) }}</div>
                          </td>
                      </tr>
                  @endforeach
                  </tbody>
              </table>
              <div class="pagination">
                  {{$users->appends([])->links()}}
              </div>
          @endif
      </div>
  </div>

  
@endsection