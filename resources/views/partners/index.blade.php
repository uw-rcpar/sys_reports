@extends('layouts.dashboard')

@section('content')

    <h1>Partners List</h1>
      <div class="panel panel-default">
        
        @if (session('status'))
        <div class="alert alert-success">
          {{ session('status') }}
        </div>
        @endif

            <div class="container filters">
                {{ Form::open(array('action' => array('PartnerProfilesController@index')) ) }}
                <div class="row">
                    <div class="col-md-2">
                        {{ Form::label('partner_name', 'Partner name') }}
                        {{ Form::text('partner_name', $filters['partner_name']) }}
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-2">
                        {{Form::submit('submit', ['class'=>'btn btn-primary'])}}
                    </div>
                </div>
                {{ Form::close() }}
            </div>
          <div class="panel-heading">
              Partners:
          </div>

        @if (count($partners) > 0)

          <div class="panel-body">
              <table class="table table-striped task-table">
                  <!-- Table Headings -->
                  <thead>
                      <th>ID</th>
                      <th>Name</th>
                      <th>Account Name</th>
                  </thead>
                  <!-- Table Body -->
                  <tbody>
                      @foreach ($partners as $p)
                          <tr>
                              <!-- Task Name -->
                              <td class="table-text">
                                  <div><a href="{{ route('partners.show', $p->nid) }}">{{ $p->nid }}</a></div>
                              </td>
                              <td class="table-text">
                                  <div><a href="{{ route('partners.show', $p->nid) }}">{{ $p->partner_name }}</a></div>
                              </td>
                              <td class="table-text">
                                  <div>{{ $p->account_name }}</div>
                              </td>
                          </tr>
                      @endforeach
                  </tbody>
              </table>
              <div class="pagination">
                  {{$partners->appends([
                    'partner_name' => $filters['partner_name']
                   ])->links()}}
              </div>
          </div>
        @endif

      </div>

@endsection