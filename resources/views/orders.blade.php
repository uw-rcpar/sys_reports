@extends('layouts.dashboard')

@section('content')

  <!-- Current Tasks -->
  @if (count($orders) > 0)
      <div class="panel panel-default">
        
        @if (session('status'))
        <div class="alert alert-success">
          {{ session('status') }}
        </div>
        @endif
        
          <div class="panel-heading">
              Current FT Orders ({{count($orders)}})
          </div><br>
          
          <div class="panel-body">
              <table class="table table-striped task-table">
                  <!-- Table Headings -->
                  <thead>
                      <th>ID</th>
                      <th>Total</th>
                      <th>Created</th>
                      <th></th>
                  </thead>
                  <!-- Table Body -->
                  <tbody>
                      @foreach ($orders as $o)
                          <tr>
                              <!-- Task Name -->
                              <td class="table-text">
                                  <div>{{ $o->id }}</div>
                              </td>
                              <td class="table-text">
                                  <div>{{ $o->create_day }}</div>
                              </td>
                              <td class="table-text">
                                  <div>{{ $o->total_count }}</div>
                              </td>
                              <td>
                              <form action="/order/{{$o->id}}/delete" method="POST">
                                {{ csrf_field() }}
                                {{ method_field('DELETE') }}
                                <input type="hidden" name="id" value="{{ $o->id }}">
                                <button>Delete Task</button>
                                </form>
                              </td>
                          </tr>
                      @endforeach
                  </tbody>
              </table>
              <a href="/orders/random/insert">Generate some more entries</a><br>
              <A href="/orders/import/orders">Import: orders.csv</a>
          </div>
      </div>
  @endif
  
@endsection