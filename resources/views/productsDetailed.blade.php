<!-- Stored in resources/views/dashboard.blade.php -->
@extends('layouts.dashboard')

@section('title', 'Products Detailed')

@section('content')

    <link href="{{ URL::asset('css/reports.css') }}" rel="stylesheet">

    <link href="{{ URL::asset('css/bootstrap-datepicker.min.css') }}" rel="stylesheet">

    <script type="text/javascript" src="{{ URL::asset('js/bootstrap-datepicker.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/reports.js') }}"></script>

    <div class="container filters">
        {{ Form::open(array('url' => '#')) }}
            <div class="row">
                <div class="col-md-2">
                    {{ Form::label('sku', 'Search products (SKU)') }}
                    {{ Form::text('sku', $filters['sku']) }}
                </div>
                <div class="col-md-2">
                    {{ Form::label('start_date', 'Start date') }}
                    {{ Form::text('start_date', $filters['start_date'], [
                        'data-provide' => 'datepicker',
                        'data-date-format' => 'yyyy-mm-dd',
                        'data-date-autoclose' => 'true'
                    ]) }}
                </div>
                <div class="col-md-2">
                    {{ Form::label('country', 'Country') }}
                    {{ Form::select('country', [
                            '' => "-Any-",
                            'US' => "USA",
                            'NON-US' => "Non USA",
                        ], $filters['country']) }}
                </div>
            </div>
            <div class="row">
                <div class="col-md-2">
                </div>
                <div class="col-md-2">
                    {{ Form::label('end_date', 'End date') }}
                    {{ Form::text('end_date', $filters['end_date'], [
                        'data-provide' => 'datepicker',
                        'data-date-format' => 'yyyy-mm-dd',
                        'data-date-autoclose' => 'true'
                    ]) }}
                </div>
                <div class="col-md-2">
                    {{ Form::label('payment_methods', 'Payment method') }}
                    {{ Form::select('payment_methods', [
                        "Affirm" => "Affirm Payment",
                        "CC" => "Authorize.Net AIM - Credit Card",
                        "DB" => "Direct Bill Payment",
                        "PPD CR" => "Prepaid Credit Payment",
                        "PPD DR" => "Prepaid Debit Payment",
                        "freetrial" => "Partner Free Trial",
                        "Check" => "Pay by Check",
                       ], $filters['payment_methods'] ,array('multiple'=>'multiple', 'name'=>'payment_methods[]')) }}
                </div>
            </div>
            <div class="row">
                <div class="col-md-2">
                    {{Form::submit('submit', ['class'=>'btn btn-primary'])}}
                    <a href="{{$csv_export_url}}" target="_blank">Export as CSV</a>
                </div>
            </div>
        {{ Form::close() }}
    </div>

    @if ($products->count() > 0)
        <div id="orders-list">
            <table class="results-table col-md-12">
                <thead>
                <tr>
                    <th class="">Product</th>
                    <th class="">Title</th>
                    <th class="">Sold</th>
                    <th class="">Pre-tax Revenue</th>
                    <th class="">Taxed</th>
                    <th class="">Total</th>
                </tr>
                </thead>
                <tbody>
                    @foreach ($products as $product)
                    <tr class="
                            @if ($loop->iteration  % 2 == 0)
                                even
                            @else
                                odd
                            @endif
                            ">
                        <td>{{$product->sku}}</td>
                        <td>{{$product->name}}</td>
                        <td>{{$product->sold_count}}</td>
                        <td>${{number_format($product->pre_tax_revenue,2)}}</td>
                        <td>${{number_format($product->taxed,2)}}</td>
                        <td>${{number_format($product->total,2)}}</td>
                    </tr>
                    @endforeach
                </tbody>
                <tfoot>
                <tr class="summary">
                    <td></td>
                    <td></td>
                    <td><div class="total">Total:</div>{{$products_totals['quantity']}}</td>
                    <td><div class="total">Total:</div>${{number_format($products_totals['pre_tax'],2)}}</td>
                    <td><div class="total">Total:</div>${{number_format($products_totals['taxed'],2)}}</td>
                    <td>
                        <div class="total">Total:</div>${{number_format($products_totals['total'],2)}}
                        @if ($display_shipping)
                            <div class="total">Total Shipping:</div>${{number_format($products_totals['ship'],2)}}
                            <div class="total">Total + Shipping:</div>${{number_format($products_totals['total_plus_ship'],2)}}
                        @endif
                    </td>
                </tr>
                </tfoot>
            </table>
        </div>
    @endif

    @if ($refunds->count() > 0)
        <div class="block-container">
            <div>
                <a class="toggler" data-target="refunds-list" href="">Show/hide Refunds list</a>
            </div>
            <div class="toggler-target hidden" id="refunds-list">
                <table class="results-table col-md-12">
                    <thead>
                    <tr>
                        <th class="">Order Id</th>
                        <th class="">Order state</th>
                        <th class="">Credit date</th>
                        <th class="">Paid date</th>
                        <th class="">Taxed</th>
                        <th class="">Order total</th>
                        <th class="">Amount</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($refunds as $order)
                        <tr class="
                            @if ($loop->iteration  % 2 == 0)
                                even
                            @else
                                odd
                            @endif
                                ">
                            <td>{{$order->order_id}}</td>
                            <td>{{$order->status}}</td>
                            <td>{{date('l, F j, Y', strtotime($order->refund_date))}}</td>
                            <td>{{date('l, F j, Y', strtotime($order->payment_updated_at))}}</td>
                            <td>${{number_format($order->tax_total,2)}}</td>
                            <td>${{number_format($order->order_total,2)}}</td>
                            <td>${{number_format($order->refund_amount,2)}}</td>
                        </tr>
                    @endforeach
                    </tbody>
                    <tfoot>
                    <tr class="summary">
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td><div class="total">Total:</div>${{number_format($refunds_totals['taxed'],2)}}</td>
                        <td><div class="total">Total:</div>${{number_format($refunds_totals['total'],2)}}</td>
                        <td><div class="total">Total:</div>${{number_format($refunds_totals['refund_amount'],2)}}</td>
                    </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    @endif

    @if ($orders->count() > 0)
    <div class="block-container">
        <div>
            <a class="toggler" data-target="order-list" href="">Show/hide Order list</a>
        </div>
        <div class="toggler-target hidden" id="order-list">
            <table class="results-table col-md-12">
                <thead>
                <tr>
                    <th class="">Order Id</th>
                    <th class="">Payment update date</th>
                    <th class="">Order state</th>
                    <th class="">Pre-tax Revenue</th>
                    <th class="">Taxed</th>
                    <th class="">Total</th>
                    <th class="">Total + shipp</th>
                </tr>
                </thead>
                <tbody>
                @foreach ($orders as $order)
                    <tr class="
                            @if ($loop->iteration  % 2 == 0)
                                even
                            @else
                                odd
                            @endif
                            ">
                        <td>{{$order->order_id}}</td>
                        <td>{{date('l, F j, Y', strtotime($order->payment_updated_at))}}</td>
                        <td>{{$order->status}}</td>

                        <td>${{number_format($order->order_total - $order->tax_total,2)}}</td>
                        <td>${{number_format($order->tax_total,2)}}</td>
                        <td>${{number_format($order->order_total-$order->shipping,2)}}</td>
                        <td>${{number_format($order->order_total,2)}}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
    @endif

@endsection
