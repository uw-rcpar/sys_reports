<?php

namespace App\TokenStore;

use App\Library\Model\Variables;

class TokenCache {
  public function storeTokens($access_token, $refresh_token, $expires) {
    $val['access_token'] = $access_token;
    $val['refresh_token'] = $refresh_token;
    $val['token_expires'] = $expires;

    Variables::set('outlook_token', $val);

  }

  public function clearTokens() {
    Variables::set('outlook_token', array());
  }

  public function getAccessToken() {
    // Check if tokens exist

    $tokens = Variables::get('outlook_token');

    if (empty($tokens['access_token']) ||
      empty($tokens['refresh_token']) ||
      empty($tokens['token_expires'])) {
      return '';
    }

    // Check if token is expired
    //Get current time + 5 minutes (to allow for time differences)
    $now = time() + 300;
    if ($tokens['token_expires'] <= $now) {
      // Token is expired (or very close to it)
      // so let's refresh

      // Initialize the OAuth client
      $oauthClient = new \League\OAuth2\Client\Provider\GenericProvider([
        'clientId'                => env('OAUTH_APP_ID'),
        'clientSecret'            => env('OAUTH_APP_PASSWORD'),
        'redirectUri'             => env('OAUTH_REDIRECT_URI'),
        'urlAuthorize'            => env('OAUTH_AUTHORITY').env('OAUTH_AUTHORIZE_ENDPOINT'),
        'urlAccessToken'          => env('OAUTH_AUTHORITY').env('OAUTH_TOKEN_ENDPOINT'),
        'urlResourceOwnerDetails' => '',
        'scopes'                  => env('OAUTH_SCOPES')
      ]);

      try {
        $newToken = $oauthClient->getAccessToken('refresh_token', [
          'refresh_token' => $tokens['refresh_token']
        ]);

        // Store the new values
        $this->storeTokens($newToken->getToken(), $newToken->getRefreshToken(),
          $newToken->getExpires());

        return $newToken->getToken();
      }
      catch (League\OAuth2\Client\Provider\Exception\IdentityProviderException $e) {
        return '';
      }
    }
    else {
      // Token is still valid, just return it
      return $tokens['access_token'];
    }
  }
}