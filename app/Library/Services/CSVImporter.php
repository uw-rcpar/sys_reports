<?php

namespace App\Library\Services;

use App\Library\Services\Contracts\CSVImporterInterface;

use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\File;

class CSVImporter implements CSVImporterInterface {

  /**
   * The application instance.
   *
   * @var \Illuminate\Foundation\Application
   */
  protected $app;

  /**
   * Create a new CSVImporter instance.
   *
   * @param  \Illuminate\Foundation\Application  $app
   * @return void
   */
  public function __construct($app)
  {
    $this->app = $app;
  }

  /**
   * Given the header of a CSV file (obtained by running fgetcsv function)
   * it returns an array of the column positions of the values, indexed by their
   * header label.
   * @param $csvHeader array Array with the header names of the csv ordered as they
   *  appears on the csv file.
   * @return array|bool Array with the indexes of the columns, indexed by their header label.
   */
  protected function getColumnMappingFromCSVHeader($csvHeader) {
    return array_flip($csvHeader);
  }

    /**
     * Changes the array key of the $mapping variable using the translation table
     * specified on $mappingOverrides.
     * @param $mapping array Original mapping.
     * @param $mappingOverrides array Translation table
     * (from csv column titles to actual column names in the model).
     * @return array Array with the translated column names.
     */
  protected function mergeMappings($mapping, $mappingOverrides) {
    $newMapping = [];
    foreach($mapping as $columnName => $value) {
        $key = $columnName;
        if (isset($mappingOverrides[$columnName])) {
            $key = $mappingOverrides[$columnName];
        }
        $newMapping[$key] = $value;
    }
    return $newMapping;
  }

  /**
   * {@inheritdoc}
   */
  public function countRowsOnCsv($filePath) {
      $lineCount = 0;
      $path = Storage::path($filePath);
      $handle = fopen($path, "r");
      while (!feof($handle)) {
          if (fgets($handle) !== false) {
              $lineCount++;
          }
      }
      fclose($handle);
      // We don't want to count the header of the file as a row, so we subtract
      // one to the count.
      if ($lineCount > 0) {
          $lineCount--;
      }
      return $lineCount;
  }

  /**
   * {@inheritdoc}
   */
  public function importCSV($filePath) {
    $total_rows = 0;
    $info = pathinfo($filePath);
    $fileName = $info['basename'];

    // If we have a real file, let's roll
    $csvColumnMapping = null;
    $path = Storage::path($filePath);
    if (Storage::path($filePath)) {

      // Since it exists get file path to open
      // Read file imported and save to DB using the Model
      if (($handle = fopen($path, 'r')) !== FALSE) {
        $header = TRUE;

        $modelClassName = $this->getModelClassNameFromFilename($filePath);
        if (!$modelClassName) {
            throw new \InvalidArgumentException('Model class not found for: '.$fileName);
        }
        $defaultMapping = $modelClassName::getColumnMapping();
        $rows_to_skip = $modelClassName::getRowsToSkipCount();
        while (($data = fgetcsv($handle, 10000, ',')) !== FALSE) {
          if ($rows_to_skip > 0) {
              $rows_to_skip--;
              continue;
          }
          if ($header == TRUE) {
            $csvColumnMapping = $this->getColumnMappingFromCSVHeader($data);
            // If the model class has some columns mapped in a particular way
            // we need to that into account on our mapping
            if (count($defaultMapping) > 0) {
                $csvColumnMapping = $this->mergeMappings($csvColumnMapping, $defaultMapping);
            }
            $header = FALSE;
          }
          else {
            $modelObj = new $modelClassName;
            $modelObj->updateOrCreateFromMapping($csvColumnMapping, $data);
            if  (method_exists($modelClassName, "afterImportCSVRow") ) {
              $modelClassName::afterImportCSVRow($csvColumnMapping, $data);
            }
            $total_rows++;
          }
        }
        if  (method_exists($modelClassName, "afterImportCSV") ){
          $modelClassName::afterImportCSV();
        }
        fclose($handle);
      }
      // Move processed file
      $src = $filePath;
      $dir = $this->app['config']['data_importers.imported_csv_path'] . date('Y-m');
      $dest =  $dir . '/' . $fileName . '.csv' . '-' . date('Y.m.d-H:i-T');
      if(!Storage::exists($dir)){
        $old_umask = umask(00);
        File::makeDirectory('storage/app/' . $dir, 0777, true, true);
        umask($old_umask);
      }
      if (Storage::exists($dest)) {
        Storage::delete($dest);
      }
      Storage::move($src, $dest);
    }
    return $total_rows;
  }

  /**
   * {@inheritdoc}
   */
  public function listCSVAvailableToImport() {
    $filesStr = array();
    $dir = Config::get('data_importers.csv_to_import_path');
    $dir = Storage::path($dir);
    $files = File::allFiles($dir);
    foreach ($files as $file) {
      if (File::extension($file)){
        $fileName = substr((string)File::basename($file), 0, -4);
        $filesStr[] = $fileName;
      }
    }
    return $filesStr;
  }

  /**
   * {@inheritdoc}
   */
  function getModelClassNameFromFilename($fileName) {
    $info = pathinfo($fileName);
    $fileNameNoExt = basename($fileName, '.' . $info['extension']);
    $fileName = $info['basename'];

    // Check if we have a Model class with that name.
    switch ($fileNameNoExt) {
      case 'IPQSessionTransactions':
        $modelClassName = 'IPQSessions';
        break;
      default:
        $modelClassName = $fileNameNoExt;
    }
    $modelClassName = 'App\\Library\\Model\\' . $fileNameNoExt;
    if (class_exists($modelClassName)) {
      return $modelClassName;
    }

    // The CSV sent to Outlook account have special names, let's check if this
    // is one of them.

    // Roger CPA Review_2017-02-02_2017-02-03_details.csv
    if($fileName == 'LinkConnector_Transaction_Report_All.csv') {
      return 'App\\Library\\Model\\AffiliateTransactions';
    }
    else if (preg_match('/Roger CPA Review_[0-9_-]*_details.csv$/', $fileName)) {
      return "App\\Library\\Model\\AffirmTransactions";
    }
    else if (preg_match('/Availability Summary Report - Roger CPA Review_[0-9_-]*.csv$/', $fileName)) {
        return "App\\Library\\Model\\SiteAvailabilityIssues";
    }else {
      // Allow date suffixes on all models: MyModelName-2019-08-01.csv as example
      if (preg_match("/^([a-z\s]+)-[0-9]{4}-[0-9]{2}-[0-9]{2}.csv$/i", $fileName, $matches)) {
        if (isset($matches[1])) {
          return "App\\Library\\Model\\".$matches[1];
        }
      }
    }
    // Weren't able to find a matching model class.
    return NULL;
  }

}