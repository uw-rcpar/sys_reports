<?php

namespace App\Library\Services;

use App\Library\Services\Contracts\RCPAROutlookInterface;
use App\TokenStore\TokenCache;

use Illuminate\Support\Facades\Log;
use Microsoft\Graph\Graph;
use Microsoft\Graph\Model;
use App\Exceptions\OutlookAuthException;
use Illuminate\Support\Facades\Storage;

class RCPAROutlook implements RCPAROutlookInterface {

  /**
   * The application instance.
   *
   * @var \Illuminate\Foundation\Application
   */
  protected $app;

  /**
   * Create a new RCPAROutlook instance.
   *
   * @param  \Illuminate\Foundation\Application  $app
   * @return void
   */
  public function __construct($app) {
    $this->app = $app;
  }

  /**
   * {@inheritdoc}
   */
  public function isAuthenticated() {
    if (session_status() == PHP_SESSION_NONE) {
      session_start();
    }
    $tokenCache = new TokenCache;

    if ($tokenCache->getAccessToken()) {
      return true;
    }
    else {
      return false;
    }
  }

  /**
   * Constructs an OAuth client object instantiated with the values needed to auth the app with Microsoft.
   * @return \League\OAuth2\Client\Provider\GenericProvider
   */
  protected function getOAuthClient() {
    return new \League\OAuth2\Client\Provider\GenericProvider([
        'clientId'                => env('OAUTH_APP_ID'),
        'clientSecret'            => env('OAUTH_APP_PASSWORD'),
        'redirectUri'             => env('OAUTH_REDIRECT_URI'),
        'urlAuthorize'            => env('OAUTH_AUTHORITY') . env('OAUTH_AUTHORIZE_ENDPOINT'),
        'urlAccessToken'          => env('OAUTH_AUTHORITY') . env('OAUTH_TOKEN_ENDPOINT'),
        'urlResourceOwnerDetails' => '',
        'scopes'                  => env('OAUTH_SCOPES')
      ]);
  }

  /**
   * {@inheritdoc}
   */
  public function getAuthenticationUrl() {
    if (session_status() == PHP_SESSION_NONE) {
      session_start();
    }

    // Initialize the OAuth client
    $oauthClient = $this->getOAuthClient();

    // Generate the auth URL
    $authorizationUrl = $oauthClient->getAuthorizationUrl();

    // Save client state so we can validate in response
    $_SESSION['oauth_state'] = $oauthClient->getState();

    return $authorizationUrl;
  }

  /**
   * {@inheritdoc}
   */
  public function pullTokenAfterRedirect(){
    if (session_status() == PHP_SESSION_NONE) {
      session_start();
    }

    // Authorization code should be in the "code" query param
    if (isset($_GET['code'])) {
      // Check that state matches
      if (empty($_GET['state']) || ($_GET['state'] !== $_SESSION['oauth_state'])) {
        throw new OutlookAuthException('State provided in redirect does not match expected value.', OutlookAuthException::WRONG_STATE);
      }

      // Clear saved state
      //unset($_SESSION['oauth_state']);

      $oauthClient = $this->getOAuthClient();

      try {
        // Make the token request
        $accessToken = $oauthClient->getAccessToken('authorization_code', [
          'code' => $_GET['code']
        ]);

        // Save the access token and refresh tokens in session
        // This is for demo purposes only. A better method would
        // be to store the refresh token in a secured database
        $tokenCache = new \App\TokenStore\TokenCache;
        $tokenCache->storeTokens($accessToken->getToken(), $accessToken->getRefreshToken(),
          $accessToken->getExpires());
      }
      catch (League\OAuth2\Client\Provider\Exception\IdentityProviderException $e) {
        throw new OutlookAuthException('OAuth error:'.$e->getMessage(), OutlookAuthException::OAUTH_ERROR);
      }
      catch (\Exception $e) {
        throw new OutlookAuthException('Error:'.$e->getMessage(), OutlookAuthException::GENERIC_ERROR);
      }

    }
    elseif (isset($_GET['error'])) {
      throw new OutlookAuthException('ERROR: ' . $_GET['error'] . ' - ' . $_GET['error_description'], OutlookAuthException::GENERIC_ERROR);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getLatestsAttachmentsList() {
    if (session_status() == PHP_SESSION_NONE) {
      session_start();
    }

    $attachmentsList = array();

    $tokenCache = new \App\TokenStore\TokenCache;

    $graph = new Graph();
    $graph->setAccessToken($tokenCache->getAccessToken());

    $messageQueryParams = array (
      // Only return Subject, ReceivedDateTime, and From fields
      "\$select" => "subject,receivedDateTime,from,hasAttachments",
      // Sort by ReceivedDateTime, newest first
      "\$orderby" => "receivedDateTime DESC",
      // Return at most 10 results
      "\$top" => "10"
    );

    $getMessagesUrl = '/me/mailfolders/inbox/messages?'.http_build_query($messageQueryParams);
    $messages = $graph->createRequest('GET', $getMessagesUrl)
      ->setReturnType(Model\Message::class)
      ->execute();

    $attachmentQueryParams = array (
      // We only need the filename here and the contentType
      "\$select" => "name,contentType",
    );
    Log::notice('Downloading messages. Number of messages found: '. count($messages));
    foreach($messages as $msg) {
        
      if ($msg->getHasAttachments()) {
        Log::notice('msg has attachment');
        $getAttachmentsUrl = '/me/mailFolders/inbox/messages/'.$msg->getId().'/attachments?'.http_build_query($attachmentQueryParams);
        $attachments = $graph->createRequest('GET', $getAttachmentsUrl)
          ->setReturnType(Model\FileAttachment::class)
          ->execute();
        foreach ($attachments as $attachment) {
          $attachmentsList[] = [
            'id' => $attachment->getId(),
            'fileName' => $attachment->getName(),
            'contentType' => $attachment->getContentType(),
            'mailId' => $msg->getId(),
          ];
        }
      }else{
          Log::notice('msg without attachment.. skipping');
      }
      
    }
    return $attachmentsList;
  }

  /**
   * {@inheritdoc}
   */
  public function downloadAttachment($mailId, $attachmentId, $destination) {
    $tokenCache = new \App\TokenStore\TokenCache;

    $graph = new Graph();
    $graph->setAccessToken($tokenCache->getAccessToken());

    $getAttachmentsUrl = '/me/mailFolders/inbox/messages/'.$mailId.'/attachments/'.$attachmentId;
    $attachment = $graph->createRequest('GET', $getAttachmentsUrl)
      ->setReturnType(Model\FileAttachment::class)
      ->execute();
    $fileName = $attachment->getName();
    $b64Content = $attachment->getContentBytes();

    Storage::put($destination, base64_decode($b64Content), 'public');
  }

}
