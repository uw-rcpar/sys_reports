<?php

namespace App\Library\Services\Contracts;

Interface CSVImporterInterface {


  /**
   * Count the rows inside a csv file.
   * @param $filePath File path of the csv file that contains the data.
   * @return int Number or rows on the csv file
   */
  public function countRowsOnCsv($filePath);

  /**
   * Imports the data of a csv that matches one of our Model entity classes.
   * @param $filePath File path of the csv file that contains the data.
   * @return int Number of records imported.
   * @throws \Exception when the process fails.
   */
  public function importCSV($filePath);

  /**
   * Lists all the CSV that are available for processing by the importer.
   * @return array Array with the filenames of the csv files.
   */
  public function listCSVAvailableToImport();

  /**
   * When possible, returns the Classname that should be used to load the data
   * inside the specified file, this is resolved only by looking at the filename
   * stcture, the actual file is not touched nor opened.
   * @param $fileName string Filename
   * @return mixed returns a Class name with it's namespace, or NULL when no class
   * was found.
   */
  public function getModelClassNameFromFilename($fileName);
}