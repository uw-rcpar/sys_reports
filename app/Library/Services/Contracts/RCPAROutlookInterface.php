<?php

namespace App\Library\Services\Contracts;

Interface RCPAROutlookInterface {

  /**
   * Checks if the application was authenticated with Microsoft and it is ready
   * to use it's API's
   * @return boolean True is the app is authenticated and it's current, false otherwise.
   */
  public function isAuthenticated();

  /**
   * Returns the URL that should be used to authenticate the application with Microsoft.
   * @return string URL to use to start authentication process.
   */
  public function getAuthenticationUrl();

  /**
   * Callback to be used as part of the authentication process with Microsoft,
   * Should be called when the user is redirected after the authentication
   * ($_GET['code'] and $_GET['state'] parameters should be present).
   * @return mixed
   * @throws App\Exceptions\OutlookAuthException;
   */
  public function pullTokenAfterRedirect();

  /**
   * Returns a list of the attachments files contained in the last 10 emails on
   * the associated microsoft account inbox.
   * @return array containing a list of ['id', 'fileName', 'contentType', 'mailId'].
   */
  public function getLatestsAttachmentsList();

  /**
   * Downloads a file attached to the specified email on Microsoft account inbox to the local filesystem.
   * @param $mailId Mail identifier of the email where the attachment was originally attached.
   * @param $attachmentId Id of the attachment to be downloaded.
   * @param $destination Path and filename of the destination of the download.
   * @return void.
   */
  public function downloadAttachment($mailId, $attachmentId, $destination);

}