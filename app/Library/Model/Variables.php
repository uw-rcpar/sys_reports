<?php

namespace App\Library\Model;

use Illuminate\Database\Eloquent\Model;

class Variables extends Model
{
    static function get($name) {
      $var = Variables::where('name', $name)->first();
      if (!$var) {
        return NULL;
      }
      $v = $var->value;
      return json_decode($v, TRUE);
    }

    static function set($name, $value) {
      $var = Variables::where('name', $name)->first();
      if (!$var) {
        $var = new Variables;
        $var->name = $name;
      }
      $var->value = json_encode($value);
      $var->save();
    }
}
