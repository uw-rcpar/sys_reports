<?php

namespace App\Library\Model;

use App\Library\Model\CSVImportable;

class StatsIPQChapters extends CSVImportable
{
  protected $table = 'rpt_stats_ipq_chapters';

  public $fillable = [
    'chapter_id',
    'trending_average',
    'average',
    'total_questions',
    'distinct_questions',
    'sd',
    'groupname'
  ];

  /**
   * {@inheritdoc}
   */
  public function getObjectKeyFields() {
    return ['chapter_id', 'groupname'];
  }
}
