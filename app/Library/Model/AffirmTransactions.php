<?php

namespace App\Library\Model;

use App\Library\Model\CSVImportable;

class AffirmTransactions extends CSVImportable
{

  protected $table = 'rpt_affirm_transactions';

  public $fillable = [
    'date',
    'charge_created_date',
    'charge_id',
    'transaction_id',
    'order_id',
    'sales',
    'refunds',
    'fees',
    'total_settled',
  ];

  /**
   * {@inheritdoc}
   */
  public function getObjectKeyFields() {
    return ['transaction_id'];
  }

}
