<?php

namespace App\Library\Model;

use App\Library\Model\CSVImportable;
use Illuminate\Support\Facades\DB;

class OrderTransactions extends CSVImportable
{
  protected $table = 'rpt_order_transactions';

  public $fillable = [
    'user_id',
    'mail',
    'order_id',
    'transaction_id',
    'create_date',
    'create_year',
    'create_month',
    'create_day',
    'graduated',
    'grad_date',
    'grad_year',
    'grad_month',
    'country',
    'country',
    'state',
    'st',
    'market',
    'region',
    'zip',
    'ship_state',
    'ship_zip',
    'partner_nid',
    'partner_title',
    'intacct',
    'college_state_tid',
    'college_name',
    'college_state_abbrv',
    'college_state',
    'promotion_type',
    'promotion',
    'promo_description',
    'free_trial',
    'ft_time_diff',
    'ft_time_group',
    'channel',
    'channel3',
    'lead_status_all',
    'package',
    'payment_method',
    'sub_total',
    'promo_total',
    'partner_discount_total',
    'shipping',
    'taxes',
    'order_total',
    'order_status',
    'refund_date',
    'refund_amount',
    'net_order_revenue',
    'account_id',
    'account_name',
    'office_location',
    'shipping_type',
    'employer',
    'uw_order_id',
    'order_id_both',
    'uw_sf_id',
    'uw_partner_name',
    'fice',
    'opeid'
  ];

  /**
   * {@inheritdoc}
   */
  public function getObjectKeyFields() {
    return ['user_id', 'order_id'];
  }

  /**
   * Gets a list of products and the count of items sold of that particular type
   * The list of returned items is restricted at most to the following list of products:
   *
   * AUD BEC FAR REG
   * AUD-FS BEC-FS FAR-FS REG-FS
   * AUD-PST BEC-PST FAR-PST REG-PST
   * AUD-CRAM BEC-CRAM FAR-CRAM REG-CRAM
   *
   * the SKU filter code must be one of those, otherwise an empty list will be returned
   *
   * @param string $sku SKU filter value
   * @param array $order_state Order state filter value
   * @param null $start_date Start date (formatted as 'yyyy-mm-dd') filter value
   * @param null $end_date Start date (formatted as 'yyyy-mm-dd') filter value
   * @return \Illuminate\Support\Collection List of products and sold count
   */
  public function getCAProductsSold(
    $sku = "",
    $order_state = [],
    $start_date = null,
    $end_date = null,
    $billing_type = ""
  ) {

    $query = DB::table('rpt_order_transactions AS o')
      ->select('li.name AS product_name', 'sku',
        'o.payment_method',
        'o.create_date AS payment_updated_at',
        DB::raw('SUM(quantity) AS quantity'))
      ->join('rpt_line_item_revenue AS li', 'o.order_id', '=',
        'li.order_id')
      ->join('rpt_user_profile AS cp', 'cp.uid', '=', 'o.user_id')
      ->leftJoin('rpt_partner_profile AS partner', 'o.partner_nid', '=', 'partner.nid')
      ->leftJoin('us_states AS us_state', 'us_state.full_name', '=', 'cp.ship_state')
      ->where('cp.ship_country', '=', 'United States')
      ->where('us_state.abbreviated_name', '=', 'CA')
      ->groupBy('sku')
      ->orderBy('quantity', 'desc');

    if (!empty($sku)) {
      $query->where('li.sku', '=', $sku);
    }

    if (!empty($order_state)) {
      $query->whereIn('o.order_status', $order_state);
    }

    if (!empty($start_date)) {
      $query->where(DB::raw('o.create_date'),
        '>=',
        $start_date);
    }

    if (!empty($end_date)) {
      $query->where(DB::raw('o.create_date'),
        '<=',
        $end_date);
    }

    if (!empty($billing_type)) {
      switch($billing_type) {
        case 'CC-affirm':
          $methods = array('Affirm', 'CC');
          break;
        case 'freetrial':
          $methods = array('');
          break;
        case 'prepaid':
          $methods = array('PPD CR', 'PPD DR');
          break;
        default:
          $methods = array($billing_type);
      }
      $query->whereIn('o.payment_method', $methods);
    }
    // This condition needs to be the last, otherwise the '?' character is
    // incorrectly interpreted as a replacement placeholder.
    $query->where('li.sku', 'RLIKE', '^((AUD)|(BEC)|(REG)|(FAR))(-((FS)|(PST)|(CRAM)))?$');

    $products = $query->get();
    return $products;
  }

  /**
   * Get's a list of orders that matches the filters and include at least one
   * items of the following list:
   *
   * AUD BEC FAR REG
   * AUD-FS BEC-FS FAR-FS REG-FS
   * AUD-PST BEC-PST FAR-PST REG-PST
   * AUD-CRAM BEC-CRAM FAR-CRAM REG-CRAM
   *
   * the SKU filter code must be one of those, otherwise an empty list will be returned
   *
   * @param string $sku SKU filter value
   * @param array $order_state Order state filter value
   * @param string $start_date Start date (formatted as 'yyyy-mm-dd') filter value
   * @param string $end_date Start date (formatted as 'yyyy-mm-dd') filter value
   * @return \Illuminate\Support\Collection List of orders that have a product
   * that matches the specified criteria
   */
  public function getOrdersFromCAProductsSold(
    $sku = "",
    $order_state = [],
    $start_date = null,
    $end_date = null,
    $billing_type = ""
  ) {

    $query = DB::table('rpt_order_transactions AS o')
      ->select('o.order_id', 'o.order_status as status',
        DB::raw('SUM(total) AS total'),
        DB::raw('SUM(taxed) AS taxed'),
        'o.shipping AS shipping',
        'o.payment_method',
        'o.create_date AS payment_updated_at'
      )
      ->join('rpt_line_item_revenue AS li', 'o.order_id', '=',
        'li.order_id')
      ->join('rpt_user_profile AS cp', 'cp.uid', '=', 'o.user_id')
      ->leftJoin('rpt_partner_profile AS partner', 'o.partner_nid', '=', 'partner.nid')
      ->leftJoin('us_states AS us_state', 'us_state.full_name', '=', 'cp.ship_state')
      ->where('cp.ship_country', '=', 'United States')
      ->where('us_state.abbreviated_name', '=', 'CA')
      ->groupBy('o.order_id');

    if (!empty($sku)) {
      $query->where('li.sku', '=', $sku);
    }

    if (!empty($order_state)) {
      $query->whereIn('o.order_status', $order_state);
    }

    if (!empty($start_date)) {
      $query->where(DB::raw('o.create_date'),
        '>=',
        $start_date);
    }

    if (!empty($end_date)) {
      $query->where(DB::raw('o.create_date'),
        '<=',
        $end_date);
    }

    if (!empty($billing_type)) {
      switch($billing_type) {
        case 'CC-affirm':
          $methods = array('Affirm', 'CC');
          break;
        case 'freetrial':
          $methods = array('');
          break;
        case 'prepaid':
          $methods = array('PPD CR', 'PPD DR');
          break;
        default:
          $methods = array($billing_type);
      }
      $query->whereIn('o.payment_method', $methods);
    }

    // This condition needs to be the last, otherwise the '?' character is
    // incorrectly interpreted as a replacement placeholder.
    $query->whereExists(function ($query2) {
      $query2->select('li2.order_id')
        ->from('rpt_line_item_revenue AS li2')
        ->whereRaw('li2.order_id = o.order_id')
        ->whereRaw("li2.sku RLIKE '^((AUD)|(BEC)|(REG)|(FAR))(-((FS)|(PST)|(CRAM)))?$' ");
    });

    $products = $query->get();
    return $products;
  }

  /**
   * Get's a list of orders and the refunded amount associated to them
   * that matches the filters, have at least one refund associated
   * to them and include at least one item of the following list:
   *
   * AUD BEC FAR REG
   * AUD-FS BEC-FS FAR-FS REG-FS
   * AUD-PST BEC-PST FAR-PST REG-PST
   * AUD-CRAM BEC-CRAM FAR-CRAM REG-CRAM
   *
   * the SKU filter code must be one of those, otherwise an empty list will be returned
   *
   * @param string $sku SKU filter value
   * @param array $order_state Order state filter value
   * @param string $start_date Start date (formatted as 'yyyy-mm-dd') filter value
   * @param string $end_date Start date (formatted as 'yyyy-mm-dd') filter value
   * @return \Illuminate\Support\Collection List of orders that have a product
   * that matches the specified criteria
   */
  public function getOrdersRefundsFromCAProductsSold(
    $sku = "",
    $order_state = [],
    $start_date = null,
    $end_date = null,
    $billing_type = ""
  ) {
    $query = DB::table('rpt_order_transactions AS o')
      ->select('o.order_id', 'o.order_status AS status',
        DB::raw('SUM(total) AS total'),
        DB::raw('SUM(taxed) AS taxed'),
        DB::raw('SUM(refund_amount) AS refunded')
      )
      ->join('rpt_line_item_revenue AS li', 'o.order_id', '=',
        'li.order_id')
      ->join('rpt_user_profile AS cp', 'cp.uid', '=', 'o.user_id')
      ->leftJoin('rpt_partner_profile AS partner', 'o.partner_nid', '=', 'partner.nid')
      ->leftJoin('us_states AS us_state', 'us_state.full_name', '=', 'cp.ship_state')
      ->where('cp.ship_country', '=', 'United States')
      ->where('us_state.abbreviated_name', '=', 'CA')
      ->groupBy('o.order_id');

    if (!empty($sku)) {
      $query->where('li.sku', '=', $sku);
    }

    if (!empty($order_state)) {
      $query->whereIn('o.order_status', $order_state);
    }

    if (!empty($start_date)) {
      $query->where(DB::raw('o.create_date'), '>=', $start_date);
    }

    if (!empty($end_date)) {
      $query->where(DB::raw('o.create_date'), '<=', $end_date);
    }

    if (!empty($billing_type)) {
      switch($billing_type) {
        case 'CC-affirm':
          $methods = array('Affirm', 'CC');
          break;
        case 'freetrial':
          $methods = array('');
          break;
        case 'prepaid':
          $methods = array('PPD CR', 'PPD DR');
          break;
        default:
          $methods = array($billing_type);
      }
      $query->whereIn('o.payment_method', $methods);
    }

    // This condition needs to be the last, otherwise the '?' character is
    // incorrectly interpreted as a replacement placeholder.
    $query->whereExists(function ($query2) {
      $query2->select('li2.order_id')
        ->from('rpt_line_item_revenue AS li2')
        ->whereRaw('li2.order_id = o.order_id')
        ->whereRaw("li2.sku RLIKE '^((AUD)|(BEC)|(REG)|(FAR))(-((FS)|(PST)|(CRAM)))?$' ");
    });

    $products = $query->get();
    return $products;
  }

  /**
   * Get's a list of orders that matches the filters and information about its
   * total, shipping and per product price amounts.
   *
   * @param string $sku SKU filter value
   * @param array $order_state Order state filter value
   * @param string $start_date Start date (formatted as 'yyyy-mm-dd') filter value
   * @param string $end_date Start date (formatted as 'yyyy-mm-dd') filter value
   * @param array $payment_methods Payment methods associated to the orders
   * @param array $product_list List of products for which specific information should be added
   *  (in the format of ["SKU" => "Product Name"]
   * @param boolean $pagination Indicates if the results should show one page
   *  or the full results list (default true, paginated results).
   * @return \Illuminate\Support\Collection
   */
  public function getOrdersWithProductInfo(
    $state = "",
    $order_state = [],
    $start_date = null,
    $end_date = null,
    $payment_methods = [],
    $product_list = [],
    $pagination = true
  ) {
    $query = DB::table('rpt_order_transactions AS o')
      ->select(
        'o.order_id', 'o.order_status AS status', 'o.create_date AS created_date', 'o.order_total AS total',
        'us_state.abbreviated_name AS state', 'cp.ship_city AS city',
        'cp.ship_zip AS postal_code', 'cp.first_name AS customer_first_name',
        'cp.last_name AS customer_last_name',
        DB::raw('SUM(taxed) AS taxed'),
        'o.shipping AS shipping'
      )
      ->join('rpt_line_item_revenue AS li', 'o.order_id', '=',
        'li.order_id')
      ->join('rpt_user_profile AS cp', 'cp.uid', '=', 'o.user_id')
      ->leftJoin('rpt_partner_profile AS partner', 'o.partner_nid', '=', 'partner.nid')
      ->leftJoin('us_states AS us_state', 'us_state.full_name', '=', 'cp.ship_state')
      ->where('cp.ship_country', '=', 'United States')
      ->groupBy('o.order_id')
      ->orderBy('city', 'asc');


    if (!empty($order_state)) {
      $query->whereIn('o.order_status', $order_state);
    }

    if (!empty($state)) {
      $query->where('us_state.abbreviated_name', '=', $state);
    }

    if (!empty($start_date)) {
      $query->where(DB::raw('o.create_date'), '>=', $start_date);
    }

    if (!empty($end_date)) {
      $query->where(DB::raw('o.create_date'), '<=', $end_date);
    }

    if (!empty($payment_methods)) {
      $query->whereIn('o.payment_method', $payment_methods);
    }

    if ($pagination) {
      $orders = $query->paginate(50);
    } else {
      $orders = $query->get();
    }

    foreach ($orders as $o) {
      foreach (array_keys($product_list) as $prod) {
        $prod_query = DB::table('rpt_line_item_revenue AS li')
          ->select(DB::raw('total AS total'))
          ->where('li.order_id', '=', $o->order_id)
          ->where('li.sku', '=', $prod);
        $prod_r = $prod_query->get();
        $o->$prod = 0;
        if (!$prod_r->isEmpty()) {
          $o->$prod = $prod_r->first()->total;
        }
      }
    }
    return $orders;
  }




  /**
   *
   * Gets a list of order that have Intacct ID set and for a given time frame, order status.
   *
   * @param string $month
   * @param string $year
   * @param string $order_status
   * @return \Illuminate\Support\Collection
   */
  public function getIntacctOrders(
    $month = null,
    $year = null,
    $order_status = ""
  ) {

    $query = DB::table('rpt_order_transactions AS o')
      ->select('o.intacct AS Intacct_Customer_ID',
      DB::raw('null'),
      'p.partner_terms AS Terms',
      DB::raw('null'),
      'o.order_total AS Total_Amount',
      DB::raw('null'),
      DB::raw('null'),
      DB::raw('null'),
      DB::raw('(SELECT GROUP_CONCAT(sku ORDER BY sku SEPARATOR ", ") FROM rpt_line_item_revenue WHERE order_id = o.order_id) AS Course_Product'),
      DB::raw('null'),
      'o.net_order_revenue AS Sales_Price',
      'o.shipping AS Shipping',
      'o.taxes AS Taxes',
      'create_date AS Order_Date',
      DB::raw('CONCAT(up.first_name, " ", up.last_name) AS Student_Name'),
      'o.office_location AS Office_Location',
      DB::raw('REPLACE(o.shipping_type, "&#039;", "\'") AS Shipping_Via'),
      'o.ship_state AS Shipping_State',
      'o.free_trial',
      'o.order_id'
       )
      ->leftJoin('rpt_user_profile AS up', 'up.uid', '=', 'o.user_id')
      ->leftJoin('rpt_partner_profile AS p', 'o.partner_nid', '=', 'p.nid')
      ->whereNotNull('o.intacct')
      ->where('o.intacct', '<>', '', 'and')
      ->orderBy('create_date', 'desc');

    if (!empty($order_status)) {
      $query->whereIn('o.order_status', $order_status);
    }

    if (!empty($month)) {
      $query->where(DB::raw('o.create_month'),'=', $month);
    }

    if (!empty($year)) {
      $query->where(DB::raw('o.create_year'), '=', $year);
    }

    //DB::enableQueryLog();
    $products = $query->get();
    //dd(DB::getQueryLog());

    return $products;
  }

  /**
   * Gets a list of cities of the selected state for which we have orders
   * after filtering as specified by the parameters.
   *
   * @param string $start_date Start date (formatted as 'yyyy-mm-dd') filter value
   * @param string $end_date Start date (formatted as 'yyyy-mm-dd') filter value
   * @param string $state Abbreviated state name
   * @param array $order_state Order state filter value
   * @param array $payment_methods Payment methods associated to the orders
   * @return \Illuminate\Support\Collection
   */
  public function getTaxesByCity(
    $start_date = null,
    $end_date = null,
    $state = '',
    $order_state = [],
    $payment_methods = []
  ) {

    if (!is_array($payment_methods)){
          $payment_methods = [] ;
    }
    // The actual value that we need to search when looking for freetrial is ''
    $payment_methods = array_replace($payment_methods,
      array_fill_keys(array_keys($payment_methods, 'freetrial'), '')
    );
    $query = DB::table('rpt_order_transactions AS o')
      ->select(
        'cp.ship_city AS city_name',
         DB::raw("GROUP_CONCAT(DISTINCT o.order_id ORDER BY o.order_id ASC separator ',') AS order_ids")
      )->join('rpt_user_profile AS cp', 'cp.uid', '=', 'o.user_id')
      ->leftJoin('rpt_partner_profile AS partner', 'o.partner_nid', '=', 'partner.nid')
      ->leftJoin('us_states AS us_state', 'us_state.full_name', '=', 'cp.ship_state')
      ->groupBy('cp.ship_city');
    $query->where('cp.ship_country', '=', 'United States');

    if (!empty($order_state)) {
      $query->whereIn('o.order_status', $order_state);
    }

    if (!empty($start_date)) {
      $query->where(DB::raw('o.create_date'), '>=', $start_date);
    }

    if (!empty($end_date)) {
      $query->where(DB::raw('o.create_date'), '<=', $end_date);
    }

    if (empty($state)) {
      throw new InvalidArgumentException('state parameter can\'t be null');
    }
    $query->where('us_state.abbreviated_name', '=', $state);

    if (!empty($payment_methods)) {
      $query->whereIn('o.payment_method', $payment_methods);
    }
    $orders = $query->get();
    return $orders;
  }

  /**
   * Get's a list of orders and the refunded amount associated to them
   * that matches the filters and have at least one refund associated
   * to them.
   *
   * @param string $start_date Start date (formatted as 'yyyy-mm-dd') filter value
   * @param string $end_date Start date (formatted as 'yyyy-mm-dd') filter value
   * @param string $state Abbreviated state name
   * @param array $order_state Order state filter value
   * @param array $payment_methods Payment methods associated to the orders
   * @return \Illuminate\Support\Collection
   */
  public function getRefundsForTaxesByCity(
    $start_date = null,
    $end_date = null,
    $state = '',
    $order_state = [],
    $payment_methods = []
  ) {
    // TODO: Unify getRefundsForTaxesByCity, getOrdersRefundsFromCAProductsSold
    // and OrderLine::getRefundsForProductSoldDetailed into a single function.

    // The actual value that we need to search when looking for freetrial is ''
    $payment_methods = array_replace($payment_methods,
      array_fill_keys(array_keys($payment_methods, 'freetrial'), '')
    );

    $query = DB::table('rpt_order_transactions AS o')
      ->select(
        'o.order_id', 'o.order_status AS status', 'o.order_total AS order_total',
        'o.shipping AS shipping_total', 'o.taxes AS tax_total',
        'o.create_date AS payment_updated_at', 'o.refund_date', 'o.refund_amount', DB::raw('SUM(taxable) AS taxable')
      )
      ->leftJoin('rpt_line_item_revenue AS li', 'o.order_id', '=', 'li.order_id')
      ->join('rpt_user_profile AS cp', 'cp.uid', '=', 'o.user_id')
      ->leftJoin('rpt_partner_profile AS partner', 'o.partner_nid', '=', 'partner.nid')
      ->leftJoin('us_states AS us_state', 'us_state.full_name', '=', 'cp.ship_state')
      ->groupBy('o.order_id');

    $query->where('cp.ship_country', '=', 'United States');

    // We only want to pull orders with refunds on this query
    $query->where('o.refund_amount', '<>', 0);

    if (!empty($order_state)) {
      $query->whereIn('o.order_status', $order_state);
    }

    if (!empty($start_date)) {
      $query->where(DB::raw('o.refund_date'), '>=', $start_date);
    }

    if (!empty($end_date)) {
      $query->where(DB::raw('o.refund_date'), '<=', $end_date);
    }

    if (empty($state)) {
      throw new InvalidArgumentException('state parameter can\'t be null');
    }
    $query->where('us_state.abbreviated_name', '=', $state);

    if (!empty($payment_methods)) {
      $query->whereIn('o.payment_method', $payment_methods);
    }
    $orders = $query->get();
    return $orders;
  }

  /**
   * Get the taxable, taxed, order total and some other columns for the specified orders.
   * @param $order_ids array of order ids
   * @return \Illuminate\Support\Collection
   */
  public function getTaxesInfo($order_ids) {
    $query = DB::table('rpt_order_transactions AS o')
      ->select(
        'o.order_id', 'o.order_total', 'o.order_status',
        'o.shipping', 'o.taxes',
        DB::raw('SUM(taxable) AS taxable'),
        'o.create_date'
      )
      ->join('rpt_line_item_revenue AS li', 'o.order_id', '=', 'li.order_id')
      ->whereIn('o.order_id', $order_ids)
      ->groupBy('o.order_id');
    $orders = $query->get();
    return $orders;
  }

    /**
     * Reads the shipping for the order
     * @param string $sku
     * @param null $start_date
     * @param null $end_date
     * @param string $country
     * @param array $payment_methods
     * @param array $excluded_product_list
     * @return \Illuminate\Support\Collection
     */
    public function getProductSoldTotalShipping(
        $sku = "",
        $start_date = null,
        $end_date = null,
        $country = '',
        $payment_methods = [],
        $excluded_product_list = []
    ) {

        if (!is_array($payment_methods)){
            $payment_methods = [] ;
        }
        // The actual value that we need to search when looking for freetrial is ''
        $payment_methods = array_replace($payment_methods,
            array_fill_keys(array_keys($payment_methods, 'freetrial'), 'FT')
        );
        $query = DB::table('rpt_order_transactions AS o')
            ->select(
                DB::raw('shipping')
            )
            ->leftJoin('rpt_line_item_revenue AS li', 'o.order_id', '=', 'li.order_id')
            ->leftJoin('rpt_user_profile AS cp', 'cp.uid', '=', 'o.user_id')
            ->groupBy('o.order_id')
            ->where('o.order_status', '<>', 'canceled');

        if (!empty($sku)) {
            $query->where('li.sku', '=', $sku);
        }

        if (!empty($start_date)) {
            $query->where(DB::raw('o.create_date'), '>=', $start_date);
        }

        if (!empty($end_date)) {
            $query->where(DB::raw('o.create_date'), '<=', $end_date);
        }

        if (!empty($country)) {
            if ($country == "US") {
                $query->where('cp.ship_country', '=', 'United States');
            }
            else {
                $query->where('cp.ship_country', '<>', 'United States');
            }
        }

        if (!empty($payment_methods)) {
            $query->whereIn('o.payment_method', $payment_methods);
        }

        // Filter products
        if (!empty($excluded_product_list)){
            $query->whereNotIn('li.sku', $excluded_product_list);
        }


        $results = $query->get();
        $total = 0 ;
        foreach ($results as $row) {
            $total+= $row->shipping;
        }
        return $total ;
    }
}
