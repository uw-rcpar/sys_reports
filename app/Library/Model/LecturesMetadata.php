<?php

namespace App\Library\Model;

use App\Library\Model\CSVImportable;

class LecturesMetadata extends CSVImportable {
  protected $table = 'rpt_lecture_metadata';

  public $fillable = [
    'topic_nid',
    'year',
    'section',
    'topic_title',
    'videos_total_duration',
    'videos_count',
  ];

  /**
   * {@inheritdoc}
   */
  public function getObjectKeyFields() {
    return ['topic_nid'];
  }
}
