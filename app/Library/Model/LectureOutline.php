<?php

namespace App\Library\Model;

use App\Library\Model\CSVImportable;

use Illuminate\Database\Eloquent\Model;

class LectureOutline extends CSVImportable
{
  protected $table = 'rpt_lecture_outline';

  public $fillable = [
    'version',
    'course_type_id',
    'course_type',
    'section_id',
    'section',
    'section_title',
    'chapter_id',
    'chapter_title',
    'topic_id',
    'topic_title',
    'weight',
    'chapter_weight',
    'video_id',
    'video_title',
    'video_duration',
    'video_duration_sec',
    'video_duration_ms'
  ];

  /**
   * {@inheritdoc}
   */
  public function getObjectKeyFields() {
    return ['version', 'video_id'];
  }
}

