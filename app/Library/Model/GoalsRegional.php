<?php

namespace App\Library\Model;

use App\Library\Model\CSVImportable;

class GoalsRegional extends CSVImportable
{
  protected $table = 'goals_regional';

  public $fillable = [
    'unique_id',
    'year',
    'market_name',
    'rev_goal',
    'pack_goal',
  ];

  /**
   * {@inheritdoc}
   */
  public function getObjectKeyFields() {
    return ['unique_id'];
  }

}
