<?php

namespace App\Library\Model;

use App\Library\Model\CSVImportable;
use Illuminate\Support\Facades\DB;

class AvataxTransactions extends CSVImportable {

  protected $table = 'rpt_avatax_transactions';

  public $fillable = [
      'Country',
      'CompanyId',
      'DocumentId',
      'DocumentCode',
      'DocType',
      'DocumentDate',
      'TaxCalcDate',
      'ReportingDate',
      'PaymentDate',
      'ExchangeRateEffDate',
      'ExchangeRate',
      'CurrencyCode',
      'CustomerVendorCode',
      'EntityUseCodeDocumentHeader',
      'PurchaseOrderNo',
      'ReferenceCode',
      'SalespersonCode',
      'LocationCode',
      'LineNo',
      'ItemCode',
      'ItemCodeDescription',
      'TaxCode',
      'TaxCodeDescription',
      'RateType',
      'Quantity',
      'LineAmount',
      'DiscountAmount',
      'LineTaxableAmount',
      'Ref1',
      'Ref2',
      'RevenueAccount',
      'LineExemptAmount',
      'EntityUseCodeDocumentLine',
      'ExemptionNo',
      'IsSSTP',
      'IsTaxable',
      'OriginStreet',
      'OriginCity',
      'OriginStateProvince',
      'OriginCountry',
      'OriginPostalCode',
      'BusinessIdentificationId',
      'DestStreet',
      'DestCity',
      'DestStateProvince',
      'DestCountry',
      'DestPostalCode',
      'LineTaxDetailId',
      'JurisdictionTypeDescription',
      'TaxTypeDescription',
      'StateAssignedNo',
      'Sourcing',
      'JurisdictionDescription',
      'InState',
      'StateProvince',
      'JurisdictionTypeTaxableAmount',
      'JurisdictionTypeExemptAmount',
      'ExemptReason',
      'NonTaxableAmount',
      'NonTaxableReason',
      'NonTaxableRuleDescription',
      'OverrideAmount',
      'TaxAmount',
      'TaxCalculated',
      'TaxVariance',
      'Rate',
      'ECMSCertId'
  ];

  /**
   * {@inheritdoc}
   */
  public function getObjectKeyFields() {
    return [
        'DocumentCode',
        'ItemCode',
        'JurisdictionTypeDescription',
        'JurisdictionDescription'
    ];
  }


  /**
   * Implementation of afterImportCSV event
   * Execute post-processing code after avatax transactions are imported
   * @param AvataxTransactions $firstInsertedRow
   */
  public static function afterImportCSVRow($csvColumnMapping, $data) {
    $avataxDocumentCode = $data[$csvColumnMapping['DocumentCode']];
    $orderId = intval(substr($avataxDocumentCode, 3)); // DC-999581 => 999581
    // Validations
    if (!$data) {
      echo "Error data found\n";
      return;
    }
    if (!$avataxDocumentCode) {
      echo "Error: avatax DocumentCode not found\n";
      return;
    }

    $insertSql = "
        INSERT into avatax_summary (order_id, sku, taxable, tax, total) (
        SELECT
          substr(a.DocumentCode,4) AS order_id,
          a.ItemCode AS sku,
          if((a.LineTaxableAmount > '0.00'),a.LineTaxableAmount, a.NonTaxableAmount) AS taxable,
          round(sum(a.TaxAmount),2) AS tax,
          round((if((a.LineTaxableAmount > '0.00'),a.LineTaxableAmount,a.NonTaxableAmount) + round(sum(a.TaxAmount),2)),2) AS total
        FROM rpt_avatax_transactions a
        WHERE a.DocumentCode = '" . $avataxDocumentCode . "'
        GROUP BY a.DocumentCode,a.ItemCode
    )
  
    ON DUPLICATE KEY UPDATE  
      taxable = VALUES(taxable),
      tax = VALUES(tax),
      total = VALUES(total) 
    ";

    DB::insert($insertSql);

    $updateSql = "
        UPDATE rpt_line_item_revenue target
               INNER JOIN avatax_summary src ON src.order_id = target.order_id AND src.sku = target.sku 
        SET
             target.taxable = src.taxable,
             target.taxed = src.tax,
             target.total = src.total
        WHERE src.order_id = $orderId
    ";
    DB::update($updateSql);
  }
}