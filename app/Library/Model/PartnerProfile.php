<?php

namespace App\Library\Model;

use App\Library\Model\CSVImportable;
use Illuminate\Support\Facades\DB;

class PartnerProfile extends CSVImportable
{
    protected $table = 'rpt_partner_profile';

    public $fillable = [
      'nid',
      'account_id',
      'account_name',
      'partner_name',
      'abbreviated_name',
      'billing_type',
      'intacct_cid',
      'partner_type',
      'lead_status',
      'account_executive_uid',
      'description',
      'partner_terms',
      'terms_description',
      'act_access',
      'monitoring_access',
      'monitoring_uid',
      'archive_pid',
      'fixed_expiration',
      'course_section_full',
      'course_section_aud',
      'course_section_bec',
      'course_section_far',
      'course_section_reg',
      'general_discount',
      'aud_discount',
      'bec_discount',
      'far_discount',
      'reg_discount',
      'bundled_full',
      'bundled_6m_ext',
      'bundled_offline_full',
      'bundled_mobile_download',
      'bundled_audio_full',
      'bundled_flashcard_full',
      'bundled_cram_full',
      'bundled_cram_part_cert',
      'bundled_flashcard_part',
      'bundled_audio_part',
      'bundled_price',
      'email_validation',
      'exempt_sales_tax',
      'flatrate_shipping',
      'shipping_rate',
      'partial_shipping_rate',
      'international',
      'show_price',
      'show_retail_price',
      'shipping_flow',
      'status',
      'author_uid',
    ];

    /**
     * {@inheritdoc}
     */
    public function getObjectKeyFields() {
        return ['nid'];
    }

    /**
     * Gets information about the users related to this partner (users that
     * were registered via this partner)
     *
     * @return \Illuminate\Support\Collection
     */
    function getAssociatedUsers() {
        $query = DB::table('rpt_user_profile AS u')
          ->select('u.id', 'u.uid', 'u.registration_date', 'u.first_name', 'u.last_name',
            'u.mail',
            'u.roles'
          )
          ->join('rpt_order_transactions AS o', 'o.user_id', '=', 'u.uid')
          ->where('o.partner_nid', '=', $this->nid)
          ->groupBy('u.uid');
        $results = $query->paginate(50);
        return $results;
    }
}
