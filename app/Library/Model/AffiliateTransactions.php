<?php

namespace App\Library\Model;

use App\Library\Model\CSVImportable;

class AffiliateTransactions extends CSVImportable {
  protected $table = 'rpt_affiliate_transactions';

  public $fillable = [
    'OriginalDate',
    'FundedDate',
    'Affiliate',
    'Campaign',
    'EventType',
    'Amount',
    'Commission',
    'LCFees',
    'TotalCost',
    'OrderID',
    'MTID',
    'Status',
    'Reason'
  ];

  /**
   * {@inheritdoc}
   */
  public function getObjectKeyFields() {
    return ['OrderID', 'Reason'];
  }

  /**
   * @param $value
   */
  public function setAmountAttribute($value) {
    $this->attributes['Amount'] = str_ireplace('$', '', $value);
  }

  /**
   * @param $value
   */
  public function setCommissionAttribute($value) {
    $this->attributes['Commission'] = str_ireplace('$', '', $value);
  }

  /**
   * @param $value
   */
  public function setLCFeesAttribute($value) {
    $this->attributes['LCFees'] = str_ireplace('$', '', $value);
  }

  /**
   * @param $value
   */
  public function setTotalCostAttribute($value) {
    $this->attributes['TotalCost'] = str_ireplace('$', '', $value);
  }

  /**
   * @param $value
   */
  public function setOriginalDateAttribute($value) {
    $this->attributes['OriginalDate'] = date_format(date_create($value), 'Y-m-d H:i:s');
  }

  /**
   * @param $value
   */
  public function setFundedDateAttribute($value) {
    $this->attributes['FundedDate'] = date_format(date_create($value), 'Y-m-d H:i:s');
  }
}
