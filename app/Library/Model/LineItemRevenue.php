<?php

namespace App\Library\Model;

use App\Library\Model\CSVImportable;

class LineItemRevenue extends CSVImportable
{
    protected $table = 'rpt_line_item_revenue';

    public $fillable = [
      'order_id',
      'line_item_id',
      'quantity',
      'revenue',
    ];

    /**
     * {@inheritdoc}
     */
    public function getObjectKeyFields() {
        return ['order_id', 'line_item_id'];
    }
}
