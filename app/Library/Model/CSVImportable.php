<?php

namespace App\Library\Model;

use Illuminate\Database\Eloquent\Model;

abstract class CSVImportable extends Model {

  /**
   * This method should the main key (possible composite by several field)
   * for the entity type.
   * @return array of fields names (eg:  ['create_day','total_count'])
   */
  abstract public function getObjectKeyFields();

  /**
   * This function will create or update an element in the Model table, using the
   * $csvColumnMapping to assign the values from $csvRowData to it's corresponding fields.
   *
   * @param $csvColumnMapping array An array of elements in the form:
   *    ['field_name_1' => 0, 'field_name_2' => 1 ... ] where field_name's are
   *    the csv's headers names for the corresponding column indexes
   * @param $csvRowData array An array of values in the form:
   *    [0 => 'value1', 1 => 'value2' ... ] representing a row on the csv that's being processed.
   * @return void
   */
  public function updateOrCreateFromMapping($csvColumnMapping, $csvRowData) {
    $keyFields = $this::getObjectKeyFields();

    $keysArray = [];
    foreach($keyFields as $key) {
      $keysArray[$key] = $csvRowData[$csvColumnMapping[$key]];
    }
    $valuesArray = [];
    foreach ($csvColumnMapping as $key => $index) {
      // For some column types the 'NULL' string is understood as a literal value
      // to avoid that, we cast it to the NULL constant here.
      if ($csvRowData[$index] == 'NULL') {
          $valuesArray[$key] = NULL;
      }
      else {
          $valuesArray[$key] = $csvRowData[$index];
      }
    }
    self::updateOrCreate($keysArray, $valuesArray);
  }

  /**
   * Returns an array with the mapping that should be used by the default csv
   * to load the data for the current model. If a column is not present in the mapping
   * the title on the column will be used as the model column (returning an empty array
   * will use all the csv column titles to map the columns.
   *
   * @return array a "csv title" => "model column" set of values used to map the default csv
   * to load this model class.
   */
  public static function getColumnMapping() {
      return [];
  }

  /**
   * Some csv files starts with rows that we need to ignore, this function
   * returns the number of trailing rows that needs to be ignored before the data starts
   * (eg SiteAvailabilityIssues.csv files)
   */
  public static function getRowsToSkipCount() {
      return 0;
  }

  /**
   * Event hadler after bulkImports
   * Override this method to exectue stuff after a Model.csv is imported using CSVImported class
   */
  public static function afterImportCSV() {}

    /**
     *  Event hadler after csvRow is imported
     *  Override this method to exectue stuff after a Model.csv is imported using CSVImported class
     *
     * @param $csvRow
     */
  public static function afterImportCSVRow($csvColumnMapping, $data ) {}
}