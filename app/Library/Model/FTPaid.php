<?php

namespace App\Library\Model;

use App\Library\Model\CSVImportable;

class FTPaid extends CSVImportable
{
  protected $table = 'rpt_free_trial_2purchase';
  
  public $fillable = [
    'user_id',
    'mail',
    'order_id',
    'total_purchased_order_count',
    'date_first_ft',
    'first_purchase_date',
    'ft_time_diff',
    'ft_time_group'
  ];

  /**
   * {@inheritdoc}
   */
  public function getObjectKeyFields() {
    return ['user_id'];
  }
}
