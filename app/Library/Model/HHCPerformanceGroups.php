<?php

namespace App\Library\Model;

use App\Library\Model\CSVImportable;

class HHCPerformanceGroups extends CSVImportable
{
    protected $table = 'rpt_hhc_performance_user_groups';

    public $fillable = [
        'uid',
        'count',
        'user_group',
    ];

    /**
     * {@inheritdoc}
     */
    public function getObjectKeyFields() {
        return ['uid'];
    }
}
