<?php

namespace App\Library\Model;

use App\Library\Model\CSVImportable;

class LectureHistory extends CSVImportable {
    protected $table = 'rpt_lecture_history';

    public $fillable = [
      'entitlement_nid',
      'viewed'
    ];

    /**
     * {@inheritdoc}
     */
    public function getObjectKeyFields() {
        return ['entitlement_nid'];
    }
}
