<?php

namespace App\Library\Model;

use App\Library\Model\CSVImportable;

class DiscountVerification extends CSVImportable
{
  protected $table = 'rpt_discount_verifications';

  public $fillable = [
    'uid',
    'coupon_code',
    'discount_type',
    'uploaded_fid',
    'previous_course',
    'status',
    'used',
    'deleted',
    'drupal_created_at',
    'drupal_updated_at',
  ];

  /**
   * {@inheritdoc}
   */
  public function getObjectKeyFields() {  
    return ['uid', 'drupal_created_at'];
  }
}
