<?php

namespace App\Library\Model;

use App\Library\Model\CSVImportable;

class NASBAFirstTimeCandidate extends CSVImportable
{
  protected $table = 'rpt_nasba_firsttime_candidates';

  public $fillable = [
    'year',
    'state_abbrev',
    'state_univ',
    'cohort',
    'juris_cohort',
    'market',
    'region',
    'univ_sections',
    'univ_ft_sections'
  ];

  /**
   * {@inheritdoc}
   */
  public function getObjectKeyFields() {
    return ['year', 'state_abbrev', 'market', 'region'];
  }
}
