<?php

namespace App\Library\Model;

use App\Library\Model\CSVImportable;

class SiteAvailabilityIssues extends CSVImportable
{
    protected $table = 'rpt_site_availability_issues';

    public $fillable = [
      'monitor',
      'start_time',
      'end_time',
      'duration',
      'comments'
    ];

    /**
     * {@inheritdoc}
     */
    public function getObjectKeyFields() {
        return ['start_time'];
    }

    /**
     * {@inheritdoc}
     */
    public static function getColumnMapping() {
        return [
          'Monitor' => 'monitor',
          'Start Time' => 'start_time',
          'End Time' => 'end_time',
          'Duration - Data' => 'duration',
          'Comments' => 'comments',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public static function getRowsToSkipCount() {
        return 6;
    }
}
