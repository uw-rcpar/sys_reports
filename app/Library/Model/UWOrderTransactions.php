<?php

namespace App\Library\Model;

use App\Library\Model\CSVImportable;


class UWOrderTransactions extends CSVImportable {
  //
  protected $table = 'rpt_uw_order_transactions';

  public $fillable = [
      'order_id',
      'user_id',
      'email_id',
      'created_date',
      'created_day',
      'created_month',
      'created_year',
      'grad_date',
      'grad_month',
      'grad_year',
      'ubi_zip_code',
      'ubi_state_id',
      'ubi_state_name',
      'ubi_state_abbrev',
      'ubi_country_id',
      'ubi_country_name',
      'ubi_market',
      'ubi_region',
      'usi_zip_code',
      'usi_state_id',
      'usi_state_name',
      'usi_state_abbrev',
      'usi_country_id',
      'usi_country_name',
      'usi_market',
      'usi_region',
      'partner_id',
      'partner_name',
      'partner_zip_code',
      'partner_state_id',
      'partner_state_name',
      'partner_state_abbrev',
      'partner_country_id',
      'partner_country_name',
      'partner_market',
      'partner_region',
      'user_school_id',
      'user_school_zip_code',
      'user_school_state_id',
      'user_school_state_name',
      'user_school_state_abbrev',
      'user_school_country_id',
      'user_school_country_name',
      'user_school_market',
      'user_school_region',
      'channel',
      'channel3',
      'lead_status_all',
      'payment_method',
      'card_type',
      'transaction_type',
      'sub_total',
      'order_total',
      'promo_total',
      'partner_discount_total',
      'credit_amount',
      'shipping',
      'sales_tax',
      'credit_sales_tax',
      'created_by',
      'user_school_name',
      'response_code',
      'purchase_type',
      'sf_id_school_id',
      'transaction_id',
      'date_payment_received',
      'transaction_type_id',
      'sales_user_id',
      'invoice_type',
      'internal_sales_user_id',
      'invoice_number',
      'invoice_net_duration',
      'invoice_category'
  ];


  /**
   * {@inheritdoc}
   */
  public function getObjectKeyFields() {
    return ['order_id'];
  }

}
