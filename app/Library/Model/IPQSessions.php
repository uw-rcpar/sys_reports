<?php

namespace App\Library\Model;

use App\Library\Model\CSVImportable;

class IPQSessions extends CSVImportable {
  protected $table = 'rpt_ipq_sessions';

  public $fillable = [
    'uid',
    'section',
    'quiz_type',
    'total_quizes_finished',
    'avg_time_finished',
    'avg_perc_complete_finished',
    'avg_perc_correct_finished',
    'first_date_finished',
    'last_date_finished',
    'total_quizes_unfinished',
    'avg_time_unfinished',
    'avg_perc_complete_unfinished',
    'avg_perc_correct_unfinished',
    'first_date_unfinished',
    'last_date_unfinished',
    'entitlement_id',
    'total_question_points',
    'total_obtained_points',
    'mcq_question_score',
    'mcq_question_count',
    'mcq_distinct_question_count',
    'drs_question_score',
    'drs_question_count',
    'drs_distinct_question_count',
    'research_question_score',
    'research_question_count',
    'research_distinct_question_count',
    'tbs_form_question_score',
    'tbs_form_question_count',
    'tbs_form_distinct_question_count',
    'tbs_journal_question_score',
    'tbs_journal_question_count',
    'tbs_journal_distinct_question_count',
    'regular_tbs_question_score',
    'regular_tbs_question_count',
    'regular_tbs_distinct_question_count',
    'wc_question_score',
    'wc_question_count',
    'wc_distinct_question_count',
  ];

  /**
   * {@inheritdoc}
   */
  public function getObjectKeyFields() {
    return ['uid', 'section', 'entitlement_id', 'quiz_type'];
  }

  /**
   * @param $value
   */
  /* public function setfirstDateFinishedAttribute($value) {
    if ($value != NULL || $value = 0) {
      $this->attributes['first_date_finished'] = date_format($value, 'Y-m-d H:i:s');
    }
  }*/

  /**
   * @param $value
   */
  /*public function setlastDateFinishedAttribute($value) {
    if ($value != NULL || $value = 0) {
      $this->attributes['last_date_finished'] = date_format(date_create($value), 'Y-m-d H:i:s');
    }
  }*/

  /**
   * @param $value
   */
  /*public function setfirstDateUnfinishedAttribute($value) {
    if ($value != NULL || $value = 0) {
      $this->attributes['first_date_unfinished'] = date_format(date_create($value), 'Y-m-d H:i:s');
    }
  }*/

  /**
   * @param $value
   */
  /*public function setlastDateUnfinishedAttribute($value) {
    if ($value != NULL || $value = 0) {
      $this->attributes['last_date_unfinished'] = date_format(date_create($value), 'Y-m-d H:i:s');
    }
  }*/

}
