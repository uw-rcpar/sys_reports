<?php

namespace App\Library\Model;

use App\Library\Model\CSVImportable;

class HHCPerformance extends CSVImportable
{
    protected $table = 'rpt_hhc_performance';
    
    public $fillable = [
        'uid',
        'user',
        'package',
        'user_group',
        'cid',
        'moderator',
        'date',
        'answer_date',
        'thread',
        'year',
        'month',
        'day',
        'timediff',
        'title',
        'section_chapter',
        'section',
        'moderator_question',
        'comment_body_value',
    ];

    /**
     * {@inheritdoc}
     */
    public function getObjectKeyFields() {
        return ['cid'];
    }
}
