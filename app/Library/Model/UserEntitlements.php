<?php

namespace App\Library\Model;

use App\Library\Model\CSVImportable;

class UserEntitlements extends CSVImportable
{
    protected $table = 'rpt_user_entitlements';

    public $fillable = [
      'nid',
      'uid',
      'order_id',
      'created_date',
      'expiration_date',
      'orig_expiration_date',
      'status',
      'bundle',
      'sku',
      'product_type_tid',
      'course_type',
      'course_section_tid',
      'course_section',
      'default_duration',
      'default_interval',
      'extended_duration',
      'extended_interval',
      'content_restricted',
      'unlimited_access',
      'mobile_offline_access',
      'hhc_access',
      'cram_active',
      'ipq_access',
      'partner_id',
      'exam_version',
      'activation_delayed',
      'serial_number',
      'allowed_installs',
      'install_dates'
    ];

    /**
     * {@inheritdoc}
     */
    public function getObjectKeyFields() {
        return ['nid'];
    }
}
