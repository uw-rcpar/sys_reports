<?php

  namespace App\Library\Model;

  use App\Library\Model\CSVImportable;

  class PartnerEmails extends CSVImportable {
    public $fillable = [
      'partner_email_id',
      'partner_nid',
      'gid',
      'email',
      'status',
      'count',
      'campus_state',
      'create_date',
      'create_year',
      'create_month',
      'create_day'
    ];
    protected $table = 'rpt_partner_emails';

    /**
     * {@inheritdoc}
     */
    public function getObjectKeyFields() {
      return ['partner_email_id', 'partner_nid'];
    }
  }
