<?php

namespace App\Library\Model;

use App\Library\Model\CSVImportable;
use Illuminate\Support\Facades\DB;


class NetPromoterScore extends CSVImportable
{
    protected $table = 'rpt_nps';

    public $fillable = [
      'uid',
      'create_date',
      'score',
      'comment',
      'purchaser_type',
      'skus'
    ];

    /**
     * {@inheritdoc}
     */
    public function getObjectKeyFields() {
        return ['uid', 'create_date'];
    }

    /**
     * Gets a list of NetPromoterScore values filtered by email (using LIKE %email%)
     * and sorted as specified by $order_by and $order_direction.
     * @param string $email
     * @param string $order_by
     * @param string $order_direction
     * @param bool $paginate
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator|\Illuminate\Support\Collection
     */
    public function getNetPromoterScoreReport(
      $email = '',
      $order_by = 'date',
      $order_direction = 'desc',
      $paginate = true
    ) {
        $query = DB::table('rpt_nps AS nps')
          ->select(
            'nps.uid', 'nps.create_date AS date',
            'comment', 'purchaser_type', 'skus', 'score', 'mail'
          )
          ->join('rpt_user_profile AS cp', 'cp.uid', '=', 'nps.uid')
          ->orderBy($order_by, $order_direction);
        if (!empty($email)) {
            $query->where('cp.mail', 'LIKE', '%' . $email . '%');
        }
        if ($paginate) {
            $nps = $query->paginate(50);
        } else {
            $nps = $query->get();
        }
        return $nps;
    }

}
