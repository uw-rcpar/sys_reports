<?php

namespace App\Library\Model;

use App\Library\Model\CSVImportable;

class IPQBluePrints extends CSVImportable
{
  protected $table = 'rpt_ipq_blueprints';

  public $fillable = [
    'effective_date',
    'computed_key',
    'section',
    'area_id',
    'area_title',
    'group_id',
    'group_title',
    'topic_id',
    'topic_title',
    'task_id',
    'task_skill',
    'representative_task',
    'changed',
    'change_description'
  ];

  /**
   * {@inheritdoc}
   */
  public function getObjectKeyFields() {
    return ['effective_date', 'computed_key'];
  }
}
