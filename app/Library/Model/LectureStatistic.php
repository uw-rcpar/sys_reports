<?php

namespace App\Library\Model;

use App\Library\Model\CSVImportable;

class LectureStatistic extends CSVImportable
{
  protected $table = 'rpt_lecture_statistics';

  public $fillable = [
    'uid',
    'section',
    'course_type',
    'videos_watched',
    'videos_completed',
    'notes',
    'bookmarks',
    'highlights',
    'group',
    'entitlement_id',
    'video_total_duration',
    'avg_video_total_duration',
    'watched_time_total',
    'watched_time_complete',
    'first_date',
    'last_date',
  ];

  /**
   * {@inheritdoc}
   */
  public function getObjectKeyFields() {
    return ['uid', 'section', 'course_type', 'entitlement_id', 'group'];
  }
}
