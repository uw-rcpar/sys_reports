<?php

namespace App\Library\Model;

use App\Library\Model\CSVImportable;

class ExamScoreAttempts extends CSVImportable
{
  protected $table = 'rpt_score_attempts';

  public $fillable = [
    'uid',
    'first_exam_taken',
    'last_exam_taken',
    'avg_score',
    'attempts',
  ];

  /**
   * {@inheritdoc}
   */
  public function getObjectKeyFields() {
    return ['uid'];
  }
}
