<?php

namespace App\Library\Model;

use App\Library\Model\CSVImportable;

class Hubspot extends CSVImportable
{
    protected $table = 'rpt_hubspot';

    public $fillable = [
        'hubspot_contact_id',
        'email',
        'emails_delivered',
        'emails_opened',
        'emails_clicked',
        'first_email_sent_date',
        'visits',
        'original_source_type',
        'form_submissions',
        'lead_status',
        'cpa_exam_date',
        'cpa_exam_date_month',
        'cpa_exam_date_year',
        'first_name',
        'last_name',
        'graduation_date',
        'graduation_year',
        'graduation_month',
        'lifecycle_stage',
        'create_date',
        'became_customer_date',
        'score',
        'campus',
        'prods_purchased',
        'user_id',
        'ft_mark',
        'asl_mark',
        'fsl_mark',
        'pal_mark',
        'act_mark',
        'ft_date',
        'asl_date',
        'pal_date',
        'fsl_date',
        'act_date',
        'aud_exam_date',
        'bec_exam_date',
        'far_exam_date',
        'reg_exam_date',
        'order_id',
        'added_to_list_date',
        'company_name'
      ];

    /**
     * {@inheritdoc}
     */
    public function getObjectKeyFields() {
        return ['hubspot_contact_id'];
    }

    /**
     * {@inheritdoc}
     */
    public static function getColumnMapping() {
        return [
          'Contact ID' =>  'hubspot_contact_id',
          'Email' =>  'email',
          'Emails Delivered' =>  'emails_delivered',
          'Emails Opened' =>  'emails_opened',
          'Emails Clicked' =>  'emails_clicked',
          'First email send date' =>  'first_email_sent_date',
          'Number of Visits' =>  'visits',
          'Original Source Type' =>  'original_source_type',
          'Number of Form Submissions' =>  'form_submissions',
          'Lead Status' =>  'lead_status',
          'Study for the CPA Exam Date' =>  'cpa_exam_date',
          'Study for the CPA Exam Month' =>  'cpa_exam_date_month',
          'Study for the CPA Exam Year' =>  'cpa_exam_date_year',
          'First Name' =>  'first_name',
          'Last Name' =>  'last_name',
          'Graduation Date' =>  'graduation_date',
          'Graduation Date Year' =>  'graduation_year',
          'Graduation Date Month' =>  'graduation_month',
          'Lifecycle Stage' =>  'lifecycle_stage',
          'Create Date' =>  'create_date',
          'Became a Customer Date' =>  'became_customer_date',
          'HubSpot Score' =>  'score',
          'Campus' =>  'campus',
          'Products Purchased' =>  'prods_purchased',
          'Drupal ID' =>  'user_id',
          'free Trial mark' =>  'ft_mark',
          'ASL Mark' =>  'asl_mark',
          'FSL Mark' =>  'fsl_mark',
          'PAL Mark' =>  'pal_mark',
          'ACT Mark' =>  'act_mark',
          'Free Trial Date' =>  'ft_date',
          'ASL Date' =>  'asl_date',
          'PAL Date' =>  'pal_date',
          'FSL Date' =>  'fsl_date',
          'ACT Date' =>  'act_date',
          'Exam Date AUD' =>  'aud_exam_date',
          'Exam Date BEC' =>  'bec_exam_date',
          'Exam Date FAR' =>  'far_exam_date',
          'Exam Date REG' =>  'reg_exam_date',
          'Order id' =>  'order_id',
          'Added To List On' =>  'added_to_list_date',
          'Associated Company Name' =>  'company_name'
        ];
    }
}
