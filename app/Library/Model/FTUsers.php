<?php

namespace App\Library\Model;

use App\Library\Model\CSVImportable;

class FTUsers extends CSVImportable
{
  protected $table = 'rpt_free_trial_users';
  
  public $fillable = [
    'user_id',
    'mail',
    'order_count',
    'date_first_ft',
    'date_last_ft',
  ];

  /**
   * {@inheritdoc}
   */
  public function getObjectKeyFields() {
    return ['user_id'];
  }
}
