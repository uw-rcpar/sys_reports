<?php

namespace App\Library\Model;

use Illuminate\Database\Eloquent\Model;

class USState extends Model
{
    protected $table = 'us_states';

    public $fillable = [
      'abbreviated_name',
      'full_name'
    ];

}
