<?php

namespace App\Library\Model;

use App\Library\Model\CSVImportable;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\Facades\DB;

class OrderLine extends CSVImportable
{
  protected $table = 'rpt_line_item_revenue';

  public $fillable = [
    'order_id',
    'line_item_id',
    'sku',
    'name',
    'quantity',
    'revenue',
    'created_date',
    'modified_date',
    'taxable',
    'taxed',
    'total',
  ];

  /**
   * {@inheritdoc}
   */
  public function getObjectKeyFields() {
    return ['line_item_id'];
  }

    /**
     * Get's a list of products that matches the filters and information about its
     * total and taxes price amounts. Also lists the orders that were taken into
     * account to get the products and the refunds for the specified period.
     *
     * @param string $sku SKU filter value.
     * @param null $start_date Start date (formatted as 'yyyy-mm-dd') filter value.
     * @param null $end_date Start date (formatted as 'yyyy-mm-dd') filter value.
     * @param string $country Country code filter value.
     * @param array $payment_methods Payment methods associated to the orders.
     * @param array $excluded_product_list List of products SKU that should be excluded.
     *  from the report.
     * @return \Illuminate\Support\Collection
     */
    public function getProductSoldDetailed(
      $sku = "",
      $start_date = null,
      $end_date = null,
      $country = '',
      $payment_methods = [],
      $excluded_product_list = []
    ) {
        if (!is_array($payment_methods)){
            $payment_methods = [] ;
        }
        // The actual value that we need to search when looking for freetrial is ''
        $payment_methods = array_replace($payment_methods,
          array_fill_keys(array_keys($payment_methods, 'freetrial'), 'FT')
        );
        $query = DB::table('rpt_order_transactions AS o')
          ->select(
            'li.sku', 'li.name',
            DB::raw('SUM(li.quantity) AS sold_count'),
            DB::raw('SUM(total - taxed) AS pre_tax_revenue'),
            DB::raw('SUM(taxed) AS taxed'),
            DB::raw('SUM(total) AS total')
          )
          ->leftJoin('rpt_line_item_revenue AS li', 'o.order_id', '=', 'li.order_id')
          ->leftJoin('rpt_user_profile AS cp', 'cp.uid', '=', 'o.user_id')
          ->leftJoin('rpt_partner_profile AS partner', 'o.partner_nid', '=', 'partner.nid')
          ->groupBy('li.sku');
        $query->where('o.order_status', '<>', 'canceled');
        
        if (!empty($sku)) {
            $query->where('li.sku', '=', $sku);
        }

        if (!empty($start_date)) {
            $query->where(DB::raw('o.create_date'), '>=', $start_date);
        }

        if (!empty($end_date)) {
            $query->where(DB::raw('o.create_date'), '<=', $end_date);
        }

        if (!empty($country)) {
            if ($country == "US") {
                $query->where('cp.ship_country', '=', 'United States');
            }
            else {
                $query->where('cp.ship_country', '<>', 'United States');
            }
        }

        if (!empty($payment_methods)) {
            $query->whereIn('o.payment_method', $payment_methods);
        }

        // Filter products
        if (!empty($excluded_product_list)){
            $query->whereNotIn('li.sku', $excluded_product_list);
        }

        // Exclude duplicated line items for same sku -
        // For some reason there are duplicated line items for same SKU on a given order
        // Example: order = 776931 ; sku = FULL-MOB-OFF	 ; ORDER IDS = [9214526,9220036]
        // In those case we ignore the first one
        $query->whereNotIn('li.line_item_id', function($query) {
            $query->select(DB::raw("DISTINCT( MIN( line_item_id ) )"))
                ->from("rpt_line_item_revenue")
                ->groupBy("order_id", "sku")
                ->havingRaw("COUNT( line_item_id ) > 1");
        });

        $query->orderBy("total", "desc");
        $orders = $query->get();
        return $orders;
    }



    /**
     * Get's a list of products that matches the filters and information about its
     * total and taxes price amounts.
     *
     * @param string $sku SKU filter value.
     * @param null $start_date Start date (formatted as 'yyyy-mm-dd') filter value.
     * @param null $end_date Start date (formatted as 'yyyy-mm-dd') filter value.
     * @param string $country Country code filter value.
     * @param array $payment_methods Payment methods associated to the orders.
     * @param array $excluded_product_list List of products SKU that should be excluded.
     *  from the report.
     * @return \Illuminate\Support\Collection
     */
    public function getOrdersForProductSoldDetailed(
      $sku = "",
      $start_date = null,
      $end_date = null,
      $country = '',
      $payment_methods = [],
      $excluded_product_list = []
    ) {
        if (!is_array($payment_methods)){
            $payment_methods = [] ;
        }
        // The actual value that we need to search when looking for freetrial is ''
        $payment_methods = array_replace($payment_methods,
          array_fill_keys(array_keys($payment_methods, 'freetrial'), '')
        );

        $query = DB::table('rpt_order_transactions AS o')
          ->select(
            'o.order_id',
            'o.order_status AS status',
            'o.order_total AS order_total',
            'o.shipping AS shipping',
            'o.taxes AS tax_total',
            'o.create_date AS payment_updated_at'
          )
          ->join('rpt_line_item_revenue AS li', 'o.order_id', '=', 'li.order_id')
          ->join('rpt_user_profile AS cp', 'cp.uid', '=', 'o.user_id')
          ->leftJoin('rpt_partner_profile AS partner', 'o.partner_nid', '=', 'partner.nid')
          ->groupBy('o.order_id');
        $query->where('o.order_status', '<>', 'canceled');

        if (!empty($sku)) {
            $query->where('li.sku', '=', $sku);
        }

        if (!empty($start_date)) {
            $query->where(DB::raw('o.create_date'), '>=', $start_date);
        }

        if (!empty($end_date)) {
            $query->where(DB::raw('o.create_date'), '<=', $end_date);
        }

        if (!empty($country)) {
            if ($country == "US") {
                $query->where('cp.ship_country', '=', 'United States');
            }
            else {
                $query->where('cp.ship_country', '<>', 'United States');
            }
        }

        if (!empty($payment_methods)) {
            $query->whereIn('o.payment_method', $payment_methods);
        }

        // Filter products
        if (!empty($excluded_product_list)){
            $query->whereNotIn('li.sku', $excluded_product_list);
        }

        $orders = $query->get();
        return $orders;
    }

    /**
     * Get's a list of refunds for the specified period for the orders that would have been
     * considered to obtain the information on the getOrdersForProductSoldDetailed function.
     *
     * @param string $sku SKU filter value.
     * @param null $start_date Start date (formatted as 'yyyy-mm-dd') filter value.
     * @param null $end_date Start date (formatted as 'yyyy-mm-dd') filter value.
     * @param string $country Country code filter value.
     * @param array $payment_methods Payment methods associated to the orders.
     * @param array $excluded_product_list List of products SKU that should be excluded.
     *  from the report.
     * @return \Illuminate\Support\Collection
     */
    public function getRefundsForProductSoldDetailed(
      $sku = "",
      $start_date = null,
      $end_date = null,
      $country = '',
      $payment_methods = [],
      $excluded_product_list = []
    ) {
        if (!is_array($payment_methods)){
            $payment_methods = [] ;
        }
        // The actual value that we need to search when looking for freetrial is ''
        $payment_methods = array_replace($payment_methods,
          array_fill_keys(array_keys($payment_methods, 'freetrial'), '')
        );

        $query = DB::table('rpt_order_transactions AS o')
          ->select(
            'o.order_id', 'o.order_status AS status' ,
            'o.order_total AS order_total',
            'o.taxes AS tax_total',
            'o.create_date AS payment_updated_at',
            'o.refund_date',
            'o.refund_amount'
          )
          ->join('rpt_line_item_revenue AS li', 'o.order_id', '=', 'li.order_id')
          ->join('rpt_user_profile AS cp', 'cp.uid', '=', 'o.user_id')
          ->leftJoin('rpt_partner_profile AS partner', 'o.partner_nid', '=', 'partner.nid')
          ->groupBy('o.order_id');

        // We only want to pull orders with refunds on this query
        $query->where('o.refund_amount', '<>', 0);

        if (!empty($sku)) {
            $query->where('li.sku', '=', $sku);
        }

        if (!empty($start_date)) {
            $query->where(DB::raw('o.refund_date'), '>=', $start_date);
        }

        if (!empty($end_date)) {
            $query->where(DB::raw('o.refund_date'), '<=', $end_date);
        }

        if (!empty($country)) {
            if ($country == "US") {
                $query->where('cp.ship_country', '=', 'United States');
            }
            else {
                $query->where('cp.ship_country', '<>', 'United States');
            }
        }

        if (!empty($payment_methods)) {
            $query->whereIn('o.payment_method', $payment_methods);
        }

        // Filter products
        if (!empty($excluded_product_list)){
            $query->whereNotIn('li.sku', $excluded_product_list);
        }
        $orders = $query->get();
        return $orders;
    }
}
