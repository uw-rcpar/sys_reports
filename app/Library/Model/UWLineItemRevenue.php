<?php

namespace App\Library\Model;

use App\Library\Model\CSVImportable;

class UWLineItemRevenue extends CSVImportable {
  protected $table = 'rpt_uw_line_item_revenue';

  public $fillable = [
      'item_id',
      'order_id',
      'user_id',
      'package_id',
      'package_name',
      'course_id',
      'course_name',
      'duration',
      'is_renewal',
      'is_sim',
      'form_id',
      'is_trial',
      'qbank_id',
      'qbank_name',
      'top_level_product_id',
      'top_level_product_name',
      'top_level_prodct_group_id',
      'top_level_prodct_group_name',
      'sub_total',
      'promo_total',
      'partner_discount_total',
      'credit_amount',
      'quantity',
      'sales_tax',
      'credit_sales_tax',
      'order_total',
      'free_trial',
      'ft_time_diff',
      'ft_time_group',
      'promotion_type',
      'promotion',
      'promo_description',
  ];

  /**
   * {@inheritdoc}
   */
  public function getObjectKeyFields() {
    return ['order_id', 'item_id'];
  }
}
