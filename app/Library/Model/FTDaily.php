<?php

namespace App\Library\Model;

use App\Library\Model\CSVImportable;

class FTDaily extends CSVImportable
{
  protected $table = 'rpt_free_trial_daily';
  
  public $fillable = [
    'create_day',
    'total_count'
  ];

  /**
   * {@inheritdoc}
   */
  public function getObjectKeyFields() {
    return ['create_day'];
  }
}
