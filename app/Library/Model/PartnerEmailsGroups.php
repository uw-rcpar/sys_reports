<?php

  namespace App\Library\Model;

  use App\Library\Model\CSVImportable;

  class PartnerEmailsGroups extends CSVImportable {
    public $fillable = [
      'group_id',
      'partner_nid',
      'group_name',
      'start_date',
      'end_date',
      'create_date',
      'override_expiration',
      'class_name'
    ];
    protected $table = 'rpt_partner_emails_groups';

    /**
     * {@inheritdoc}
     */
    public function getObjectKeyFields() {
      return ['group_id', 'partner_nid'];
    }
  }