<?php

namespace App\Library\Model;

use App\Library\Model\CSVImportable;

class Referral extends CSVImportable
{
  protected $table = 'rpt_referral';

  public $fillable = [
    'uid',
    'referring_uid',
    'order_id',
    'create_date',
    'referred_destination'
  ];

  /**
   * {@inheritdoc}
   */
  public function getObjectKeyFields() {
    return ['uid'];
  }
}
