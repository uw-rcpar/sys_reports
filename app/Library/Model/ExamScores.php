<?php

namespace App\Library\Model;

use App\Library\Model\CSVImportable;

class ExamScores extends CSVImportable
{
  protected $table = 'rpt_exam_scores';

  public $fillable = [
    'exam_scores_id',
    'attempt',
    'uid',
    'score_entered',
    'exam_taken',
    'exam_year',
    'score',
    'pass_fail',
    'section',
    'country',
    'state',
    'st',
    'market',
    'region',
    'us_college',
    'campus_state',
    'college_name',
    'exam_type',
  ];

  /**
   * {@inheritdoc}
   */
  public function getObjectKeyFields() {
    return ['exam_scores_id'];
  }
}
