<?php

namespace App\Library\Model;


use App\Library\Model\CSVImportable;
use Illuminate\Support\Facades\DB;

class UserProfile extends CSVImportable
{
    protected $table = 'rpt_user_profile';

    public $fillable = [
      'uid',
      'registration_date',
      'first_exam_sit',
      'college_name',
      'did_graduate_college',
      'grad_date',
      'courses_taught',
      'phone',
      'roles',
      'status',
      'username',
      'legacy_id',
      'mail',
      'ship_country',
      'ship_state',
      'ship_city',
      'ship_dependent_locality',
      'ship_zip',
      'ship_thoroughfare',
      'ship_premise',
      'billing_country',
      'billing_state',
      'billing_city',
      'billing_dependent_locality',
      'billing_zip',
      'billing_thoroughfare',
      'billing_premise',
      'name_line',
      'first_name',
      'last_name',
      'user_profile_country',
      'user_profile_state',
      'campus',
      'aud_exam_date',
      'bec_exam_date',
      'far_exam_date',
      'reg_exam_date',
      'archive_synced',
      'sync_stage',
      'uw_uid'
    ];

    /**
     * {@inheritdoc}
     */
    public function getObjectKeyFields() {
        return ['uid'];
    }

    /**
     * Gets information about the orders that this user created.
     *
     * @return mixed
     */
    function getAssociatedOrders() {
        $query = DB::table('rpt_order_transactions AS o')
          ->where('o.user_id', '=', $this->uid);
        $results = $query->paginate(50);
        return $results;
    }

    /**
     * Gets information related to the the lectures and user learning processes.
     *
     * @return \Illuminate\Support\Collection
     */
    function getAssociatedLearningStats() {
        $cur_date = date('Y-m-d');
        $query = DB::table('rpt_user_entitlements as ent')
          ->leftJoin('rpt_lecture_history as lect_hist',
            'lect_hist.entitlement_nid',
            '=', 'ent.nid')
          ->leftJoin(
            DB::raw('(SELECT
                SUM(videos_total_duration) as videos_total_duration,
                 section, year FROM rpt_lecture_metadata GROUP BY section, year) as lect_md')
            ,
            function ($join) {
                $join->on('lect_md.section', '=', 'ent.course_section')
                  ->on('lect_md.year', '=', 'ent.exam_version');
            }
          )
          ->where('ent.uid', '=', $this->uid)
          ->where('ent.expiration_date', '>=', $cur_date);
        $results = $query->get();
        return $results;
    }
}
