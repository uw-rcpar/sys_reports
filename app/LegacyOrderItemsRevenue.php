<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LegacyOrderItemsRevenue extends Model {
  protected $table = 'rpt_order_items_revenue_01_2010_05_2015';

  public $fillable = [
    'order_id',
    'item_id',
    'item_name',
    'quantity',
    'revenue',
  ];
}
