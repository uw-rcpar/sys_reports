<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Log;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->call(function () {
            Log::notice('Starting enqueueing of uploaded CSV files.');
            $csvImporter = \App::get('rcpar.csvimporter');
            $files = $csvImporter->listCSVAvailableToImport();
            foreach ($files as $file) {
                Log::notice('Enqueueing file for later processing: '.$file.'.csv.');
                \App\Jobs\ProcessImporterETL::dispatch($file)->onQueue('csv-importer');
            }
            Log::notice('Enqueueing of uploaded CSV files finished.');
        })->name('import-uploaded-csv')->everyFifteenMinutes()->withoutOverlapping();

        $schedule->call(function () {
          Log::notice('Starting pulling of CSV files from outlook.');
          $outlook = \App::get('rcpar.outlook');
          // If the App is not authenticated in Outlook, redirect the user to the authentication
          // page.
          if (!$outlook->isAuthenticated()) {
            Log::error('Outlook authentication failed, manually authentication needed.');
            return;
          }
          $availableAttachments = $outlook->getLatestsAttachmentsList();
          $csvImporter = \App::get('rcpar.csvimporter');
          $app = \App::getFacadeRoot();
          $csv_path = $app['config']['data_importers.csv_to_import_path'];
          Log::notice('Number of attachments: '.count($availableAttachments));
          foreach ($availableAttachments as $att) {
            $info = pathinfo($att["fileName"]);
            if (
              in_array($att["contentType"], array('text/csv', 'application/csv'))
                ||
              $info['extension'] == 'csv'
             ) {
              $destination = $csv_path . $info["basename"];
              Log::notice('Attachment Name = '.$destination);
              // If we have a Model class with that name, we assume that this is a csv
              // file to use with that class.
              $modelClassName = $csvImporter->getModelClassNameFromFilename($destination);
              if($modelClassName) {
                try {
                  Log::notice('Downloading: '.$info["basename"].'.');
                  $outlook->downloadAttachment($att["mailId"], $att["id"], $destination);
                  Log::notice('Starting import of '.$info["basename"].'.');

                  $total_rows = $csvImporter->countRowsOnCsv($destination);
                  $start_time = date("H:i:s");
                  $processed_rows = $csvImporter->importCSV($destination);
                  $end_time = date("H:i:s");
                  Log::info("Processed (from Outlook) - " . $info["basename"] . ' - Records total:' . $total_rows
                    . ' - Records imported: ' . $processed_rows
                    . ' - Start: '.$start_time . ' End: ' . $end_time
                  );
                } catch (\Exception $e) {
                  Log::error('Error when importing '.$info["basename"].': '.$e->getMessage());
                }
              }else{
                Log::error('Model not found for destination: '.$destination);
              }
            }
          }
          Log::notice('Finished pulling of CSV files from outlook.');
        })->hourly();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
