<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;


class ImportCSV extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'rcpar:import-csv {csv-file}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import CSV file data into the system ().';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
      try {
        $app = \App::getFacadeRoot();
        $csvImporter = $app->get('rcpar.csvimporter');
        $csv_path = $app['config']['data_importers.csv_to_import_path'];

        $pathToFile = $csv_path . $this->argument('csv-file') . '.csv';

        $total_rows = $csvImporter->countRowsOnCsv($pathToFile);
        $start_time = date("H:i:s");
        $processed_rows = $csvImporter->importCSV($pathToFile);
        $end_time = date("H:i:s");
        $this->line("Processed -  " .$this->argument('csv-file') . '.csv' . ' - Records total:' . $total_rows
          . ' - Records imported: ' . $processed_rows
          . ' - Start: '.$start_time . ' End: ' . $end_time
        );
      }
      catch (\Exception $e) {
        $this->error('Sorry something went wrong: ' . $e->getMessage());
      }
    }
}
