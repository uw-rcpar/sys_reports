<?php

namespace App\Exceptions;

use Exception;

class OutlookAuthException extends Exception
{
  /**
   * Constant codes for error types.
   */
  const WRONG_STATE = 1;
  const OAUTH_ERROR = 2;
  const GENERIC_ERROR = 3;
}
