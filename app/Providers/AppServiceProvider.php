<?php

namespace App\Providers;

use Illuminate\Support\Facades\Blade;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider {
  /**
   * Bootstrap any application services.
   *
   * @return void
   */
  public function boot() {
    Blade::directive('convertCurrency', function ($money) {
      setlocale(LC_MONETARY, 'en_US.UTF-8');
      return "<?php echo money_format('%.2n', $money); ?>";
    });
  }

  /**
   * Register any application services.
   *
   * @return void
   */
  public function register() {
    //
  }
}
