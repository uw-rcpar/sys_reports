<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Library\Services\CSVImporter;

class CSVImporterServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('rcpar.csvimporter', function ($app) {
          return new CSVImporter($app);
        });
    }
}
