<?php

namespace App\Providers;

use App\Library\Services\RCPAROutlook;
use Illuminate\Support\ServiceProvider;

class RCPAROutlookServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
      $this->app->singleton('rcpar.outlook', function ($app) {
        return new RCPAROutlook($app);
      });
    }
}
