<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Log;

class ProcessImporterETL implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $fileName;

    /**
     * Create a new job instance.
     *
     * @param string $fileName
     * @return void
     */
    public function __construct($fileName)
    {
        $this->fileName = $fileName;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $csvImporter = \App::get('rcpar.csvimporter');
        $total_rows = 0;
        $processed_rows = 0;
        $fileNameNoExt = $this->fileName;
        try {
            $app = \App::getFacadeRoot();
            $csv_path = $app['config']['data_importers.csv_to_import_path'];
            $pathToFile = $csv_path . $this->fileName . '.csv';
            $total_rows = $csvImporter->countRowsOnCsv($pathToFile);
            $start_time = date("H:i:s");
            $processed_rows = $csvImporter->importCSV($pathToFile);
            $end_time = date("H:i:s");
            Log::info("Processed - " . $this->fileName . ' - Records total:' . $total_rows
              . ' - Records imported: ' . $processed_rows
              . ' - Start: '.$start_time . ' End: ' . $end_time
            );
        }
        catch (\Exception $e) {
            Log::error("There was a problem when importing: " . $fileNameNoExt .
                ' - ' . $e->getMessage());
        }
    }
}
