<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

use Illuminate\Contracts\Mail\Mailer;

class SendQueueEmail implements ShouldQueue {
  use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

  /**
   * Create a new job instance.
   *
   * @return void
   */
  public function __construct() {
    $jobName = '';
  }

  /**
   * Execute the job.
   *
   * @return void
   */
  public function handle(Mailer $mailer) {

    $mailer->send('email.queue', ['data' => 'data'], function ($message) {

      $message->from('it@rogercpareview.com', 'IT Queue Agent');

      $message->to('patkins@rogercpareview.com, 9e6c612158-1901f9@inbox.mailtrap.io');

      $message->subject('IT Queue Complete ' . $this->jobName);

    });

  }
}
