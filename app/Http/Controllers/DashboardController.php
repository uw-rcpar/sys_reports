<?php

namespace App\Http\Controllers;

use App\Http\Controllers\FTDailyController AS FTDaily;
use App\Http\Controllers\FTUsersController AS FTUsers;
use App\Http\Controllers\FTPaidController AS FTPaid;
use App\Http\Controllers\AffiliatesController AS Affiliates;
use App\Http\Controllers\IPQSessionsController AS IPQSessions;

class DashboardController extends Controller {

  // Index View
  public function index() {
    $browser_lt = 'Coming Soon...';
    $app_server = 'Coming Soon...';
    $app_errors = 'Coming Soon...';

    return view('dashboard', [
      'browser_lt' => $browser_lt,
      'app_server' => $app_server,
      'app_errors' => $app_errors
    ]);
  }

  // Index View
  public function reportingIndex() {
    $ftdaily_snapshot = 'Coming Soon...Daily';
    $ftusers_snapshot = 'Coming Soon...Users';

    $ftdaily = new FTDaily();
    $ftdaily_snapshot = $ftdaily->getDailyList();

    $ftusers = new FTUsers();
    $ftusers_snapshot = $ftusers->getTotalCounts();

    $ftpaid = new FTPaid();
    $ftpaid_snapshot = $ftpaid->getTotalPaidCounts();

    $affiliates = new Affiliates();
    $affiliates_snapshot = $affiliates->getDailyList();
    $affiliates_stats_snapshot = $affiliates->getDailyAffiliateCounts();

    $IPQSessions = new IPQSessions;
    $ipq_snapshot = $IPQSessions->getDailyList();
    $ipq_snapshot_stats = $IPQSessions->getDailyIPQSessionsCounts();
    $ipq_stats_completed = $IPQSessions->getUsageByStatus('total_quizes_finished');
    $ipq_stats_incomplete = $IPQSessions->getUsageByStatus('total_quizes_unfinished');

    return view('reporting', [
      'ft_daily_snapshot'         => $ftdaily_snapshot,
      'ft_users_snapshot'         => $ftusers_snapshot,
      'ft_paid_snapshot'          => $ftpaid_snapshot,
      'affiliates_snapshot'       => $affiliates_snapshot,
      'affiliates_stats_snapshot' => $affiliates_stats_snapshot,
      'ipq_snapshot'              => $ipq_snapshot,
      'ipq_snapshot_stats'        => $ipq_snapshot_stats,
      'ipq_stats_completed'       => $ipq_stats_completed,
      'ipq_stats_incomplete'      => $ipq_stats_incomplete

    ]);
  }

}
