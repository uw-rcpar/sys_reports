<?php

namespace App\Http\Controllers;

use App\Library\Model\PartnerProfile;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PartnerProfilesController extends Controller
{
    /**
     * Display a listing of partners.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request) {
        $results_per_page = 20;
        $filters['partner_name'] = "";
        $query = DB::table('rpt_partner_profile');
        if ($request->getMethod() == "POST" || $request->input("partner_name") != null) {
            $filters['partner_name'] = $request->input("partner_name");
            $query->where('partner_name', 'like',
              '%' . $filters['partner_name'] . '%');
        }
        $query->orderBy('partner_name', 'asc');
        $partners = $query->paginate($results_per_page);
        return view('partners.index', array(
          'partners' => $partners,
          'filters' => $filters
        ));
    }

    /**
     * Display the specified partner.
     *
     * @param $nid Node Id (drupal id) of the partner.
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show($nid) {
        $partner = PartnerProfile::where('nid', $nid)->first();
        $users = $partner->getAssociatedUsers();
        return view('partners.show', array(
          'partner' => $partner,
          'users' => $users
        ));
    }
}
