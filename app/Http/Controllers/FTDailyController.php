<?php

namespace App\Http\Controllers;

use App\Library\Model\FTDaily; // Load data model

class FTDailyController extends Controller {

  /**
   * @return mixed
   */
  public function getDailyList() {
    $results = FTDaily::orderBy('create_day', 'desc')->take('14')->get();
    return $results;
  }

  /**
   * @param $data
   */
  public function processRecord($data) {

    // Load data model for upsert
    $dm = new FTDaily();

    // Upsert using the model
    $dm::updateOrCreate(
        ['create_day' => $data[0]],
      [
        'create_day' => $data[0],
        'total_count' => $data[1]
      ]
    );
  }
}
