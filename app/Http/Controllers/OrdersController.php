<?php

namespace App\Http\Controllers;

use App\Library\Model\OrderTransactions;
use App\Library\Model\UserProfile;
use Illuminate\Http\Request;

class OrdersController extends Controller
{
    /**
     * Display the specified Order.
     *
     * @param  int $order_id Drupal Order id
     * @return Response
     */
    public function show($order_id) {
        $order = OrderTransactions::where('order_id', $order_id)->first();
        $user = UserProfile::where('uid', $order->user_id)->first();;
        return view('orders.show', array(
          'order' => $order,
          'user' => $user
        ));
    }
}
