<?php

namespace App\Http\Controllers;

//use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Mockery\Exception;
use App\Exceptions\OutlookAuthException;

class AuthController extends Controller {

  public function signin() {
    $outlook = \App::get('rcpar.outlook');
    $authorizationUrl = $outlook->getAuthenticationUrl();

    // Redirect to authorization endpoint
    header('Location: ' . $authorizationUrl);
    exit();
  }

  public function gettoken() {
    try {
      $outlook = \App::get('rcpar.outlook');
      $outlook->pullTokenAfterRedirect();

      // Redirect back to mail page
      return redirect('mail');
    }
    catch (OutlookAuthException $e) {
      exit($e->getMessage());
    }

  }
}