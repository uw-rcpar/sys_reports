<?php

namespace App\Http\Controllers;

use App\Library\Model\AffiliateTransactions; // Load data model
use DB;

class AffiliatesController extends Controller {

  /**
   * @return mixed
   */
  public function getDailyList() {
    $results = AffiliateTransactions::orderBy('OriginalDate', 'desc')->take('14')->get();
    return $results;
  }

  /**
   * @return mixed
   */
  public function getDailyAffiliateCounts() {
    $results = DB::table('rpt_affiliate_transactions')
      ->select(DB::raw('Affiliate, DATE(OriginalDate) AS OriginalDate, COUNT(OrderID) AS OrderCount, Sum(TotalCost) AS TotalCost'))
      ->where('OriginalDate', '>=', date('Y-m-d H:i:s', strtotime('-3 months')))
      ->orderBy('OriginalDate', 'DESC')
      ->groupBy('Affiliate')
      ->get();
    return $results;
  }

  /**
   * @param $data
   */
  public function processRecord($data) {

    // Load data model for upsert
    $dm = new AffiliateTransactions();

    // Upsert using the model
    $dm::updateOrCreate(
      ['OrderID' => $data[9],
       'Reason'       => $data[12]
        ],
      [
        'OriginalDate' => $data[0],
        'FundedDate'   => $data[1],
        'Affiliate'    => $data[2],
        'Campaign'     => $data[3],
        'EventType'    => $data[4],
        'Amount'       => $data[5],
        'Commission'   => $data[6],
        'LCFees'       => $data[7],
        'TotalCost'    => $data[8],
        'OrderID'      => $data[9],
        'MTID'         => $data[10],
        'Status'       => $data[11],
        'Reason'       => $data[12],
      ]
    );
  }
}