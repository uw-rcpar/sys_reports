<?php

namespace App\Http\Controllers;

use App\Library\Model\FTPaid; // Load data model
use DB;

class FTPaidController extends Controller {
  /**
   * @return mixed
   */
  public function getTotalPaidCounts() {
    $results = DB::table('rpt_free_trial_2purchase')
      ->select(DB::raw('ft_time_group AS groups, COUNT(user_id) as total_users, total_purchased_order_count as purchase_count, date_first_ft as ft, first_purchase_date as purchase, ft_time_diff as diff'))
      ->groupBy('ft_time_group')
      ->get();
    return $results;
  }

  /**
   * @param $data
   */
  public function processRecord($data) {

    // Load data model for upsert
    $dm = new FTPaid();

    // Upsert using the model
    $dm::updateOrCreate(
      ['create_day' => $data[0]],
      [
        'create_day'  => $data[0],
        'total_count' => $data[1]
      ]
    );
  }
}
