<?php

namespace App\Http\Controllers;

use App\Library\Model\NetPromoterScore;
use App\Library\Model\OrderTransactions;
use App\Library\Model\OrderLine;
use Illuminate\Http\Request;

class ReportsController extends Controller
{
    /**
     * Array with the products to get for the Orders with products report.
     * @var array
     */
    protected $products_for_taxable_report = [
      'AUD-6MEXT' => '6 month extension AUD',
      'BEC-6MEXT' => '6 month extension BEC',
      'FAR-6MEXT' => '6 month extension FAR',
      'REG-6MEXT' => '6 month extension REG',
      'AUD-OFF-LEC' => 'Offline Lectures AUD',
      'BEC-OFF-LEC' => 'Offline Lectures BEC',
      'FAR-OFF-LEC' => 'Offline Lectures FAR',
      'REG-OFF-LEC' => 'Offline Lectures REG',
      'AUD' => 'CPA Exam Review AUD',
      'BEC' => 'CPA Exam Review BEC',
      'FAR' => 'CPA Exam Review FAR',
      'REG' => 'CPA Exam Review REG',
      'AUD-EFC' => 'Flashcards AUD',
      'BEC-EFC' => 'Flashcards BEC',
      'FAR-EFC' => 'Flashcards FAR',
      'REG-EFC' => 'Flashcards REG',
      'AUD-CRAM' => 'Online CRAM Courses AUD',
      'BEC-CRAM' => 'Online CRAM Courses BEC',
      'FAR-CRAM' => 'Online CRAM Courses FAR',
      'REG-CRAM' => 'Online CRAM Courses REG',
      'AUD-AUD' => 'Audio Lectures AUD',
      'BEC-AUD' => 'Audio Lectures BEC',
      'FAR-AUD' => 'Audio Lectures FAR',
      'REG-AUD' => 'Audio Lectures REG',
      'AUD-BK' => 'Course Textbook AUD',
      'BEC-BK' => 'Course Textbook BEC',
      'FAR-BK' => 'Course Textbook FAR',
      'REG-BK' => 'Course Textbook REG',
      'AUD-EFC' => 'Flash CRAM Course AUD',
      'BEC-EFC' => 'Flash CRAM Course BEC',
      'FAR-EFC' => 'Flash CRAM Course FAR',
      'REG-EFC' => 'Flash CRAM Course REG',
      'FULL-EFC-DIS' => 'Full Flashcard Discount',
      'FULL-AUD-DIS' => 'Full Audio Discount',
      'FULL-SSD-DIS' => 'Full Flash Drive Cram Course Discount',
      'FULL-ELITE-DIS' => 'Elite Package Discount',
      'FULL-PREM-DIS' => 'Premier Package Discount',
    ];

    /**
     * Array with the products to exclude on the products detailed report.
     * @var array
     */
    protected $excluded_products_detailed = [
        'FULL-DIS',
        'FULL-ACR-DIS',
        'PARTNER-DIS',
        'FULL-PREM-DIS',
        'FULL-SSD-DIS',
        'FULL-AUD-DIS',
        'FULL-ELITE-DISold',
        'FULL-OFF-LEC-DIS',
        'FULL-ELITE-DIS',
        'FULL-ST-DIS',
        'FULL-FS-DIS',
        'FREETRIAL-BUNDLE',
        'FULL-EFC-DIS',
        ''
    ];

    /**
     * Returns the product type related with product
     * This pattern is the same that OrderTransaction::getCAProductsSold RLIKE condition
     * @TODO Need to check other cases and research if can be determined by SKU (or needs extra data to fetch from ETL?)
     * @param $sku
     * @return null|string
     */
    static function getProductTypeBySku($sku) {
        if (preg_match('/^((AUD)|(BEC)|(REG)|(FAR))(-((FS)|(PST)|(CRAM)))?$/', $sku)) {
            return "Courses";
        }
        return null;
    }

    /**
     * Endpoint for the reporting/CA-use-tax-report report page
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function caUseTaxReport(Request $request)
    {
        $products = [];
        $orders = [];
        $refunded = [];
        $products_total_quantity = 0;

        $filters['sku'] = '';
        $filters['order_state'] = ['pending', 'completed'];
        $filters['start_date'] = date("Y-m-d", strtotime("-1 month"));
        $filters['end_date'] = date("Y-m-d");
        $filters['billing_type'] = "CC-affirm";

        if ($request->getMethod() == "POST") {
            $filters['sku'] = $request->input("sku");
            $filters['order_state'] = $request->input("order_state");
            $filters['start_date'] = $request->input("start_date");
            $filters['end_date'] = $request->input("end_date");
            $filters['billing_type'] = $request->input("billing_type");
            $o = new OrderTransactions();
            $products = $o->getCAProductsSold(
              $filters['sku'],
              $filters['order_state'],
              $filters['start_date'],
              $filters['end_date'],
              $filters['billing_type']
            );

            foreach ($products as $product) {
                $products_total_quantity += $product->quantity;
                $product->product_type = self::getProductTypeBySku($product->sku);
            }
            $orders = $o->getOrdersFromCAProductsSold(
              $filters['sku'],
              $filters['order_state'],
              $filters['start_date'],
              $filters['end_date'],
              $filters['billing_type']
            );

            $refunded = $o->getOrdersRefundsFromCAProductsSold(
              $filters['sku'],
              $filters['order_state'],
              $filters['start_date'],
              $filters['end_date'],
              $filters['billing_type']
            );
        }

        return view('caUseTaxReport', [
          'filters' => $filters,
          'products' => $products,
          'products_total_quantity' => $products_total_quantity,
          'orders' => $orders,
          'refunded' => $refunded,
        ]);
    }

    /**
     * Endpoint for the reporting/CA-use-tax-report report page
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function taxablePerStateAndProduct(Request $request)
    {
        $ordersInfo = collect([]);
        $csv_export_url = '';

        $filters['order_state'] = ['pending', 'completed'];
        $filters['start_date'] = date("Y-m-d", strtotime("-1 month"));
        $filters['end_date'] = date("Y-m-d");
        $filters['state'] = "WA";
        $filters['payment_method'] = ['authnet_aim'];

        if ($request->getMethod() == "POST" || $request->input("state") != NULL) {
            $filters['state'] = $request->input("state");
            $filters['order_state'] = $request->input("order_state");
            $filters['start_date'] = $request->input("start_date");
            $filters['end_date'] = $request->input("end_date");
            $filters['payment_method'] = $request->input("payment_method");

            $o = new OrderTransactions();
            $ordersInfo = $o->getOrdersWithProductInfo(
              $filters['state'],
              $filters['order_state'],
              $filters['start_date'],
              $filters['end_date'],
              $filters['payment_method'],
              $this->products_for_taxable_report
            );
            $csv_export_url =  'taxable_per_state_and_product/csv?' . http_build_query($filters);
        }

        return view('taxablePerStateAndProduct', [
          'filters' => $filters,
          'products' => $this->products_for_taxable_report,
          'products_total_quantity' => 0,
          'orders' => $ordersInfo,
          'csv_export_url' => $csv_export_url
        ]);
    }

    /**
     * Endpoint for the CSV version of the reporting/CA-use-tax-report report.
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function taxablePerStateAndProductExport(Request $request) {
        $headers = array(
          "Content-type" => "text/csv",
          "Content-Disposition" => "attachment; filename=taxablePerStateAndProduct.csv",
          "Pragma" => "no-cache",
          "Cache-Control" => "must-revalidate, post-check=0, pre-check=0",
          "Expires" => "0"
        );

        $filters['state'] = $request->input("state");
        $filters['order_state'] = $request->input("order_state");
        $filters['start_date'] = $request->input("start_date");
        $filters['end_date'] = $request->input("end_data");
        $filters['payment_method'] = $request->input("payment_method");

        $o = new OrderTransactions();
        $ordersInfo = $o->getOrdersWithProductInfo(
          $filters['state'],
          $filters['order_state'],
          $filters['start_date'],
          $filters['end_date'],
          $filters['payment_method'],
          $this->products_for_taxable_report,
          false
        );

        $columns = array(
          'Order Id',
          'Order State',
          'City',
          'State',
          'Zip code',
          'Total',
          'Shipping',
          'Taxable',
          'Order date',
          'Customer first name',
          'Customer last name'
        );
        // Let's add the dynamic columns (product columns).
        foreach ($this->products_for_taxable_report as $prod) {
            $columns[] = $prod;
        }

        $callback = function () use ($ordersInfo, $columns) {
            $file = fopen('php://output', 'w');
            fputcsv($file, $columns);

            foreach ($ordersInfo as $order) {
                $row = array(
                  $order->order_id,
                  $order->status,
                  $order->city,
                  $order->state,
                  $order->postal_code,
                  '$'.number_format($order->total,2),
                  '$'.number_format($order->shipping,2),
                  '$'.number_format($order->taxed,2),
                  date('l, F j, Y - H:i', strtotime($order->created_date)),
                  $order->customer_first_name,
                  $order->customer_last_name,
                );
                foreach ($this->products_for_taxable_report as $sku => $prod) {
                    $row[] = '$'.number_format($order->$sku, 2);
                }
                fputcsv($file, $row);
            }
            fclose($file);
        };
        return response()->stream($callback, 200, $headers);
    }

    /**
     * Endpoint for the reporting/products_detailed report page
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function productsDetailed(Request $request) {
        $products = collect([]);
        $orders = collect([]);
        $refunds = collect([]);
        $csv_export_url = '';

        $products_totals['quantity'] = 0;
        $products_totals['pre_tax'] = 0;
        $products_totals['taxed'] = 0;
        $products_totals['total'] = 0;
        $products_totals['ship'] = 0;
        $products_totals['total_plus_ship'] = 0;

        $refunds_totals['taxed'] = 0;
        $refunds_totals['total'] = 0;
        $refunds_totals['refund_amount'] = 0;

        $filters['sku'] = '';
        $filters['start_date'] = date("Y-m-d", strtotime("-1 month"));
        $filters['end_date'] = date("Y-m-d");
        $filters['country'] = "US";
        $filters['payment_methods'] = ['affirm'];

        if ($request->getMethod() == "POST" || $request->input("state") != NULL) {
            $filters['sku'] = $request->input("sku");
            $filters['start_date'] = $request->input("start_date");
            $filters['end_date'] = $request->input("end_date");
            $filters['country'] = $request->input("country");
            $filters['payment_methods'] = $request->input("payment_methods");


            $ot = new OrderTransactions();
            $total_shipping = $ot->getProductSoldTotalShipping(
                $filters['sku'],
                $filters['start_date'],
                $filters['end_date'],
                $filters['country'],
                $filters['payment_methods'],
                $this->excluded_products_detailed
            );

            $ol = new OrderLine();
            $products = $ol->getProductSoldDetailed(
              $filters['sku'],
              $filters['start_date'],
              $filters['end_date'],
              $filters['country'],
              $filters['payment_methods'],
              $this->excluded_products_detailed
            );
            foreach ($products as $product) {
                $products_totals['quantity'] += $product->sold_count;
                $products_totals['pre_tax'] += $product->pre_tax_revenue;
                $products_totals['taxed'] += $product->taxed;
                $products_totals['total'] += $product->total;
            }
            // Add total shipping to the order
            $products_totals['total_plus_ship'] = $products_totals['total'] + $total_shipping;
            $products_totals['ship'] = $total_shipping;

            $refunds = $ol->getRefundsForProductSoldDetailed(
              $filters['sku'],
              $filters['start_date'],
              $filters['end_date'],
              $filters['country'],
              $filters['payment_methods'],
              $this->excluded_products_detailed
            );
            foreach ($refunds as $refund) {
                $refunds_totals['taxed'] += $refund->tax_total;
                $refunds_totals['total'] += $refund->order_total;
                $refunds_totals['refund_amount'] += $refund->refund_amount;
            }

            $orders = $ol->getOrdersForProductSoldDetailed(
              $filters['sku'],
              $filters['start_date'],
              $filters['end_date'],
              $filters['country'],
              $filters['payment_methods'],
              $this->excluded_products_detailed
            );
        }

        $csv_export_url =  'products_detailed/csv?' . http_build_query($filters);

        return view('productsDetailed', [
          'filters' => $filters,
          'products' => $products,
          'products_totals' => $products_totals,
          'refunds_totals' => $refunds_totals,
          'orders' => $orders,
          'refunds' => $refunds,
          'csv_export_url' => $csv_export_url,
          'display_shipping'=>  !$filters['sku']
        ]);
    }

    /**
     * Endpoint for the CSV version of the reporting/products_detailed report.
     * @param \Illuminate\Http\Request $request
     * @return \Symfony\Component\HttpFoundation\StreamedResponse
     */
    public function productsDetailedExport(Request $request) {
        $headers = array(
          "Content-type" => "text/csv",
          "Content-Disposition" => "attachment; filename=productsDetailed.csv",
          "Pragma" => "no-cache",
          "Cache-Control" => "must-revalidate, post-check=0, pre-check=0",
          "Expires" => "0"
        );
        $filters['sku'] = $request->input("sku");
        $filters['start_date'] = $request->input("start_date");
        $filters['end_date'] = $request->input("end_date");
        $filters['country'] = $request->input("country");
        $filters['payment_methods'] = $request->input("payment_methods");

        $ot = new OrderTransactions();
        $total_shipping = $ot->getProductSoldTotalShipping(
            $filters['sku'],
            $filters['start_date'],
            $filters['end_date'],
            $filters['country'],
            $filters['payment_methods'],
            $this->excluded_products_detailed
        );

        $ol = new OrderLine();
        $products = $ol->getProductSoldDetailed(
          $filters['sku'],
          $filters['start_date'],
          $filters['end_date'],
          $filters['country'],
          $filters['payment_methods'],
          $this->excluded_products_detailed
        );

        $orders = $ol->getOrdersForProductSoldDetailed(
          $filters['sku'],
          $filters['start_date'],
          $filters['end_date'],
          $filters['country'],
          $filters['payment_methods'],
          $this->excluded_products_detailed
        );

        $refunds = $ol->getRefundsForProductSoldDetailed(
          $filters['sku'],
          $filters['start_date'],
          $filters['end_date'],
          $filters['country'],
          $filters['payment_methods'],
          $this->excluded_products_detailed
        );


        $callback = function () use ($products, $refunds, $orders, $total_shipping) {
            $file = fopen('php://output', 'w');

            // First we print the product block.
            $products_totals['quantity'] = 0;
            $products_totals['pre_tax'] = 0;
            $products_totals['taxed'] = 0;
            $products_totals['total'] = 0;


            $columns = [
              'Product',
              'Title',
              'Sold',
              'Pre-tax Revenue',
              'Taxed',
              'Total',
              '',
              ''
            ];
            fputcsv($file, $columns);

            foreach ($products as $product) {
                $row = array(
                  $product->sku,
                  $product->name,
                  $product->sold_count,
                  '$'.number_format($product->pre_tax_revenue,2),
                  '$'.number_format($product->taxed,2),
                  '$'.number_format($product->total,2),
                  '',
                  ''
                );
                fputcsv($file, $row);

                $products_totals['quantity'] += $product->sold_count;
                $products_totals['pre_tax'] += $product->pre_tax_revenue;
                $products_totals['taxed'] += $product->taxed;
                $products_totals['total'] += $product->total;

            }

            // Add total shipping to the order
            $products_totals['total_plus_ship'] = $products_totals['total'] + $total_shipping;
            $products_totals['ship'] = $total_shipping;

            $row = [
              '',
              '',
              'Total: ' . $products_totals['quantity'],
              'Total: $' . number_format($products_totals['pre_tax'],2),
              'Total: $' . number_format($products_totals['taxed'],2),

              'Total: $' . number_format($products_totals['total'],2) .
              ' Total shipping: $' . number_format($products_totals['ship'],2) .
              ' Total + shipping: $' . number_format($products_totals['total_plus_ship'],2),
              '',
              ''
            ];
            fputcsv($file, $row);

            // Separators
            $separator = ['', '', '', '', '', '', '', ''];
            fputcsv($file, $separator);
            fputcsv($file, $separator);

            fputcsv($file, ['Refunds', '', '', '', '', '', '', '']);

            // Now the refunds block.
            $columns = [
              'Order_id',
              'Status',
              'Credit Date',
              'Paid Date',
              'Taxed',
              'Order Total',
              'Amount',
              ''
            ];
            fputcsv($file, $columns);

            $refunds_totals['taxed'] = 0;
            $refunds_totals['total'] = 0;
            $refunds_totals['refund_amount'] = 0;

            foreach ($refunds as $refund) {
                $row = array(
                  $refund->order_id,
                  $refund->status,
                  date('l, F j, Y', strtotime($refund->refund_date)),
                  date('l, F j, Y', strtotime($refund->payment_updated_at)),
                  '$'.number_format($refund->tax_total,2),
                  '$'.number_format($refund->order_total,2),
                  '$'.number_format($refund->refund_amount,2),
                  '',
                );
                fputcsv($file, $row);

                $refunds_totals['taxed'] += $refund->tax_total;
                $refunds_totals['total'] += $refund->order_total;
                $refunds_totals['refund_amount'] += $refund->refund_amount;
            }
            $row = [
              '',
              '',
              '',
              '',
              'Total: $' . number_format($refunds_totals['taxed'],2),
              'Total: $' . number_format($refunds_totals['total'],2),
              'Total: $' . number_format($refunds_totals['refund_amount'],2),
              ''
            ];
            fputcsv($file, $row);

            fputcsv($file, $separator);
            fputcsv($file, $separator);

            fputcsv($file, ['Order List', '', '', '', '', '', '', '']);

            // Now the order list.
            $columns = [
              'Order_id',
              'Payment update date',
              'Order state',
              'Pre-tax Revenue',
              'Taxed',
              'Total',
              'Total + shipp',
              ''
            ];
            fputcsv($file, $columns);

            foreach ($orders as $order) {
                $row = array(
                  $order->order_id,
                  date('l, F j, Y', strtotime($order->payment_updated_at)),
                  $order->status,
                  '$'.number_format($order->order_total - $order->tax_total,2),
                  '$'.number_format($order->tax_total,2),
                  '$'.number_format($order->order_total-$order->shipping,2),
                  '$'.number_format($order->order_total,2),
                  '',
                );
                fputcsv($file, $row);
            }
            fclose($file);
        };
        return response()->stream($callback, 200, $headers);
    }

    /**
     * Endpoint for the reporting/rcpar_nps_log report page.
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function rcparNpsLog(Request $request) {
        $promoterScores = collect([]);

        $filters['email'] = '';
        $sorting['sort_by'] = 'date';
        $sorting['sort_dir'] = 'desc';
        if ($request->getMethod() == "POST" || count($request->input()) > 0) {
            $filters['email'] = $request->input("email");
            $sorting['sort_by'] = $request->input("sort_by") ? $request->input("sort_by") : 'date';
            $sorting['sort_dir'] = $request->input("sort_dir") ? $request->input("sort_dir") : 'desc';
        }
        $params = $filters + $sorting;
        $csv_export_url = 'rcpar_nps_log/csv?' . http_build_query($params);
        $sorting_url = 'rcpar_nps_log?' . http_build_query($filters);
        $nps = new NetPromoterScore();
        $promoterScores = $nps->getNetPromoterScoreReport(
          $filters['email'],
          $sorting['sort_by'],
          $sorting['sort_dir']
        );
        return view('npsLog', [
          'promoterScores' => $promoterScores,
          'csv_export_url' => $csv_export_url,
          'sorting_url' => $sorting_url,
          'filters' => $filters,
          'sorting' => $sorting,
        ]);
    }

    /**
     * Endpoint for the CSV version of the reporting/rcpar_nps_log report.
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function rcparNpsLogExport(Request $request) {
        $headers = array(
          "Content-type" => "text/csv",
          "Content-Disposition" => "attachment; filename=rcparNpsLog.csv",
          "Pragma" => "no-cache",
          "Cache-Control" => "must-revalidate, post-check=0, pre-check=0",
          "Expires" => "0"
        );
        $filters['email'] = $request->input("email");
        $sorting['sort_by'] = $request->input("sort_by");
        $sorting['sort_dir'] = $request->input("sort_dir");
        $nps = new NetPromoterScore();
        $promoterScores = $nps->getNetPromoterScoreReport(
          $filters['email'],
          $sorting['sort_by'],
          $sorting['sort_dir'],
          false
        );
        $columns = array(
          'User ID',
          'Email',
          'Date',
          'Score',
          'Comment',
          'Purchaser Type',
          'Sku'
        );
        $callback = function () use ($promoterScores, $columns) {
            $file = fopen('php://output', 'w');
            fputcsv($file, $columns);
            foreach ($promoterScores as $score) {
                $row = array(
                  $score->uid,
                  $score->mail,
                  date('D, m/d/Y - H:i', strtotime($score->date)),
                  $score->score,
                  $score->comment,
                  $score->purchaser_type,
                  $score->skus
                );
                fputcsv($file, $row);
            }
            fclose($file);
        };
        return response()->stream($callback, 200, $headers);
    }

    /**
     * Endpoint for the reporting/taxes_by_city report page.
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function taxesByCity(Request $request) {
        $cities_info = collect([]);
        $refunds = collect([]);
        $csv_export_url = '';

        $state_totals['total'] = 0;
        $state_totals['taxable'] = 0;
        $state_totals['taxed'] = 0;

        $refunds_totals['taxable'] = 0;
        $refunds_totals['taxed'] = 0;
        $refunds_totals['total'] = 0;
        $refunds_totals['refund_amount'] = 0;

        $filters['start_date'] = date("Y-m-d", strtotime("-1 month"));
        $filters['end_date'] = date("Y-m-d");
        $filters['state'] = "CA";
        $filters['order_state'] = [];
        $filters['payment_methods'] = ['CC'];

        if ($request->getMethod() == "POST" || $request->input("state") != NULL) {

            // Form validation.
            $validatedData = $request->validate([
              'state' => 'required'
            ]);

            $filters['start_date'] = $request->input("start_date");
            $filters['end_date'] = $request->input("end_date");
            $filters['state'] = $request->input("state");
            $filters['order_state'] = $request->input("order_state");
            $filters['payment_methods'] = $request->input("payment_methods");

            $o = new OrderTransactions();
            $cities_info = $o->getTaxesByCity(
              $filters['start_date'],
              $filters['end_date'],
              $filters['state'],
              $filters['order_state'],
              $filters['payment_methods']
            );

            foreach ($cities_info as $index => $city_info) {
                // Initialize city totals: read and sum values from orders on next loop
                $city_info->taxed = 0 ;
                $city_info->taxable = 0 ;
                $city_info->total = 0 ;

                $orders_taxes = $o->getTaxesInfo(explode(',', $city_info->order_ids));
                $city_info->orders_tax_info = $orders_taxes;

                foreach ($orders_taxes as $order_tax) {
                  $city_info->taxed += $order_tax->taxes ;
                  $city_info->taxable += $order_tax->taxable ;
                  $city_info->total += $order_tax->order_total ;
                }
                $state_totals['taxable'] += $city_info->taxable;
                $state_totals['taxed'] += $city_info->taxed;
                $state_totals['total'] += $city_info->total;
            }


            $refunds = $o->getRefundsForTaxesByCity(
              $filters['start_date'],
              $filters['end_date'],
              $filters['state'],
              $filters['order_state'],
              $filters['payment_methods']
            );
            foreach ($refunds as $refund) {
                $refunds_totals['taxable'] += $refund->taxable;
                $refunds_totals['taxed'] += $refund->tax_total;
                $refunds_totals['total'] += $refund->order_total;
                $refunds_totals['refund_amount'] += $refund->refund_amount;
            }
        }

        $csv_export_url =  'taxes_by_city/csv?' . http_build_query($filters);

        return view('taxesByCity', [
          'filters' => $filters,
          'cities_info' => $cities_info,
          'state_totals' => $state_totals,
          'refunds_totals' => $refunds_totals,
          'refunds' => $refunds,
          'csv_export_url' => $csv_export_url
        ]);
    }

    /**
     * Endpoint for the CSV version of the reporting/taxes_by_city report.
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function taxesByCityExport(Request $request) {
        $headers = array(
          "Content-type" => "text/csv",
          "Content-Disposition" => "attachment; filename=taxesByCity.csv",
          "Pragma" => "no-cache",
          "Cache-Control" => "must-revalidate, post-check=0, pre-check=0",
          "Expires" => "0"
        );
        $filters['start_date'] = $request->input("start_date");
        $filters['end_date'] = $request->input("end_date");
        $filters['state'] = $request->input("state");
        $filters['order_state'] = $request->input("order_state");
        $filters['payment_methods'] = $request->input("payment_methods");

        $o = new OrderTransactions();
        $citiesInfo = $o->getTaxesByCity(
          $filters['start_date'],
          $filters['end_date'],
          $filters['state'],
          $filters['order_state'],
          $filters['payment_methods']
        );

        $refunds = $o->getRefundsForTaxesByCity(
          $filters['start_date'],
          $filters['end_date'],
          $filters['state'],
          $filters['order_state'],
          $filters['payment_methods']
        );

        $callback = function () use ($citiesInfo, $refunds, $o) {
            $file = fopen('php://output', 'w');
            $columns = array(
              'City',
              'Order total',
              'Taxable',
              'Taxed',
              'Order ids',
              '',
              ''
            );
            fputcsv($file, $columns);
            $state_totals['total'] = 0;
            $state_totals['taxable'] = 0;
            $state_totals['taxed'] = 0;
            foreach ($citiesInfo as $cityInfo) {
                // Initialize city totals: read and sum values from orders on next loop
                $cityInfo->taxed = 0 ;
                $cityInfo->taxable = 0 ;
                $cityInfo->total = 0 ;

                $orders_taxes = $o->getTaxesInfo(explode(',', $cityInfo->order_ids));
                $cityInfo->orders_tax_info = $orders_taxes;

                foreach ($orders_taxes as $order_tax) {
                  $cityInfo->taxed += $order_tax->taxes ;
                  $cityInfo->taxable += $order_tax->taxable ;
                  $cityInfo->total += $order_tax->order_total ;
                }

                $row = array(
                  $cityInfo->city_name,
                  '$'.number_format($cityInfo->total,2),
                  '$'.number_format($cityInfo->taxable,2),
                  '$'.number_format($cityInfo->taxed,2),
                  $cityInfo->order_ids,
                  '',
                  ''
                );
                fputcsv($file, $row);

                $state_totals['total'] += $cityInfo->total;
                $state_totals['taxable'] += $cityInfo->taxable;
                $state_totals['taxed'] += $cityInfo->taxed;
            }
            $row = [
              '',
              'Total: $' . number_format($state_totals['total'],2),
              'Total: $' . number_format($state_totals['taxable'],2),
              'Total: $' . number_format($state_totals['taxed'],2),
              '',
              '',
              ''
            ];
            fputcsv($file, $row);

            // Separators
            $separator = ['', '', '', '', '', '', ''];
            fputcsv($file, $separator);
            fputcsv($file, $separator);

            fputcsv($file, ['Refunds', '', '', '', '', '', '']);

            // Now the refunds block.
            $columns = [
              'Order_id',
              'Status',
              'Credit Date',
              'Paid Date',
              'Taxed',
              'Order Total',
              'Amount'
            ];
            fputcsv($file, $columns);

            $refunds_totals['taxed'] = 0;
            $refunds_totals['total'] = 0;
            $refunds_totals['refund_amount'] = 0;

            foreach ($refunds as $refund) {
                $row = array(
                  $refund->order_id,
                  $refund->status,
                  date('l, F j, Y', strtotime($refund->refund_date)),
                  date('l, F j, Y', strtotime($refund->payment_updated_at)),
                  '$'.number_format($refund->tax_total,2),
                  '$'.number_format($refund->order_total,2),
                  '$'.number_format($refund->refund_amount,2),
                );
                fputcsv($file, $row);

                $refunds_totals['taxed'] += $refund->tax_total;
                $refunds_totals['total'] += $refund->order_total;
                $refunds_totals['refund_amount'] += $refund->refund_amount;
            }
            $row = [
              '',
              '',
              '',
              '',
              'Total: $' . number_format($refunds_totals['taxed'],2),
              'Total: $' . number_format($refunds_totals['total'],2),
              'Total: $' . number_format($refunds_totals['refund_amount'],2),
            ];
            fputcsv($file, $row);

            fclose($file);
        };
        return response()->stream($callback, 200, $headers);
    }

  /**
   * Endpoint for the reporting/products_detailed report page
   * @param \Illuminate\Http\Request $request
   * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
   */
  public function intacctReport(Request $request) {
    $products = collect([]);
    $orders = collect([]);
    $refunds = collect([]);
    $csv_export_url = '';

    $products_totals['intacct'] = 0;
    $products_totals['pre_tax'] = 0;
    $products_totals['taxed'] = 0;
    $products_totals['total'] = 0;
    $products_totals['ship'] = 0;
    $products_totals['total_plus_ship'] = 0;

    $refunds_totals['taxed'] = 0;
    $refunds_totals['total'] = 0;
    $refunds_totals['refund_amount'] = 0;


    $filters['month'] = date("m", strtotime("-1 month"));
    $filters['year'] = date("Y");
    $filters['order_status'] = ['shipped','completed','pending_approval','pending'];

    if ($request->getMethod() == "POST" || $request->input("state") != NULL) {
      $filters['month'] = $request->input("month");
      $filters['year'] = $request->input("year");
      $filters['order_status'] = $request->input("order_status");

      $ol = new OrderTransactions();
      $products = $ol->getIntacctOrders(
        $filters['month'],
        $filters['year'],
        $filters['order_status']
      );
    }

    $csv_export_url =  'intacct-report/csv?' . http_build_query($filters);

    return view('intacctReport', [
      'filters' => $filters,
      'products' => $products,
      'products_totals' => $products_totals,
      'refunds_totals' => $refunds_totals,
      'orders' => $orders,
      'refunds' => $refunds,
      'csv_export_url' => $csv_export_url
    ]);
  }

  public function intacctReportExport(Request $request) {
    $filters['month'] = $request->input("month");
    $filters['year'] = $request->input("year");
    $filters['order_status'] = $request->input("order_status");

    $headers = array(
      "Content-type" => "text/csv",
      "Content-Disposition" => "attachment; filename=intacctReport-" . $filters['month'] . "-" . $filters['year'] . ".csv",
      "Pragma" => "no-cache",
      "Cache-Control" => "must-revalidate, post-check=0, pre-check=0",
      "Expires" => "0"
    );

    $ol = new OrderTransactions();
    $orders = $ol->getIntacctOrders(
      $filters['month'],
      $filters['year'],
      $filters['order_status']
    );

    $callback = function () use ($orders) {
      $file = fopen('php://output', 'w');

      // First we print the product block.
      $columns = [
        'Intacct Customer ID',
        '',
        'Terms',
        '',
        'Total Amount',
        '',
        '',
        '',
        'Course/Products',
        '',
        'Sales Price',
        'Shipping',
        'Taxes',
        'Order Date',
        'Student Name',
        'Office Location',
        'Shipping Via',
        'Shipping State',
        'Free Trial',
        'Order ID'
      ];

      fputcsv($file, $columns);

      foreach ($orders as $product) {
        $row = array(
          $product->Intacct_Customer_ID,
          '',
          $product->Terms,
          '',
          number_format($product->Total_Amount, 2),
          '',
          '',
          '',
          $product->Course_Product,
          '',
          number_format($product->Sales_Price,2),
          number_format($product->Shipping,2),
          number_format($product->Taxes,2),
          $product->Order_Date,
          $product->Student_Name,
          $product->Office_Location,
          $product->Shipping_Via,
          $product->Shipping_State,
          $product->free_trial,
          $product->order_id
        );
        fputcsv($file, $row);
      }

      fclose($file);
    };

    return response()->stream($callback, 200, $headers);
  }

}
