<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Library\Model\UserProfile;

class UserProfilesController extends Controller
{
    /**
     * Display the specified User Profile.
     *
     * @param  int $id Drupal user id (uid).
     * @return Response
     */
    public function show($uid) {
        $user = UserProfile::where('uid', $uid)->first();
        $orders = $user->getAssociatedOrders();
        $learning_stats = $user->getAssociatedLearningStats();
        return view('user_profiles.show', array(
          'user' => $user,
          'orders' => $orders,
          'learning_stats' => $learning_stats
        ));
    }
}
