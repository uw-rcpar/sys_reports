<?php

namespace App\Http\Controllers;

use App\Library\Model\IPQSessions; // Load data model
use DB;

class IPQSessionsController extends Controller {

  /**
   * @return mixed
   */
  public function getDailyList() {
    $results = IPQSessions::orderBy('last_date_finished', 'desc')->take('14')->get();
    return $results;
  }

  /**
   * @return mixed
   */
  public function getDailyIPQSessionsCounts() {
    //SELECT SUM(total_quizes_finished) AS total_completed, SUM(total_quizes_unfinished) AS total_incomplete FROM ipq_sessions;
    $results = DB::table('rpt_ipq_sessions')
      ->select(DB::raw('SUM(total_quizes_finished) AS total_completed, SUM(total_quizes_unfinished) AS total_incomplete'))
      //->where('OriginalDate', '>=', date('Y-m-d H:i:s', strtotime('-3 months')))
      //->orderBy('OriginalDate', 'DESC')
      //->groupBy('Affiliate')
      ->get();
    return $results;
  }

  /**
   * @return mixed
   */
  public function getUsageByStatus($status = 'total_quizes_finished', $limit = 10) {
    //SELECT uid, total_quizes_finished FROM ipq_sessions ORDER BY total_quizes_finished DESC;
    //SELECT uid, total_quizes_unfinished FROM ipq_sessions ORDER BY total_quizes_unfinished DESC;
    $results = DB::table('rpt_ipq_sessions')
      ->select(DB::raw('uid, ' . $status))
      ->orderBy($status, 'DESC')
      ->limit($limit)
      //->groupBy('uid')
      ->get();
    return $results;
  }

  /**
   * @param $data
   */
  public function processRecord($data) {

    // Load data model for upsert
    $dm = new IPQSessions();

    // Upsert using the model
    $dm::updateOrCreate(
      [
        'uid' => $data[0]
      ],
      [
        'quiz_type'                    => $data[1],
        'total_quizes_finished'        => $data[2],
        'avg_time_finished'            => $data[3],
        'avg_perc_complete_finished'   => $data[4],
        'avg_perc_correct_finished'    => $data[5],
        'first_date_finished'          => $data[6],
        'last_date_finished'           => $data[7],
        'total_quizes_unfinished'      => $data[8],
        'avg_time_unfinished'          => $data[9],
        'avg_perc_complete_unfinished' => $data[10],
        'avg_perc_correct_unfinished'  => $data[11],
        'first_date_unfinished'        => $data[12],
        'last_date_unfinished'         => $data[13]
      ]
    );
  }

}