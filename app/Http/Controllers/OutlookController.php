<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\App;

class OutlookController extends Controller
{
  public function mail()
  {
    $outlook = \App::get('rcpar.outlook');

    // If the App is not authenticated in Outlook, redirect the user to the authentication
    // page.
    if (!$outlook->isAuthenticated()) {
      return redirect('signin');
    }

    $availableAttachments = $outlook->getLatestsAttachmentsList();
    echo "<h2>Files ready to download:</h2>";
    echo "<ul>";
    foreach ($availableAttachments as $att) {
      $info = pathinfo($att["fileName"]);
      if (
        in_array($att["contentType"], array('text/csv', 'application/csv'))
        ||
        $info['extension'] == 'csv'
      ) {
        $fileName = basename($att["fileName"],'.'.$info['extension']);

        $app = App::getFacadeRoot();
        $csv_path = $app['config']['data_importers.csv_to_import_path'];
        $destination = $csv_path.$fileName.".csv";
        //$availableAttachments = $outlook->downloadAttachment($att["mailId"], $att["id"], $destination);
        echo "<li>" . $att["fileName"] . "</li>";
      }
    }
    echo "</ul>";
    return "";
  }

}