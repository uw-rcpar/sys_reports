<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\LegacyOrders; // Load data model
use App\LegacyOrderItems; // Load data model
use App\LegacyOrderItemTotals; // Load data model
use App\LegacyOrderItemsRevenue; // Load data model
use DB;

class LegacyOrderController extends Controller {
  /**
   * @param $data
   * Replaces table: legacy_merchandiseorders
   */
  public function processOrders($data) {

    // Load data model for upsert
    $dm = new LegacyOrders();

    // Upsert using the model
    $dm::updateOrCreate(
      ['order_id' => $data[0]],
      [
        'item_id'   => $data[0],
        'item_name' => $data[1],
        'quantity'  => $data[2],
        'revenue'   => $data[3],
      ]
    );
  }

  /**
   * @param $data
   * Replaces table: legacy_merchandiseorderitems
   */
  public function processOrderItems($data) {

    // Load data model for upsert
    $dm = new LegacyOrderItems();

    // Upsert using the model
    $dm::updateOrCreate(
      ['order_id' => $data[0]],
      [
        'item_id'   => $data[0],
        'item_name' => $data[1],
        'quantity'  => $data[2],
        'revenue'   => $data[3],
      ]
    );
  }

  /**
   * @param $data
   * Replaces table: legacy_merchandiseorderstotals
   */
  public function processOrderTotals($data) {

    // Load data model for upsert
    $dm = new LegacyOrderItemTotals();

    // Upsert using the model
    $dm::updateOrCreate(
      ['order_id' => $data[0]],
      [
        'item_id'   => $data[0],
        'item_name' => $data[1],
        'quantity'  => $data[2],
        'revenue'   => $data[3],
      ]
    );
  }

  /**
   * @param $data
   * Replaces table: rcpar_data_analysis_helper_legacy_order_item_revenue
   */
  public function processOrderItemsRevenue($data) {

    // Load data model for upsert
    $dm = new LegacyOrderItemsRevenue();

    // Upsert using the model
    $dm::updateOrCreate(
      ['order_id' => $data[0]],
      [
        'item_id'   => $data[0],
        'item_name' => $data[1],
        'quantity'  => $data[2],
        'revenue'   => $data[3],
      ]
    );
  }

  /**
   * @param $data
   * Replaces table: legacy_merchandise
   */
  public function processProducts($data) {

    // Load data model for upsert
    $dm = new LegacyProducts();

    // Upsert using the model
    $dm::updateOrCreate(
      ['order_id' => $data[0]],
      [
        'item_id'   => $data[0],
        'item_name' => $data[1],
        'quantity'  => $data[2],
        'revenue'   => $data[3],
      ]
    );
  }

}
