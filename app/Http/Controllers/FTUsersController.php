<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Library\Model\FTUsers; // Load data model
use DB;

class FTUsersController extends Controller {
  /**
   * @return mixed
   */
  public function getTotalCounts() {
    $results = DB::table('rpt_free_trial_users')
      ->select(DB::raw('COUNT(mail) as users_total, order_count as total_free_trials'))
      ->groupBy('order_count')
      ->get();
    return $results;
  }

  /**
   * @param $data
   */
  public function processRecord($data) {

    // Load data model for upsert
    $dm = new FTUsers();

    // Upsert using the model
    $dm::updateOrCreate(
      ['user_id' => $data[0]],
      [
        'mail'  => $data[1],
        'order_count' => $data[2],
        'date_first_ft'  => $data[3],
        'date_last_ft'  => $data[4],
      ]
    );
  }
}
