<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Storage;
use Illuminate\Log\Logger AS Log;

use App\Jobs\SendQueueEmail AS SendQueueEmail;

class DataImportController extends Controller {

  /**
   * @param  \Illuminate\Foundation\Application  $app
   * @param null $dataModel
   * @return \Illuminate\Http\RedirectResponse
   */
  public function loadCSV(\Illuminate\Foundation\Application $app, $dataModel = NULL) {
    // Default Redirect after processing
    $redirect = 'dashboard/reporting';

    // Set filename to be same as datamodel
    $fileName = $dataModel;

    $csv_path = $app['config']['data_importers.csv_to_import_path'];
    $pathToFile = $csv_path . $fileName . '.csv';

    $csvImporter = $app->get('rcpar.csvimporter');
    try {
      $total_rows = $csvImporter->importCSV($pathToFile);
      $dest = $app['config']['data_importers.imported_csv_path'] . date('Y-m') . '/' . $fileName . '.csv' . '-' . date('Y.m.d-H:i-T');
    }
    catch (\Exception $e) {
      $status = 'Sorry something went wrong. Please try again.';
      return redirect('dashboard')->with('status', $status);
    }

    // Send response to view.
    $status = "Inserted $total_rows Row(s), Moved file to $dest";
    return redirect($redirect)->with('status', $status);
  }

}
