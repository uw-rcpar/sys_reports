<!doctype html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>SmartPlanner - Using jQuery UI Sortable - Portlets</title>
  <!-- Latest compiled and minified CSS -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
  <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
  <link rel="stylesheet" href="https://code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css">
  <script src="https://code.jquery.com/jquery-1.9.1.js"></script>
  <script src="https://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
  <style>

    .tilt {
      transform: rotate(3deg);
      -moz-transform: rotate(3deg);
      -webkit-transform: rotate(3deg);
    }

    .highlight {
      background: ghostwhite;
    }

    body {
      min-width: 520px;
    }

    .wrap-mth {
      clear: left;
      min-height: 250px;
    / / border-style: solid;
    / / border-width: 1 px;
      border-top-style: solid;
    }

    .wrap-wk {
      width: 300px;
      float: left;
      padding: 10px;
      padding-bottom: 50px;
    / / border-color: #cccccc;
    / / border-style: solid;
      border-width: 1px;
      border-right-style: solid;
    }

    .column {
    / / width: 170 px;
      min-height: 100px;
      padding: 10px;
    }

    .portlet {
      margin: 0 1em 1em 0;
      padding: 0.3em;
    }

    .portlet-header {
      padding: 0.2em 0.3em;
      margin-bottom: 0.5em;
      position: relative;
    }

    .portlet-toggle {
      position: absolute;
      top: 50%;
      right: 0;
      margin-top: -8px;
    }

    .portlet-content {
      padding: 0.4em;
      display: none;
    }

    .portlet-placeholder {
      border: 1px dotted black;
      margin: 0 1em 1em 0;
      height: 50px;
    }

    .header-controls {
      background: #cccccc;
      height: 140px;
      border-style: solid;
    }

    .prefs, .tasks {
      float: left;
      padding: 10px;
      padding-right: 50px;
    }

    .lecture-stat {
      float: left;
    }

    .smartpath-stat {
      float: right;
    }

    .time {
      float: right;
      font-size: 12px;
      padding-right: 20px;
      padding-top: 3px;
    }

    .effort {
      background: #f3f2f2;
      font-size: 18px;
      padding: 5px;
      border: 2px dotted black;
      margin: 0 1em 1em 0;
    }

    .square-box {
      padding: 10px;
      width: 60px;
      height: 30px;
      overflow: hidden;
      float: left;
    }

    .square-content {
      color: #ffffff;
      width: 30px;
      height: 30px;
      background: #4679BD;
    }
  </style>
  <script>
    $(onPageLoad);

    function onPageLoad() {
      $(".column").sortable({
        connectWith: ".column",
        handle: ".portlet-header",
        cancel: ".portlet-toggle",
        start: function (event, ui) {
          ui.item.addClass('tilt');
        },
        stop: function (event, ui) {
          ui.item.removeClass('tilt');
        }
      });

      $(".column").mouseover(function () {
        $(this).addClass('highlight');
      });
      $(".column").mouseout(function () {
        $(this).removeClass('highlight');
      });

      $(".portlet")
        .addClass("ui-widget ui-widget-content ui-helper-clearfix ui-corner-all")
        .find(".portlet-header")
        .addClass("ui-widget-header ui-corner-all")
        .prepend("<span class='ui-icon ui-icon-plusthick portlet-toggle'></span>");

      $(".portlet-toggle").click(function () {
        var icon = $(this);
        icon.toggleClass("ui-icon-minusthick");
        icon.closest(".portlet").find(".portlet-content").toggle();
      });
    }
  </script>

</head>
<body>

<?php
date_default_timezone_set('america/Los_Angeles');

/**
 *
 * @param Array $list
 * @param int $p
 * @return multitype:multitype:
 * @link http://www.php.net/manual/en/function.array-chunk.php#75022
 */
function partition(Array $list, $p) {
  $listlen = count($list);
  $partlen = floor($listlen / $p);
  $partrem = $listlen % $p;
  $partition = array();
  $mark = 0;
  for ($px = 0; $px < $p; $px++) {
    $incr = ($px < $partrem) ? $partlen + 1 : $partlen;
    $partition[$px] = array_slice($list, $mark, $incr);
    $mark += $incr;
  }
  return $partition;
}

/* Total hours for Lectures in that SECTION
AUD = 42.1
BEC = 31.9
FAR = 67.2
REG = 41.8
*/
$sec_hours = 67.2; // hours
$lecture_completed = 95; // must meet this to be considered completed.
$add_review_time = 0;

$start_date = new DateTime(date((!empty($_REQUEST['start_study'])) ? $_REQUEST['start_study'] : date('Y-m-d') . " H:i:s"));
$end_date = new DateTime((!empty($_REQUEST['exam_date'])) ? $_REQUEST['exam_date'] : date('Y-m-') . (date('d')) . date("H:i:s"));
$end_date->add(new DateInterval('P1D')); // Add one day to the date.
$diff = $end_date->diff($start_date);
$hours = $diff->h;
$hours = $hours + ($diff->days * 24);

$weekNum = $start_date->format("W");
$effort = (!empty($_REQUEST['weekly_hours'])) ? $_REQUEST['weekly_hours'] : '1';
//$weeks = round($hours / $effort);
$weeks = round($sec_hours / $effort);
$total_months = $weeks / 4;


// SEC, Time, Time in (second)
$outline = array(
  array('AUD-0.0', '39:31', '2371'),
  array('AUD-0.1', '6:38', '398'),
  array('AUD-1.0', '23:30', '1410'),
  array('AUD-1.1', '10:06', '606'),
  array('AUD-1.2', '27:27', '1647'),
  array('AUD-1.3', '18:05', '1085'),
  array('AUD-1.4', '3:17', '197'),
  array('AUD-1.5', '21:34', '1294'),
  array('AUD-1.6', '10:33', '633'),
  array('AUD-1.7', '4:32', '272'),
  array('AUD-1.8', '17:11', '1031'),
  array('AUD-1.9', '3:18', '198'),
  array('AUD-1.10', '27:27', '1647'),
  array('AUD-1.11', '10:39', '639'),
  array('AUD-1.12', '14:23', '863'),
  array('AUD-1.13', '5:27', '327'),
  array('AUD-1.14', '7:54', '474'),
  array('AUD-1.15', '43:14', '2594'),
  array('AUD-1.16', '28:10', '1690'),
  array('AUD-2.0', '33:24', '2004'),
  array('AUD-2.1', '20:06', '1206'),
  array('AUD-2.2', '35:49', '2149'),
  array('AUD-2.3', '18:07', '1087'),
  array('AUD-2.4', '22:25', '1345'),
  array('AUD-2.5', '7:37', '457'),
  array('AUD-2.6', '17:00', '1020'),
  array('AUD-2.7', '7:25', '445'),
  array('AUD-3.0', '24:48', '1488'),
  array('AUD-3.1', '17:37', '1057'),
  array('AUD-3.2', '33:17', '1997'),
  array('AUD-3.3', '7:15', '435'),
  array('AUD-3.4', '8:42', '522'),
  array('AUD-3.5', '25:11', '1511'),
  array('AUD-3.6', '13:04', '784'),
  array('AUD-3.7', '11:59', '719'),
  array('AUD-3.8', '11:00', '660'),
  array('AUD-3.9', '11:26', '686'),
  array('AUD-3.10', '11:08', '668'),
  array('AUD-3.11', '26:21', '1581'),
  array('AUD-3.12', '23:04', '1384'),
  array('AUD-3.13', '21:48', '1308'),
  array('AUD-3.14', '13:37', '817'),
  array('AUD-3.15', '5:46', '346'),
  array('AUD-3.16', '24:48', '1488'),
  array('AUD-4.0', '23:36', '1416'),
  array('AUD-4.1', '10:53', '653'),
  array('AUD-4.2', '20:22', '1222'),
  array('AUD-4.3', '20:18', '1218'),
  array('AUD-4.4', '13:15', '795'),
  array('AUD-4.5', '26:21', '1581'),
  array('AUD-4.6', '22:18', '1338'),
  array('AUD-4.7', '20:56', '1256'),
  array('AUD-4.8', '19:45', '1185'),
  array('AUD-4.9', '16:05', '965'),
  array('AUD-4.10', '13:46', '826'),
  array('AUD-4.11', '18:42', '1122'),
  array('AUD-4.12', '4:09', '249'),
  array('AUD-4.13', '16:59', '1019'),
  array('AUD-4.14', '11:29', '689'),
  array('AUD-4.15', '12:16', '736'),
  array('AUD-4.16', '17:44', '1064'),
  array('AUD-4.17', '28:46', '1726'),
  array('AUD-4.18', '28:57', '1737'),
  array('AUD-5.0', '24:42', '1482'),
  array('AUD-5.1', '22:26', '1346'),
  array('AUD-5.2', '10:22', '622'),
  array('AUD-5.3', '6:38', '398'),
  array('AUD-5.4', '16:07', '967'),
  array('AUD-5.5', '6:54', '414'),
  array('AUD-6.0', '26:37', '1597'),
  array('AUD-6.1', '8:59', '539'),
  array('AUD-6.2', '14:24', '864'),
  array('AUD-6.3', '13:07', '787'),
  array('AUD-6.4', '19:28', '1168'),
  array('AUD-6.5', '22:46', '1366'),
  array('AUD-6.6', '22:06', '1326'),
  array('AUD-6.7', '9:34', '574'),
  array('AUD-6.8', '25:16', '1516'),
  array('AUD-7.0', '22:53', '1373'),
  array('AUD-7.1', '12:08', '728'),
  array('AUD-7.2', '05:34', '334'),
  array('AUD-7.3', '12:14', '734'),
  array('AUD-7.4', '27:22', '1642'),
  array('AUD-7.5', '4:19', '259'),
  array('AUD-7.6', '29:04', '1744'),
  array('AUD-7.7', '18:47', '1127'),
  array('AUD-7.8', '14:53', '893'),
  array('AUD-8.0', '21:34', '1294'),
  array('AUD-8.1', '19:07', '1147'),
  array('AUD-8.2', '26:25', '1585'),
  array('AUD-8.3', '28:57', '1737'),
  array('AUD-8.4', '7:39', '459'),
  array('AUD-8.5', '16:06', '966'),
  array('AUD-8.6', '4:52', '292'),
  array('AUD-9.0', '21:53', '1313'),
  array('AUD-9.1', '15:54', '954'),
  array('AUD-9.2', '02:55', '175'),
  array('AUD-9.3', '06:42', '402'),
  array('AUD-9.4', '06:01', '361'),
  array('AUD-10.0', '21:57', '1317'),
  array('AUD-10.1', '7:30', '450'),
  array('AUD-0.0', '21:12', '1272'),
  array('AUD-0.1', '9:26', '566'),
  array('AUD-0.2', '10:06', '606'),
  array('AUD-0.3', '12:18', '738'),
  array('AUD-0.4', '9:33', '573'),
  array('AUD-0.5', '17:10', '1030'),
  array('AUD-0.6', '25:04', '1504'),
  array('AUD-0.7', '5:27', '327'),
  array('AUD-0.8', '8:30', '510'),
  array('AUD-0.9', '7:55', '475'),
  array('AUD-0.10', '47:32', '2852'),
  array('AUD-0.11', '24:30', '1470'),
  array('AUD-0.12', '49:17', '2957'),
  array('AUD-0.13', '57:20', '3440'),
  array('AUD-0.14', '44:35', '2675'),
  array('AUD-0.15', '32:47', '1967'),
  array('AUD-0.16', '34:52', '2092'),
  array('AUD-0.17', '19:28', '1168'),
  array('AUD-0.18', '16:43', '1003'),
  array('AUD-0.19', '23:21', '1401'),
  array('AUD-0.20', '6:31', '391'),
  array('AUD-0.21', '11:04', '664'),
  array('AUD-0.22', '33:21', '2001'),
  array('AUD-0.23', '21:26', '1286'),
  array('AUD-0.24', '9:22', '562'),
  array('AUD-0.25', '10:46', '646'),
  array('AUD-0.26', '44:22', '2662'),
  array('AUD-0.27', '32:17', '1937'),
  array('AUD-0.28', '20:35', '1235'),
  array('AUD-0.29', '21:25', '1285'),
  array('AUD-0.30', '7:30', '450'),
  array('AUD-0.31', '35:33', '2133'),
  array('AUD-0.32', '16:17', '977'),
  array('AUD-11.0', '35:33', '2133'),
  array('AUD-11.1', '16:17', '977')
);
$outline = array(
  array('FAR-0.0', '39:31', '2371'),
  array('FAR-0.1', '6:53', '413'),
  array('FAR-1.0', '26:01', '1561'),
  array('FAR-1.1', '3:16', '196'),
  array('FAR-1.2', '23:22', '1402'),
  array('FAR-1.3', '14:13', '853'),
  array('FAR-1.4', '12:51', '771'),
  array('FAR-1.5', '4:08', '248'),
  array('FAR-1.6', '19:58', '1198'),
  array('FAR-1.7', '12:25', '745'),
  array('FAR-1.8', '3:49', '229'),
  array('FAR-1.9', '4:02', '242'),
  array('FAR-2.0', '13:20', '800'),
  array('FAR-2.1', '6:33', '393'),
  array('FAR-2.2', '12:54', '774'),
  array('FAR-2.3', '7:35', '455'),
  array('FAR-2.4', '23:52', '1432'),
  array('FAR-2.5', '27:55', '1675'),
  array('FAR-2.6', '16:27', '987'),
  array('FAR-2.7', '22:26', '1346'),
  array('FAR-2.8', '15:48', '948'),
  array('FAR-2.9', '19:32', '1172'),
  array('FAR-3.0', '25:22', '1522'),
  array('FAR-3.1', '13:18', '798'),
  array('FAR-3.2', '7:25', '445'),
  array('FAR-3.3', '9:41', '581'),
  array('FAR-3.4', '9:28', '568'),
  array('FAR-3.5', '14:02', '842'),
  array('FAR-3.6', '11:43', '703'),
  array('FAR-4.0', '33:50', '2030'),
  array('FAR-4.1', '15:37', '937'),
  array('FAR-4.2', '16:57', '1017'),
  array('FAR-4.3', '4:48', '288'),
  array('FAR-4.4', '11:29', '689'),
  array('FAR-5.0', '16:48', '1008'),
  array('FAR-5.1', '10:58', '658'),
  array('FAR-5.2', '7:32', '452'),
  array('FAR-5.3', '5:36', '336'),
  array('FAR-5.4', '3:28', '208'),
  array('FAR-6.0', '13:20', '800'),
  array('FAR-6.1', '13:27', '807'),
  array('FAR-6.2', '13:16', '796'),
  array('FAR-6.3', '5:51', '351'),
  array('FAR-6.4', '11:12', '672'),
  array('FAR-6.5', '17:46', '1066'),
  array('FAR-6.6', '3:31', '211'),
  array('FAR-7.0', '23:11', '1391'),
  array('FAR-7.1', '9:47', '587'),
  array('FAR-7.2', '17:26', '1046'),
  array('FAR-7.3', '20:52', '1252'),
  array('FAR-7.4', '8:37', '517'),
  array('FAR-7.5', '9:37', '577'),
  array('FAR-7.6', '13:20', '800'),
  array('FAR-7.7', '4:18', '258'),
  array('FAR-7.8', '5:03', '303'),
  array('FAR-7.9', '5:03', '303'),
  array('FAR-7.10', '11:37', '697'),
  array('FAR-7.11', '27:23', '1643'),
  array('FAR-8.0', '11:22', '682'),
  array('FAR-8.1', '7:24', '444'),
  array('FAR-8.2', '7:30', '450'),
  array('FAR-8.3', '7:35', '455'),
  array('FAR-8.4', '11:53', '713'),
  array('FAR-8.5', '17:38', '1058'),
  array('FAR-8.6', '13:56', '836'),
  array('FAR-8.7', '9:48', '588'),
  array('FAR-8.8', '10:38', '638'),
  array('FAR-8.9', '3:17', '197'),
  array('FAR-8.10', '13:45', '825'),
  array('FAR-8.11', '16:12', '972'),
  array('FAR-8.12', '18:12', '1092'),
  array('FAR-8.13', '2:18', '138'),
  array('FAR-8.14', '34:33', '2073'),
  array('FAR-9.0', '16:41', '1001'),
  array('FAR-9.1', '15:34', '934'),
  array('FAR-9.2', '13:48', '828'),
  array('FAR-9.3', '6:25', '385'),
  array('FAR-9.4', '7:50', '470'),
  array('FAR-9.5', '2:06', '126'),
  array('FAR-10.0', '15:48', '948'),
  array('FAR-10.1', '18:47', '1127'),
  array('FAR-10.2', '19:32', '1172'),
  array('FAR-10.3', '21:06', '1266'),
  array('FAR-10.4', '3:52', '232'),
  array('FAR-10.5', '25:27', '1527'),
  array('FAR-11.0', '25:48', '1548'),
  array('FAR-11.1', '10:44', '644'),
  array('FAR-11.2', '23:36', '1416'),
  array('FAR-11.3', '02:13', '133'),
  array('FAR-11.4', '10:21', '621'),
  array('FAR-11.5', '7:12', '432'),
  array('FAR-11.6', '9:04', '544'),
  array('FAR-11.7', '2:08', '128'),
  array('FAR-11.8', '4:16', '256'),
  array('FAR-11.9', '14:26', '866'),
  array('FAR-12.0', '10:18', '618'),
  array('FAR-12.1', '16:43', '1003'),
  array('FAR-12.2', '19:27', '1167'),
  array('FAR-12.3', '12:32', '752'),
  array('FAR-12.4', '4:05', '245'),
  array('FAR-12.5', '8:23', '503'),
  array('FAR-12.6', '17:39', '1059'),
  array('FAR-12.7', '8:08', '488'),
  array('FAR-12.8', '1:58', '118'),
  array('FAR-13.0', '27:55', '1675'),
  array('FAR-13.1', '13:11', '791'),
  array('FAR-13.2', '9:28', '568'),
  array('FAR-13.3', '12:16', '736'),
  array('FAR-13.4', '17:06', '1026'),
  array('FAR-13.5', '4:29', '269'),
  array('FAR-13.6', '3:44', '224'),
  array('FAR-13.7', '26:07', '1567'),
  array('FAR-14.0', '23:37', '1417'),
  array('FAR-14.1', '29:26', '1766'),
  array('FAR-14.2', '14:46', '886'),
  array('FAR-14.3', '6:53', '413'),
  array('FAR-14.4', '18:38', '1118'),
  array('FAR-14.5', '12:26', '746'),
  array('FAR-14.6', '4:59', '299'),
  array('FAR-14.7', '0:55', '55'),
  array('FAR-15.0', '14:11', '851'),
  array('FAR-15.1', '21:54', '1314'),
  array('FAR-15.2', '34:40', '2080'),
  array('FAR-15.3', '16:49', '1009'),
  array('FAR-15.4', '8:08', '488'),
  array('FAR-15.5', '17:46', '1066'),
  array('FAR-15.6', '22:37', '1357'),
  array('FAR-15.7', '6:35', '395'),
  array('FAR-15.8', '1:06', '66'),
  array('FAR-16.0', '20:44', '1244'),
  array('FAR-16.1', '18:17', '1097'),
  array('FAR-16.2', '8:23', '503'),
  array('FAR-16.3', '23:02', '1382'),
  array('FAR-16.4', '01:07', '67'),
  array('FAR-17.0', '12:57', '777'),
  array('FAR-17.1', '8:48', '528'),
  array('FAR-17.2', '02:41', '161'),
  array('FAR-17.3', '06:46', '406'),
  array('FAR-17.4', '8:30', '510'),
  array('FAR-17.5', '1:11', '71'),
  array('FAR-17.6', '20:47', '1247'),
  array('FAR-17.7', '02:14', '134'),
  array('FAR-18.0', '23:21', '1401'),
  array('FAR-18.1', '11:24', '684'),
  array('FAR-18.2', '8:21', '501'),
  array('FAR-18.3', '14:28', '868'),
  array('FAR-18.4', '6:08', '368'),
  array('FAR-18.5', '2:34', '154'),
  array('FAR-18.6', '1:19', '79'),
  array('FAR-19.0', '15:53', '953'),
  array('FAR-19.1', '7:59', '479'),
  array('FAR-19.2', '15:28', '928'),
  array('FAR-19.3', '10:14', '614'),
  array('FAR-19.4', '9:25', '565'),
  array('FAR-19.5', '12:15', '735'),
  array('FAR-19.6', '5:19', '319'),
  array('FAR-19.7', '11:54', '714'),
  array('FAR-19.8', '4:15', '255'),
  array('FAR-19.9', '0:57', '57'),
  array('FAR-20.0', '06:12', '372'),
  array('FAR-20.1', '3:35', '215'),
  array('FAR-20.2', '1:08', '68'),
  array('FAR-20.3', '1:21', '81'),
  array('FAR-21.0', '17:06', '1026'),
  array('FAR-21.1', '3:59', '239'),
  array('FAR-21.2', '1:15', '75'),
  array('FAR-21.3', '0:51', '51'),
  array('FAR-22.0', '18:54', '1134'),
  array('FAR-22.1', '5:55', '355'),
  array('FAR-23.0', '31:53', '1913'),
  array('FAR-23.1', '5:10', '310'),
  array('FAR-24.0', '4:12', '252'),
  array('FAR-24.1', '2:55', '175'),
  array('FAR-25.0', '15:43', '943'),
  array('FAR-25.1', '7:27', '447'),
  array('FAR-25.2', '23:00', '1380'),
  array('FAR-25.3', '13:03', '783'),
  array('FAR-25.4', '8:27', '507'),
  array('FAR-25.5', '31:55', '1915'),
  array('FAR-25.6', '7:08', '428'),
  array('FAR-25.7', '3:48', '228'),
  array('FAR-25.8', '1:13', '73'),
  array('FAR-26.0', '6:28', '388'),
  array('FAR-26.1', '5:56', '356'),
  array('FAR-27.0', '10:16', '616'),
  array('FAR-27.1', '2:59', '179'),
  array('FAR-28.0', '6:42', '402'),
  array('FAR-28.1', '20:56', '1256'),
  array('FAR-28.2', '7:04', '424'),
  array('FAR-29.0', '31:00', '1860'),
  array('FAR-29.1', '22:43', '1363'),
  array('FAR-29.2', '24:23', '1463'),
  array('FAR-29.3', '14:29', '869'),
  array('FAR-29.4', '9:13', '553'),
  array('FAR-29.5', '6:01', '361'),
  array('FAR-29.6', '7:49', '469'),
  array('FAR-29.7', '8:05', '485'),
  array('FAR-29.8', '23:06', '1386'),
  array('FAR-29.9', '23:02', '1382'),
  array('FAR-29.10', '6:46', '406'),
  array('FAR-29.11', '7:26', '446'),
  array('FAR-29.12', '10:24', '624'),
  array('FAR-29.13', '7:46', '466'),
  array('FAR-29.14', '19:43', '1183'),
  array('FAR-30.0', '21:09', '1269'),
  array('FAR-30.1', '10:01', '601'),
  array('FAR-30.2', '17:52', '1072'),
  array('FAR-30.3', '8:04', '484'),
  array('FAR-30.4', '14:16', '856'),
  array('FAR-30.5', '16:01', '961'),
  array('FAR-30.6', '7:44', '464'),
  array('FAR-30.7', '18:32', '1112'),
  array('FAR-31.0', '25:31', '1531'),
  array('FAR-31.1', '10:38', '638'),
  array('FAR-31.2', '13:26', '806'),
  array('FAR-31.3', '12:36', '756'),
  array('FAR-31.4', '06:06', '366'),
  array('FAR-31.5', '10:03', '603'),
  array('FAR-31.6', '13:49', '829'),
  array('FAR-31.7', '21:55', '1315'),
  array('FAR-31.8', '11:19', '679'),
  array('FAR-31.9', '14:51', '891'),
  array('FAR-31.10', '3:42', '222'),
  array('FAR-31.11', '0:41', '41'),
  array('FAR-32.0', '7:30', '450'),
  array('FAR-0.0', '23:52', '1432'),
  array('FAR-0.1', '15:41', '941'),
  array('FAR-0.2', '13:32', '812'),
  array('FAR-0.3', '13:58', '838'),
  array('FAR-0.4', '13:25', '805'),
  array('FAR-0.5', '8:31', '511'),
  array('FAR-0.6', '3:11', '191'),
  array('FAR-0.7', '21:24', '1284'),
  array('FAR-0.8', '31:36', '1896'),
  array('FAR-0.9', '17:08', '1028'),
  array('FAR-0.10', '16:06', '966'),
  array('FAR-0.11', '15:15', '915'),
  array('FAR-0.12', '30:34', '1834'),
  array('FAR-0.13', '38:37', '2317'),
  array('FAR-0.14', '20:10', '1210'),
  array('FAR-0.15', '23:20', '1400'),
  array('FAR-0.16', '25:06', '1506'),
  array('FAR-0.17', '26:42', '1602'),
  array('FAR-0.18', '27:00', '1620'),
  array('FAR-0.19', '7:57', '477'),
  array('FAR-0.20', '18:44', '1124'),
  array('FAR-0.21', '9:49', '589'),
  array('FAR-0.22', '18:35', '1115'),
  array('FAR-0.23', '26:23', '1583'),
  array('FAR-0.24', '19:53', '1193'),
  array('FAR-0.25', '19:45', '1185'),
  array('FAR-0.26', '34:10', '2050'),
  array('FAR-0.27', '29:50', '1790'),
  array('FAR-0.28', '28:12', '1692'),
  array('FAR-0.29', '16:48', '1008'),
  array('FAR-0.30', '42:07', '2527'),
  array('FAR-0.31', '22:31', '1351'),
  array('FAR-0.32', '23:32', '1412'),
  array('FAR-0.33', '20:57', '1257'),
  array('FAR-0.34', '23:15', '1395'),
  array('FAR-0.35', '21:01', '1261'),
  array('FAR-0.36', '8:53', '533'),
  array('FAR-0.37', '5:35', '335'),
  array('FAR-0.38', '17:55', '1075'),
  array('FAR-0.39', '15:23', '923'),
  array('FAR-0.40', '14:46', '886'),
  array('FAR-0.41', '31:25', '1885'),
  array('FAR-0.42', '33:28', '2008'),
  array('FAR-0.43', '36:52', '2212'),
  array('FAR-0.44', '24:35', '1475'),
  array('FAR-0.45', '23:20', '1400'),
  array('FAR-0.46', '9:27', '567'),
  array('FAR-0.47', '21:48', '1308'),
  array('FAR-0.48', '19:09', '1149'),
  array('FAR-0.49', '19:00', '1140'),
  array('FAR-0.50', '4:51', '291'),
  array('FAR-0.51', '9:21', '561'),
  array('FAR-0.52', '19:43', '1183'),
  array('FAR-0.53', '63:15', '3795'),
  array('FAR-0.54', '08:10', '490'),
  array('FAR-0.55', '31:05', '1865'),
  array('FAR-0.56', '15:17', '917'),
  array('FAR-33.0', '31:04', '1864'),
  array('FAR-33.1', '15:16', '916')
);
//print_r($outline);
echo "<pre>";
//print_r(partition($outline, $weeks, true));
echo "</pre>";
?>
<b>Content</b><br>
AUD = 42.1 Hrs<br>
BEC = 31.9 Hrs<br>
FAR = 67.2 Hrs<br>
REG = 41.8 Hrs<br>

<br>
Section Hours: <?php echo $sec_hours; ?><br>

<br>
<b>Time Frame</b><br>
Weeks Until Exam: <?php echo $weeks; ?><br>
Months Until Exam: <?php echo $total_months; ?><br>
<br>
Available Hours Until Exam: <?php echo $hours; ?><br>

<br>
<form method="POST" name="prefs">
  <div class="header-controls">
    <div class="prefs">
      <div><b>Study Preferences:</b></div>
      <div>Start Studying Date:
        <input type="textbox" name="start_study" value="<?php echo (!empty($_REQUEST['start_study'])) ? $_REQUEST['start_study'] : '2018-10-15'; ?>" size="15">
      </div>
      <div>Exam Scheduled Date:
        <input type="textbox" name="exam_date" value="<?php echo (!empty($_REQUEST['exam_date'])) ? $_REQUEST['exam_date'] : '2019-02-15'; ?>" size="15">
      </div>
      <div>Estimated Completion Date: 08/24/2019</div>
      <div>Estimated Study Time: 9 months, 4 days</div>
    </div>
    <div class="prefs">
      <div>&nbsp;</div>
      <div>Hours weekly:
        <input type="textbox" name="weekly_hours" value="<?php echo (!empty($_REQUEST['weekly_hours'])) ? $_REQUEST['weekly_hours'] : $effort; ?>" size="5">
      </div>
      <!-- div>Target Score:
        <select name="target_score">
          <option value="75">75 Score</option>
          <option value="85">85 Score</option>
          <option value="95">95 or better</option>
        </select>
      </div -->
    </div>
    <div class="tasks">
      <div><b>Dsiplay Preferences:</b></div>
      <div><input type="radio" name="hide_completed" value="none" checked="checked"> Display All</div>
      <div><input type="radio" name="hide_completed" value="targets"> Hide when SmartPath Targets Met</div>
      <div><input type="radio" name="hide_completed" value="lectures">Hide when Lectures Completed</div>
      <div><input type="radio" name="hide_completed" value="all">Hide when both Targets Met & Lectures Completed</div>
    </div>
    <div class="tasks">
      <div><b>Optional Preferences:</b></div>
      <div>Add review time:
        <input type="textbox" name="add_review_time" value="<?php echo (!empty($_REQUEST['add_review_time'])) ? $_REQUEST['add_review_time'] : '2w'; ?>" size="4"> (i.e. 2w, 2d<a href>?</a>)
      </div>
      <div><input type="submit" value="recalculate"></div>
      <div>
        Planner Profile: <select name="planner_profile">
          <option value="default">Default Recommendation</option>
          <option value="1">High Pressure Plan - 10/01/2018 02:45pm</option>
          <option value="2">Party's Included Plan - 10/15/2018 01:02am</option>
        </select> <input type="submit" value="save">
      </div>
    </div>
  </div>

  <h1>FAR Course</h1>

  <?php
  $week = 0;
  $wloop = 1;
  $month = 1;

  function week_date($week, $year) {
    $date = new DateTime();
    return array(
      $date->setISODate($year, $week, "1")->format('M d'),
      $date->setISODate($year, $week, "7")->format('M d')
    );
  }

  // Last topic we assigned to a weekly column
  $topicIndex = 0;
  // total time for topics assigned
  $topicsDuration = 0;
  $topicCount = 0;

  /*
   * This loop creates the Monthly containers for the weeks.
   */
  while ($week < $weeks) {
    $weekNum = ($weekNum > 52) ? 1 : $weekNum;
    if ($wloop == 1) {
      echo '<div class="wrap-mth" id="' . $week . '"> Month: ' . $month;
    }

    $wloop++;
    $week++;
    $wy = ($weekNum <= 52) ? date('Y') : date('Y') + 1;
    $wd = week_date($weekNum, $wy);

    echo '<div class="wrap-wk">
    <div>(W' . $weekNum . ')  ' . $wd[0] . ' - ' . $wd[1] . '</div>';

    /*
    echo '<div class="effort">
      <div>Study Time: 4 Hours, 32 Minutes</div>
      <div>Lectures: 2</div>
      <div>Questions: 87</div>
    </div>';
    */

    echo '<div class="week column" id="' . $weekNum . '">';

    /*
     * This loops through the lecture outline and puts each topic into the weeks without exceeding the time limits the user entered.
     */
    foreach ($outline AS $k => $v) {
      if (array_key_exists($topicIndex, $outline)) {
        $topicsDuration += $outline[$topicIndex][2];
        $topic_obj = $outline[$topicIndex];

        echo '<div class="topics portlet" time="' . $topic_obj[2] . '" id="' . $topicIndex . '">
        <div class="portlet-header">' . $topic_obj[0] . ' <div class="time">(' . $topic_obj[1] . ')</div></div>
        <div class="portlet-content">Lorem ipsum dolor sit amet, consectetuer adipiscing elit</div>
        <div class="lecture-stat" met="46"><i class="material-icons">computer</i> 46%</div>
        <div class="smartpath-stat" met="100"><i class="material-icons">school</i> 100%</div>
      </div>';

        /*
         * This chunk was to test smaller boxes for display but was not created to be draggable and most liekly would need to be.
         */
        /*
        echo '<div class="square-box">
                <div class="square-content">' . str_ireplace('FAR-', '', $topic_obj[0]) . ' <div class="time">(' . $topic_obj[1] . ')</div></div>
              </div>';
        */
        $topicCount++;
      }

      $topicIndex++;

      /*
       *  Put topics into next week if we exceed effort time.
       */
      if (round(($topicsDuration / 60) / 60, 1) >= $effort) {
        //echo round(($topicsDuration/60)/60,1) . " >= $effort <br>";
        //echo "Skip to next week<br>";
        break;
      }
    }

    echo '
    </div>
    <div class="effort">
      <div>Study Time: ' . round(($topicsDuration / 60) / 60, 1) . ' Hour(s)</div>
      <div>Lectures: ' . $topicCount . '</div>
    </div>
    </div>';

    $topicsDuration = 0;
    $weekNum++;
    if ($wloop == 5) {
      echo '</div>';
      $wloop = 1;
      $month++;
    }

    // Reset the topic counts for each week
    $topicCount = 0;
  }
  ?>


</body>

</html>
