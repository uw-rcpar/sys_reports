
$(function() {
    $(".toggler").click(function () {
        var target = $(this).data('target');
        if ($('#' + target).hasClass("hidden")) {
            $('#' + target).removeClass("hidden");
        }
        else {
            $('#' + target).addClass("hidden");
        }
        return false;
    });
});
