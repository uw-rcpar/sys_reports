<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('dashboard', 'DashboardController@index')->middleware('auth.basic');
Route::get('dashboard/reporting', 'DashboardController@reportingIndex')->middleware('auth.basic');
Route::get('reporting/CA-use-tax-report', 'ReportsController@caUseTaxReport')->middleware('auth.basic');
Route::post('reporting/CA-use-tax-report', 'ReportsController@caUseTaxReport')->middleware('auth.basic');

Route::get('reporting/taxable_per_state_and_product', 'ReportsController@taxablePerStateAndProduct')->middleware('auth.basic');
Route::post('reporting/taxable_per_state_and_product', 'ReportsController@taxablePerStateAndProduct')->middleware('auth.basic');
Route::get('reporting/taxable_per_state_and_product/csv', 'ReportsController@taxablePerStateAndProductExport')->middleware('auth.basic');

Route::get('reporting/products_detailed', 'ReportsController@productsDetailed')->middleware('auth.basic');
Route::post('reporting/products_detailed', 'ReportsController@productsDetailed')->middleware('auth.basic');
Route::get('reporting/products_detailed/csv', 'ReportsController@productsDetailedExport')->middleware('auth.basic');

Route::get('reporting/rcpar_nps_log', 'ReportsController@rcparNpsLog')->middleware('auth.basic');
Route::post('reporting/rcpar_nps_log', 'ReportsController@rcparNpsLog')->middleware('auth.basic');
Route::get('reporting/rcpar_nps_log/csv', 'ReportsController@rcparNpsLogExport')->middleware('auth.basic');

Route::get('reporting/taxes_by_city', 'ReportsController@taxesByCity')->middleware('auth.basic');
Route::post('reporting/taxes_by_city', 'ReportsController@taxesByCity')->middleware('auth.basic');
Route::get('reporting/taxes_by_city/csv', 'ReportsController@taxesByCityExport')->middleware('auth.basic');

Route::get('reporting/intacct-report', 'ReportsController@intacctReport')->middleware('auth.basic');
Route::post('reporting/intacct-report', 'ReportsController@intacctReport')->middleware('auth.basic');
Route::get('reporting/intacct-report/csv', 'ReportsController@intacctReportExport')->middleware('auth.basic');

Route::get('api/process/{dataModel}', 'DataImportController@loadCSV')->middleware('auth.basic');

Route::resource('partners', 'PartnerProfilesController')->middleware('auth.basic');
Route::post('partners', 'PartnerProfilesController@index')->middleware('auth.basic');
Route::resource('user_profiles', 'UserProfilesController')->middleware('auth.basic');
Route::resource('orders', 'OrdersController')->middleware('auth.basic');

Route::get('/signin', 'AuthController@signin')->middleware('auth.basic');
Route::get('/ra/authorize', 'AuthController@gettoken')->middleware('auth.basic');
Route::get('/mail', 'OutlookController@mail')->name('mail')->middleware('auth.basic');

Route::get('system/logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index')->middleware('auth.basic');

Route::get('/', function () {
  return redirect('dashboard');
});