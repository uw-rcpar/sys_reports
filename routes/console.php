<?php

use Illuminate\Foundation\Inspiring;

/*
|--------------------------------------------------------------------------
| Console Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of your Closure based console
| commands. Each Closure is bound to a command instance allowing a
| simple approach to interacting with each command's IO methods.
|
*/

Artisan::command('csvimport:list', function () {

  $csvImporter = \App::get('rcpar.csvimporter');

  $i = 1;
  $headers = ['#', 'File_Name'];
  $data = array();

  foreach ($csvImporter->listCSVAvailableToImport() AS $f => $v) {
    $data[] = array($i, $v);
    $i++;
  }
  print $this->table($headers, $data);

})->describe('Command \'list\' returns a list of CSV files available to be imported');