<!-- Stored in resources/views/layouts/.blade.php -->
<html lang="en">
<head>
    <title><?php echo $__env->yieldContent('title'); ?></title>
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>
<nav class="navbar navbar-default">
    <div class="container-fluid">
        <div class="navbar-header">
            <a class="navbar-brand" href="#"><?php echo $__env->yieldContent('title'); ?> Dashboard:</a>

            <ul class="nav navbar-nav">
                <li><a href="<?php echo e(URL::to('dashboard')); ?>">Systems</a></li>

                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Reporting<span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="<?php echo e(URL::to('dashboard/reporting')); ?>">Dashboard</a></li>
                        <li role="separator" class="divider"></li>
                        <li><a href="<?php echo e(URL::to('api/process/IPQSessionTransactions')); ?>">Process IPQ Sessions (IPQSessionTransactions)</a></li>
                        <li><a href="<?php echo e(URL::to('api/process/AffiliateTransactions')); ?>">Process LinkConnector (AffiliateTransactions)</a></li>
                        <li><a href="<?php echo e(URL::to('api/process/FTDaily')); ?>">Process Free Trial Daily (FTDaily)</a></li>
                    </ul>
                </li>

            </ul>

        </div>
    </div>
</nav>

<?php if(session('status')): ?>
    <div class="alert alert-success">
        <?php echo e(session('status')); ?>

    </div>
<?php endif; ?>

<?php $__env->startSection('sidebar'); ?>
    <p>This is the master sidebar.</p>
<?php $__env->stopSection(); ?>

<div class="container">
    <?php echo $__env->yieldContent('content'); ?>
</div>

</body>
</html>
