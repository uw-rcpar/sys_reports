<!-- Stored in resources/views/dashboard.blade.php -->


<?php $__env->startSection('title', 'Systems'); ?>

<?php $__env->startSection('content'); ?>

    <div class="row">
        <div class="col-xs-6 col-md-4">
            <div class="panel panel-default">
                <div class="panel-heading">Browser Performance</div>
                <div class="panel-body">

                    <table class="table table-striped task-table">
                        <!-- Table Headings -->
                        <thead>
                        <th>Browser Load Time</th>
                        </thead>
                        <!-- Table Body -->
                        <tbody>

                        <tr>
                            <td class="table-text">
                                <div><?php echo e($browser_lt); ?></div>
                            </td>
                        </tr>

                        </tbody>
                    </table>

                </div>
            </div>
        </div>

        <div class="col-xs-6 col-md-4">
            <div class="panel panel-default">
                <div class="panel-heading">Application Performance</div>
                <div class="panel-body">

                    <table class="table table-striped task-table">
                        <!-- Table Headings -->
                        <thead>
                        <th>App Server</th>
                        <th>Errors</th>
                        </thead>
                        <!-- Table Body -->
                        <tbody>
                        <tr>
                            <td class="table-text">
                                <div><?php echo e($app_server); ?></div>
                            </td>
                            <td class="table-text">
                                <div><?php echo e($app_errors); ?></div>
                            </td>
                        </tr>
                        </tbody>
                    </table>

                </div>
            </div>
        </div>

    </div>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.dashboard', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>