<!-- Stored in resources/views/dashboard.blade.php -->


<?php $__env->startSection('title', 'Reporting'); ?>

<?php $__env->startSection('content'); ?>

    <div class="row">
        <div class="col-xs-6 col-md-4">
            <div class="panel panel-default">
                <div class="panel-heading">Free Trial Orders: Last 7 Days</div>
                <div class="panel-body">

                    <table class="table table-striped task-table">
                        <!-- Table Headings -->
                        <thead>
                        <th>Day Created</th>
                        <th>Total Orders</th>
                        </thead>
                        <!-- Table Body -->
                        <tbody>
                        <?php $__currentLoopData = $ft_daily_snapshot; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $o): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <tr>
                                <td class="table-text">
                                    <div><?php echo e($o->create_day); ?></div>
                                </td>
                                <td class="table-text">
                                    <div><?php echo e($o->total_count); ?></div>
                                </td>
                            </tr>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tbody>
                    </table>

                </div>
            </div>
        </div>

        <div class="col-xs-6 col-md-4">
            <div class="panel panel-default">
                <div class="panel-heading"># Free Trials Per User</div>
                <div class="panel-body">

                    <table class="table table-striped task-table">
                        <!-- Table Headings -->
                        <thead>
                        <th>Total Users</th>
                        <th>Free Trial Orders Each</th>
                        </thead>
                        <!-- Table Body -->
                        <tbody>
                        <?php $__currentLoopData = $ft_users_snapshot; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $ftu): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <tr>
                                <td class="table-text">
                                    <div><?php echo e($ftu->users_total); ?></div>
                                </td>
                                <td class="table-text">
                                    <div><?php echo e($ftu->total_free_trials); ?></div>
                                </td>
                            </tr>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tbody>
                    </table>

                </div>
            </div>
        </div>

        <div class="col-xs-6 col-md-4">
            <div class="panel panel-default">
                <div class="panel-heading">Free Trials (Purchases)</div>
                <div class="panel-body">

                    <table class="table table-striped task-table">
                        <!-- Table Headings -->
                        <thead>
                        <th>Time Groups</th>
                        <th>Total Users</th>
                        <!-- th>Purchases</th>
                        <th>FT Date</th>
                        <th>Purchased</th -->
                        </thead>
                        <!-- Table Body -->
                        <tbody>
                        <?php $__currentLoopData = $ft_paid_snapshot; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $ftp): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <tr>
                                <td class="table-text">
                                    <div><?php echo e($ftp->groups); ?></div>
                                </td>
                                <td class="table-text">
                                    <div><?php echo e($ftp->total_users); ?></div>
                                </td>
                            <!-- td class="table-text">
                                  <div><?php echo e($ftp->purchase_count); ?></div>
                              </td>
                              <td class="table-text">
                                  <div><?php echo e($ftp->ft); ?></div>
                              </td>
                              <td class="table-text">
                                  <div><?php echo e($ftp->purchase); ?></div>
                              </td -->
                            </tr>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12 col-md-8">
            <div class="panel panel-default">
                <div class="panel-heading">LinkConnector Transactions</div>
                <div class="panel-body">

                    <table class="table table-striped task-table">
                        <!-- Table Headings -->
                        <thead>
                        <th>Created</th>
                        <th>Affiliate</th>
                        <th>Amount</th>
                        <th>Commission</th>
                        <th>LCFees</th>
                        <th>Total Cost</th>
                        <th>Event Type</th>
                        </thead>
                        <!-- Table Body -->

                        <tbody>
                        <?php $__currentLoopData = $affiliates_snapshot; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $o): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <tr>
                                <td class="table-text">
                                    <div><?php echo e($o->OriginalDate); ?></div>
                                </td>
                                <td class="table-text">
                                    <div><?php echo e($o->Affiliate); ?></div>
                                </td>
                                <td class="table-text">
                                    <div>$<?php echo e($o->Amount); ?></div>
                                </td>
                                <td class="table-text">
                                    <div>$<?php echo e($o->Commission); ?></div>
                                </td>
                                <td class="table-text">
                                    <div>$<?php echo e($o->LCFees); ?></div>
                                </td>
                                <td class="table-text">
                                    <div>$<?php echo e($o->TotalCost); ?></div>
                                </td>
                                <td class="table-text">
                                    <div><?php echo e($o->EventType); ?></div>
                                </td>
                            </tr>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tbody>
                    </table>

                </div>
            </div>
        </div>

        <div class="col-xs-6 col-md-4">
            <div class="panel panel-default">
                <div class="panel-heading">LinkConnector Performance (last 3 months)</div>
                <div class="panel-body">

                    <table class="table table-striped task-table">
                        <!-- Table Headings -->
                        <thead>
                        <th>Affiliate</th>
                        <th>Orders</th>
                        <th>Cost</th>
                        </thead>
                        <!-- Table Body -->

                        <tbody>
                        <?php $__currentLoopData = $affiliates_stats_snapshot; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $o): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <tr>
                                <td class="table-text">
                                    <div><?php echo e($o->Affiliate); ?></div>
                                </td>
                                <td class="table-text">
                                    <div><?php echo e($o->OrderCount); ?></div>
                                </td>
                                <td class="table-text">
                                    <div><?php echo money_format('%.2n', $o->TotalCost); ?></div>
                                </td>
                            </tr>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tbody>
                    </table>

                </div>
            </div>
        </div>

    </div>

    <div class="row">
        <div class="col-xs-6 col-md-4">
            <div class="panel panel-default">
                <div class="panel-heading">IPQ Session Totals: All Time</div>
                <div class="panel-body">

                    <table class="table table-striped task-table">
                        <!-- Table Headings -->
                        <thead>
                        <th>SmartPath Quizes (completed)</th>
                        <th>SmartPath Quizes (incomplete)</th>
                        </thead>
                        <!-- Table Body -->
                        <tbody>
                        <?php $__currentLoopData = $ipq_snapshot_stats; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $ipastat): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <tr>
                                <td class="table-text">
                                    <div><?php echo e($ipastat->total_completed); ?></div>
                                </td>
                                <td class="table-text">
                                    <div><?php echo e($ipastat->total_incomplete); ?></div>
                                </td>
                            </tr>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tbody>
                    </table>

                </div>
            </div>
        </div>

        <div class="col-xs-6 col-md-4">
            <div class="panel panel-default">
                <div class="panel-heading">IPQ Top 10 Users: (completed)</div>
                <div class="panel-body">

                    <table class="table table-striped task-table">
                        <!-- Table Headings -->
                        <thead>
                        <th>User ID</th>
                        <th># SmartPath</th>
                        </thead>
                        <!-- Table Body -->
                        <tbody>
                        <?php $__currentLoopData = $ipq_stats_completed; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $ipastat_c): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <tr>
                                <td class="table-text">
                                    <div><?php echo e($ipastat_c->uid); ?></div>
                                </td>
                                <td class="table-text">
                                    <div><?php echo e($ipastat_c->total_quizes_finished); ?></div>
                                </td>
                        </tr>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tbody>
                    </table>

                </div>
            </div>
        </div>

        <div class="col-xs-6 col-md-4">
            <div class="panel panel-default">
                <div class="panel-heading">IPQ Top 10 Users: (incomplete)</div>
                <div class="panel-body">

                    <table class="table table-striped task-table">
                        <!-- Table Headings -->
                        <thead>
                        <th>User ID</th>
                        <th># SmartPath</th>
                        </thead>
                        <!-- Table Body -->
                        <tbody>
                        <?php $__currentLoopData = $ipq_stats_incomplete; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $ipastat_inc): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <tr>
                            <td class="table-text">
                                <div><?php echo e($ipastat_inc->uid); ?></div>
                            </td>
                            <td class="table-text">
                                <div><?php echo e($ipastat_inc->total_quizes_unfinished); ?></div>
                            </td>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tr>
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12 col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">IPQ Sessions</div>
                <div class="panel-body">

                    <table class="table table-striped task-table">
                        <!-- Table Headings -->
                        <thead>
                        <th>User ID</th>
                        <th>Type</th>
                        <th>Quizes (finished)</th>
                        <th>Avg % Completed</th>
                        <th>Avg % Correct</th>
                        <th>First Quiz (finished)</th>
                        <th>Last Quiz (finished)</th>

                        <th>Quizes (unfinished)</th>
                        <th>Avg % Completed</th>
                        <th>Avg % Correct</th>
                        <th>First Quiz (unfinished)</th>
                        <th>Last Quiz (unfinished)</th>
                        </thead>
                        <!-- Table Body -->

                        <tbody>
                        <?php ($row = 1); ?>
                        <?php $__currentLoopData = $ipq_snapshot; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $ipq): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <tr class="accordion"  data-toggle="collapse" data-target="#collapse-<?php echo e($row); ?>">
                                <td class="table-text">
                                    <div><?php echo e($ipq->uid); ?></div>
                                </td>
                                <td class="table-text">
                                    <div><?php echo e($ipq->quiz_type); ?></div>
                                </td>
                                <td class="table-text">
                                    <div><?php echo e($ipq->total_quizes_finished); ?></div>
                                </td>
                                <!-- td class="table-text">
                                    <div>$<?php echo e($ipq->avg_time_finished); ?></div>
                                </td -->
                                <td class="table-text">
                                    <div><?php echo e($ipq->avg_perc_complete_finished); ?></div>
                                </td>
                                <td class="table-text">
                                    <div><?php echo e($ipq->avg_perc_correct_finished); ?></div>
                                </td>
                                <td class="table-text">
                                    <div><?php echo e($ipq->first_date_finished); ?></div>
                                </td>
                                <td class="table-text">
                                    <div><?php echo e($ipq->last_date_finished); ?></div>
                                </td>

                                <td class="table-text">
                                    <div><?php echo e($ipq->total_quizes_unfinished); ?></div>
                                </td>
                                <!-- td class="table-text">
                                    <div><?php echo e($ipq->avg_time_unfinished); ?></div>
                                </td -->
                                <td class="table-text">
                                    <div><?php echo e($ipq->avg_perc_complete_unfinished); ?></div>
                                </td>
                                <td class="table-text">
                                    <div><?php echo e($ipq->avg_perc_correct_unfinished); ?></div>
                                </td>
                                <td class="table-text">
                                    <div><?php echo e($ipq->first_date_unfinished); ?></div>
                                </td>
                                <td class="table-text">
                                    <div><?php echo e($ipq->last_date_unfinished); ?></div>
                                </td>
                            </tr>
                            <tr id="collapse-<?php echo e($row); ?>" class="success collapse">
                                <td></td>
                                <td colspan="11">
                                More details coming soon...
                                </td>
                            </tr>
                            <?php ($row++); ?>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tbody>
                    </table>

                </div>
            </div>
        </div>

    </div>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.dashboard', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>