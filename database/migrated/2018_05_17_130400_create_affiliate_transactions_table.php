<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAffiliateTransactionsTable extends Migration {
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up() {
    Schema::create('affiliate_transactions', function (Blueprint $table) {
      $table->increments('id');
      $table->dateTime('OriginalDate');
      $table->dateTime('FundedDate');
      $table->string('Affiliate');
      $table->string('Campaign');
      $table->string('EventType');
      $table->string('Amount');
      $table->string('Commission');
      $table->string('LCFees');
      $table->string('TotalCost');
      $table->string('OrderID');
      $table->string('MTID');
      $table->string('Status');
      $table->string('Reason');
      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down() {
    Schema::dropIfExists('affiliate_transactions');
  }
}
