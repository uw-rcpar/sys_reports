<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIPQSessionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ipq_sessions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('uid');
            $table->string('quiz_type');
            $table->integer('total_quizes_finished');
            $table->string('avg_time_finished');
            $table->string('avg_perc_complete_finished');
            $table->string('avg_perc_correct_finished');
            $table->dateTime('first_date_finished');
            $table->dateTime('last_date_finished');
            $table->integer('total_quizes_unfinished');
            $table->string('avg_time_unfinished');
            $table->string('avg_perc_complete_unfinished');
            $table->string('avg_perc_correct_unfinished');
            $table->dateTime('first_date_unfinished');
            $table->dateTime('last_date_unfinished');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ipq_sessions');
    }
}
