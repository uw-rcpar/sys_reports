<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHubspotsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rpt_hubspot', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();

            $table->unsignedInteger('hubspot_contact_id')->index();
            $table->string('email');
            $table->unsignedInteger('emails_delivered')->nullable();
            $table->unsignedInteger('emails_opened')->nullable();
            $table->unsignedInteger('emails_clicked')->nullable();
            $table->dateTime('first_email_sent_date')->nullable();
            $table->unsignedInteger('visits');
            $table->string('original_source_type');
            $table->unsignedInteger('form_submissions');
            $table->string('lead_status')->nullable();
            $table->date('cpa_exam_date')->nullable();          # do we need those three?
            $table->string('cpa_exam_date_month')->nullable();
            $table->string('cpa_exam_date_year')->nullable();
            $table->string('first_name')->nullable();
            $table->string('last_name')->nullable();
            $table->date('graduation_date')->nullable();        # do we need those three?
            $table->string('graduation_year')->nullable();
            $table->string('graduation_month')->nullable();
            $table->string('lifecycle_stage');
            $table->date('create_date');
            $table->date('became_customer_date')->nullable();
            $table->integer('score');
            $table->string('campus')->nullable();
            $table->string('prods_purchased');
            $table->unsignedInteger('user_id')->index();
            $table->string('ft_mark')->nullable();
            $table->string('asl_mark')->nullable();
            $table->string('fsl_mark')->nullable();
            $table->string('pal_mark')->nullable();
            $table->string('act_mark')->nullable();
            $table->date('ft_date')->nullable();
            $table->date('asl_date')->nullable();
            $table->date('pal_date')->nullable();
            $table->date('fsl_date')->nullable();
            $table->date('act_date')->nullable();
            $table->date('aud_exam_date')->nullable();
            $table->date('bec_exam_date')->nullable();
            $table->date('far_exam_date')->nullable();
            $table->date('reg_exam_date')->nullable();
            $table->unsignedInteger('order_id')->nullable()->index();
            $table->date('added_to_list_date');
            $table->string('company_name')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rpt_hubspot');
    }
}
