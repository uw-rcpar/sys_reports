<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNewOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // First, let's drop'the current orders table.
        Schema::dropIfExists('orders');

        Schema::create('rpt_orders', function (Blueprint $table) {
            $table->increments('id');
            $table->dateTime('created_date')->index();
            $table->dateTime('modified_date')->index();
            $table->unsignedInteger('uid')->index();
            $table->unsignedInteger('order_id')->index();
            $table->integer('order_total');
            $table->integer('shipping_total');
            $table->integer('tax_total');
            $table->string('status');
            $table->unsignedInteger('partner_id')->index();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rpt_orders');

        // Recreating the old orders table.
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('drupal_order_id')->index();
            $table->string('order_number');
            $table->string('type');
            $table->unsignedInteger('uid')->index();
            $table->string('status');
            $table->unsignedInteger('drupal_created')->index();
            $table->unsignedInteger('drupal_changed')->index();
            $table->unsignedInteger('placed');
            $table->timestamps();
        });
    }
}
