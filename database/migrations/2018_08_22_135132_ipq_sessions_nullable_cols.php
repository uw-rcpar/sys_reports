<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class IpqSessionsNullableCols extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('rpt_ipq_sessions', function (Blueprint $table) {
            $table->string('avg_time_unfinished')->nullable()->change();
            $table->string('avg_time_finished')->nullable()->change();
            $table->string('avg_perc_complete_unfinished')->nullable()->change();
            $table->string('avg_perc_complete_finished')->nullable()->change();
            $table->string('avg_perc_correct_unfinished')->nullable()->change();
            $table->string('avg_perc_correct_finished')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('rpt_ipq_sessions', function (Blueprint $table) {
            $table->string('avg_time_unfinished')->nullable(false)->change();
            $table->string('avg_time_finished')->nullable(false)->change();
            $table->string('avg_perc_complete_unfinished')->nullable(false)->change();
            $table->string('avg_perc_complete_finished')->nullable(false)->change();
            $table->string('avg_perc_correct_unfinished')->nullable(false)->change();
            $table->string('avg_perc_correct_finished')->nullable(false)->change();
        });
    }
}
