<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateRptUwOrderTransactionsTable extends Migration {
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up() {
    // DROP OLD TABLE - CRATE NEW ONE
    Schema::create('rpt_uw_order_transactions', function (Blueprint $table) {
      $table->bigIncrements('id');
      $table->integer('order_id')->index();
      $table->integer('user_id');
      $table->string('email_id');
      $table->date('created_date');
      $table->smallInteger('created_day');
      $table->smallInteger('created_month');
      $table->smallInteger('created_year');
      $table->date('grad_date')->nullable();
      $table->unsignedInteger('grad_month')->nullable();
      $table->unsignedInteger('grad_year')->nullable();
      $table->string('ubi_zip_code')->nullable();
      $table->smallInteger('ubi_state_id')->nullable();
      $table->string('ubi_state_name')->nullable();
      $table->string('ubi_state_abbrev')->nullable();
      $table->smallInteger('ubi_country_id')->nullable();
      $table->string('ubi_country_name')->nullable();
      $table->string('ubi_market')->nullable();
      $table->string('ubi_region')->nullable();
      $table->string('usi_zip_code')->nullable();
      $table->smallInteger('usi_state_id')->nullable();
      $table->string('usi_state_name')->nullable();
      $table->string('usi_state_abbrev')->nullable();
      $table->smallInteger('usi_country_id')->nullable();
      $table->string('usi_country_name')->nullable();
      $table->string('usi_market')->nullable();
      $table->string('usi_region')->nullable();
      $table->integer('partner_id')->nullable();
      $table->string('partner_title');
      $table->string('partner_zip_code')->nullable();
      $table->smallInteger('partner_state_id')->nullable();
      $table->string('partner_state_name')->nullable();
      $table->string('partner_state_abbrev')->nullable();
      $table->smallInteger('partner_country_id')->nullable();
      $table->string('partner_country_name')->nullable();
      $table->string('partner_market')->nullable();
      $table->string('partner_region')->nullable();
      $table->smallInteger('user_school_id')->nullable();
      $table->string('user_school_zip_code')->nullable();
      $table->smallInteger('user_school_state_id')->nullable();
      $table->string('user_school_state_name')->nullable();
      $table->string('user_school_state_abbrev')->nullable();
      $table->smallInteger('user_school_country_id')->nullable();
      $table->string('user_school_country_name')->nullable();
      $table->string('user_school_market')->nullable();
      $table->string('user_school_region')->nullable();
      $table->string('channel');
      $table->string('channel3');
      $table->string('lead_status_all');
      $table->string('payment_method');
      $table->string('card_type')->nullable();
      $table->string('transaction_type');
      $table->decimal('sub_total', 9, 2);
      $table->decimal('order_total', 9, 2);
      $table->decimal('promo_total', 9, 2);
      $table->decimal('partner_discount_total', 9, 2);
      $table->decimal('credit_amount', 9, 2);
      $table->decimal('shipping', 9, 2);
      $table->decimal('sales_tax', 9, 2);
      $table->decimal('credit_sales_tax', 9, 2);
      $table->integer('created_by');
      $table->timestamps();

    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down() {
    Schema::table('rpt_uw_order_transactions', function (Blueprint $table) {
      //
    });
  }
}
