<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExamScoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rpt_exam_scores', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('exam_scores_id')->index();
            $table->unsignedInteger('attempt');
            $table->unsignedInteger('uid')->index();
            $table->date('score_entered');
            $table->date('exam_taken');
            $table->unsignedInteger('exam_year');
            $table->unsignedInteger('score');
            $table->string('pass_fail');
            $table->string('section');
            $table->string('country');
            $table->string('state');
            $table->string('st');
            $table->string('market');
            $table->string('region');
            $table->unsignedInteger('us_college');
            $table->string('campus_state');
            $table->string('college_name');
            $table->string('exam_type');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rpt_exam_scores');
    }
}
