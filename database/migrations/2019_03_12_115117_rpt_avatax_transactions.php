<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RptAvataxTransactions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      // Set id as PK and autoincrement
      DB::statement("ALTER TABLE `rpt_avatax_transactions` MODIFY COLUMN `id` BIGINT UNSIGNED PRIMARY KEY AUTO_INCREMENT;");

      // Update to BIG int. Integer values already matched the maxint
      DB::statement("ALTER TABLE `rpt_avatax_transactions` MODIFY COLUMN `CompanyId` BIGINT UNSIGNED");
      DB::statement("ALTER TABLE `rpt_avatax_transactions` MODIFY COLUMN `DocumentId` BIGINT UNSIGNED");
      DB::statement("ALTER TABLE `rpt_avatax_transactions` MODIFY COLUMN `LineTaxDetailId` BIGINT UNSIGNED");

      // Remove composite index
      // https://stackoverflow.com/questions/44903533/laravel-migration-how-to-change-the-id-field-to-be-primary-and-auto-incremen
      // Laravel performs the updates based on id column, so this index is no longer required: remove this to speed up inserts
      Schema::table('rpt_avatax_transactions', function (Blueprint $table) {
          $table->dropIndex('code_item_jurisdiction');
      });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
