<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAvataxTransactionsTable extends Migration {
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up() {
    Schema::create('rpt_avatax_transactions', function (Blueprint $table) {

      $table->increments('id');
      $table->string('Country');
      $table->integer('CompanyId');
      $table->integer('DocumentId');
      $table->string('DocumentCode');
      $table->string('DocType');
      $table->string('DocumentDate');
      $table->string('TaxCalcDate');
      $table->string('ReportingDate');
      $table->string('PaymentDate');
      $table->string('ExchangeRateEffDate');
      $table->string('ExchangeRate');
      $table->string('CurrencyCode');
      $table->string('CustomerVendorCode');
      $table->string('EntityUseCodeDocumentHeader');
      $table->string('PurchaseOrderNo');
      $table->string('ReferenceCode');
      $table->string('SalespersonCode');
      $table->string('LocationCode');
      $table->string('LineNo');
      $table->string('ItemCode');
      $table->string('ItemCodeDescription');
      $table->string('TaxCode');
      $table->string('TaxCodeDescription');
      $table->string('RateType');
      $table->string('Quantity');
      $table->string('LineAmount');
      $table->string('DiscountAmount');
      $table->string('LineTaxableAmount');
      $table->string('Ref1');
      $table->string('Ref2');
      $table->string('RevenueAccount');
      $table->string('LineExemptAmount');
      $table->string('EntityUseCodeDocumentLine');
      $table->string('ExemptionNo');
      $table->string('IsSSTP');
      $table->string('IsTaxable');
      $table->string('OriginStreet');
      $table->string('OriginCity');
      $table->string('OriginStateProvince');
      $table->string('OriginCountry');
      $table->string('OriginPostalCode');
      $table->string('BusinessIdentificationId');
      $table->string('DestStreet');
      $table->string('DestCity');
      $table->string('DestStateProvince');
      $table->string('DestCountry');
      $table->string('DestPostalCode');
      $table->integer('LineTaxDetailId');
      $table->string('JurisdictionTypeDescription');
      $table->string('TaxTypeDescription');
      $table->string('StateAssignedNo');
      $table->string('Sourcing');
      $table->string('JurisdictionDescription');
      $table->string('InState');
      $table->string('StateProvince');
      $table->string('JurisdictionTypeTaxableAmount');
      $table->string('JurisdictionTypeExemptAmount');
      $table->string('ExemptReason');
      $table->string('NonTaxableAmount');
      $table->string('NonTaxableReason');
      $table->string('NonTaxableRuleDescription');
      $table->string('OverrideAmount');
      $table->string('TaxAmount');
      $table->string('TaxCalculated');
      $table->string('TaxVariance');
      $table->string('Rate');
      $table->string('ECMSCertId');
      $table->timestamps();

    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down() {
    Schema::dropIfExists('rpt_avatax_transactions');
  }
}
