<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class NpsDatetimeToDate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('rpt_nps', function (Blueprint $table) {
            $table->date('create_date')->change();
            $table->smallInteger('score')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('rpt_nps', function (Blueprint $table) {
            $table->dateTime('create_date')->change();
            $table->tinyInteger('score')->nullable(false)->change();
        });
    }
}
