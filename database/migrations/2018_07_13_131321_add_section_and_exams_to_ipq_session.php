<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSectionAndExamsToIpqSession extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('rpt_ipq_sessions', function (Blueprint $table) {
        $table->string('section');
        $table->unsignedInteger('total_exams');
        $table->date('last_exam_date');
        $table->unsignedInteger('question_count');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('rpt_ipq_sessions', function(Blueprint $table) {
        $table->dropColumn('section');
        $table->dropColumn('total_exams');
        $table->dropColumn('last_exam_date');
        $table->dropColumn('question_count');
      });
    }
}
