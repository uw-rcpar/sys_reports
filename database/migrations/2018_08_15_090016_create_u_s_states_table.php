<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Library\Model\USState;

class CreateUSStatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('us_states', function (Blueprint $table) {
            $table->increments('id');
            $table->string('abbreviated_name')->index();
            $table->string('full_name')->index();
            $table->timestamps();
        });

        $states_list = [
            'AK' => 'ALASKA',
            'AL' => 'ALABAMA',
            'AR' => 'ARKANSAS',
            'AZ' => 'ARIZONA',
            'CA' => 'CALIFORNIA',
            'CO' => 'COLORADO',
            'CT' => 'CONNECTICUT',
            'DE' => 'DELAWARE',
            'FL' => 'FLORIDA',
            'GA' => 'GEORGIA',
            'HI' => 'HAWAII',
            'IA' => 'IOWA',
            'ID' => 'IDAHO',
            'IL' => 'ILLINOIS',
            'IN' => 'INDIANA',
            'KS' => 'KANSAS',
            'KY' => 'KENTUCKY',
            'LA' => 'LOUISIANA',
            'MA' => 'MASSACHUSETTS',
            'MD' => 'MARYLAND',
            'ME' => 'MAINE',
            'MI' => 'MICHIGAN',
            'MN' => 'MINNESOTA',
            'MO' => 'MISSOURI',
            'MS' => 'MISSISSIPPI',
            'MT' => 'MONTANA',
            'NC' => 'NORTH CAROLINA',
            'ND' => 'NORTH DAKOTA',
            'NE' => 'NEBRASKA',
            'NH' => 'NEW HAMPSHIRE',
            'NJ' => 'NEW JERSEY',
            'NM' => 'NEW MEXICO',
            'NV' => 'NEVADA',
            'NY' => 'NEW YORK',
            'OH' => 'OHIO',
            'OK' => 'OKLAHOMA',
            'OR' => 'OREGON',
            'PA' => 'PENNSYLVANIA',
            'RI' => 'RHODE ISLAND',
            'SC' => 'SOUTH CAROLINA',
            'SD' => 'SOUTH DAKOTA',
            'TN' => 'TENNESSEE',
            'TX' => 'TEXAS',
            'UT' => 'UTAH',
            'VA' => 'VIRGINIA',
            'VT' => 'VERMONT',
            'WA' => 'WASHINGTON',
            'WI' => 'WISCONSIN',
            'WV' => 'WEST VIRGINIA',
            'WY' => 'WYOMING',
        ];
        foreach ($states_list as $state_short => $state_long) {
            $state = new USState;
            $state->abbreviated_name = $state_short;
            $state->full_name = $state_long;
            $state->save();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('us_states');
    }
}
