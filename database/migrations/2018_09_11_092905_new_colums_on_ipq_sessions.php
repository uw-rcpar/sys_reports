<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class NewColumsOnIpqSessions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('rpt_ipq_sessions', function (Blueprint $table) {
          $table->unsignedInteger('entitlement_id')->nullable();
          $table->unsignedInteger('total_question_points')->nullable();
          $table->unsignedInteger('total_obtained_points')->nullable();
          $table->unsignedInteger('mcq_question_score')->nullable();
          $table->unsignedInteger('mcq_question_count')->nullable();
          $table->unsignedInteger('mcq_distinct_question_count')->nullable();
          $table->unsignedInteger('drs_question_score')->nullable();
          $table->unsignedInteger('drs_question_count')->nullable();
          $table->unsignedInteger('drs_distinct_question_count')->nullable();
          $table->unsignedInteger('research_question_score')->nullable();
          $table->unsignedInteger('research_question_count')->nullable();
          $table->unsignedInteger('research_distinct_question_count')->nullable();
          $table->unsignedInteger('tbs_form_question_score')->nullable();
          $table->unsignedInteger('tbs_form_question_count')->nullable();
          $table->unsignedInteger('tbs_form_distinct_question_count')->nullable();
          $table->unsignedInteger('tbs_journal_question_score')->nullable();
          $table->unsignedInteger('tbs_journal_question_count')->nullable();
          $table->unsignedInteger('tbs_journal_distinct_question_count')->nullable();
          $table->unsignedInteger('regular_tbs_question_score')->nullable();
          $table->unsignedInteger('regular_tbs_question_count')->nullable();
          $table->unsignedInteger('regular_tbs_distinct_question_count')->nullable();
          $table->unsignedInteger('wc_question_score')->nullable();
          $table->unsignedInteger('wc_question_count')->nullable();
          $table->unsignedInteger('wc_distinct_question_count')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('rpt_ipq_sessions', function (Blueprint $table) {
            $table->dropColumn('entitlement_id');
            $table->dropColumn('total_question_points');
            $table->dropColumn('total_obtained_points');
            $table->dropColumn('mcq_question_score');
            $table->dropColumn('mcq_question_count');
            $table->dropColumn('mcq_distinct_question_count');
            $table->dropColumn('drs_question_score');
            $table->dropColumn('drs_question_count');
            $table->dropColumn('drs_distinct_question_count');
            $table->dropColumn('research_question_score');
            $table->dropColumn('research_question_count');
            $table->dropColumn('research_distinct_question_count');
            $table->dropColumn('tbs_form_question_score');
            $table->dropColumn('tbs_form_question_count');
            $table->dropColumn('tbs_form_distinct_question_count');
            $table->dropColumn('tbs_journal_question_score');
            $table->dropColumn('tbs_journal_question_count');
            $table->dropColumn('tbs_journal_distinct_question_count');
            $table->dropColumn('regular_tbs_question_score');
            $table->dropColumn('regular_tbs_question_count');
            $table->dropColumn('regular_tbs_distinct_question_count');
            $table->dropColumn('wc_question_score');
            $table->dropColumn('wc_question_count');
            $table->dropColumn('wc_distinct_question_count');
        });
    }
}
