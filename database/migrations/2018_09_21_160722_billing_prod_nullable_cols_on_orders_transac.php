<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class BillingProdNullableColsOnOrdersTransac extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('rpt_order_transactions', function (Blueprint $table) {
            $table->string('country')->nullable()->change();
            $table->string('state')->nullable()->change();
            $table->string('st')->nullable()->change();
            $table->string('market')->nullable()->change();
            $table->string('region')->nullable()->change();
            $table->string('zip')->nullable()->change();
            $table->string('ship_state')->nullable()->change();
            $table->string('ship_zip')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('rpt_order_transactions', function (Blueprint $table) {
            $table->string('country')->nullable(false)->change();
            $table->string('state')->nullable(false)->change();
            $table->string('st')->nullable(false)->change();
            $table->string('market')->nullable(false)->change();
            $table->string('region')->nullable(false)->change();
            $table->string('zip')->nullable(false)->change();
            $table->string('ship_state')->nullable(false)->change();
            $table->string('ship_zip')->nullable(false)->change();
        });
    }
}
