<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePartnerProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Let's drop the old table first.
        Schema::dropIfExists('partners');

        Schema::create('rpt_partner_profile', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('nid')->index();
            $table->integer('account_id')->index();
            $table->string('account_name');
            $table->string('partner_name');
            $table->string('abbreviated_name');
            $table->string('intacct_cid');
            $table->string('partner_type');
            $table->string('billing_type');
            $table->string('lead_status');
            $table->integer('account_executive_uid');
            $table->longText('description');
            $table->longText('partner_terms');
            $table->boolean('act_access');
            $table->boolean('monitoring_access');
            $table->integer('monitoring_uid');
            $table->integer('archive_pid');
            $table->string('fixed_expiration');
            $table->boolean('course_section_full');
            $table->boolean('course_section_aud');
            $table->boolean('course_section_bec');
            $table->boolean('course_section_far');
            $table->boolean('course_section_reg');
            $table->decimal('general_discount', 25, 4);
            $table->decimal('aud_discount', 25, 4);
            $table->decimal('bec_discount', 25, 4);
            $table->decimal('far_discount', 25, 4);
            $table->decimal('reg_discount', 25, 4);
            $table->boolean('bundled_full', 25, 4);
            $table->boolean('bundled_6m_ext');
            $table->boolean('bundled_offline_full');
            $table->boolean('bundled_mobile_download');
            $table->boolean('bundled_audio_full');
            $table->boolean('bundled_flashcard_full');
            $table->boolean('bundled_cram_full');
            $table->boolean('bundled_cram_part_cert');
            $table->boolean('bundled_flashcard_part');
            $table->boolean('bundled_audio_part');
            $table->decimal('bundled_price', 25, 4);
            $table->boolean('email_validation');
            $table->boolean('exempt_sales_tax');
            $table->boolean('flatrate_shipping');
            $table->decimal('shipping_rate', 25, 4);
            $table->decimal('partial_shipping_rate', 25, 4);
            $table->boolean('international');
            $table->boolean('show_price');
            $table->boolean('show_retail_price');
            $table->string('shipping_flow');
            $table->string('status');
            $table->integer('author_uid');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rpt_partner_profile');
    }
}
