<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UserProfilesNullableCols extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('rpt_user_profile', function (Blueprint $table) {
            $table->integer('phone')->nullable()->change();
            $table->text('courses_taught')->nullable()->change();
            $table->string('college_name')->nullable()->change();
            $table->date('first_exam_sit')->nullable()->change();
            $table->string('user_profile_country')->nullable()->change();
            $table->string('user_profile_state')->nullable()->change();
            $table->string('campus')->nullable()->change();
            $table->string('sync_stage')->nullable()->change();
            $table->string('legacy_id')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('rpt_user_profile', function (Blueprint $table) {
            $table->integer('phone')->nullable(false)->change();
            $table->text('courses_taught')->nullable(false)->change();
            $table->string('college_name')->nullable(false)->change();
            $table->date('first_exam_sit')->nullable(false)->change();
            $table->string('user_profile_country')->nullable(false)->change();
            $table->string('user_profile_state')->nullable(false)->change();
            $table->string('campus')->nullable(false)->change();
            $table->string('sync_stage')->nullable(false)->change();
            $table->string('legacy_id')->nullable(false)->change();
        });
    }
}
