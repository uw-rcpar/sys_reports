<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddingEntitlementIdAndOthersToLectureStats extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('rpt_lecture_statistics', function (Blueprint $table) {
            $table->unsignedInteger('entitlement_id')->nullable();
            $table->unsignedInteger('video_total_duration');
            $table->unsignedInteger('watched_time_total');
            $table->unsignedInteger('watched_time_complete');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('rpt_lecture_statistics', function (Blueprint $table) {
            $table->dropColumn('entitlement_id');
            $table->dropColumn('video_total_duration');
            $table->dropColumn('watched_time_total');
            $table->dropColumn('watched_time_complete');
        });
    }
}
