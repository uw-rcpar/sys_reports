<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRptUwLineItemRevenueTable extends Migration {

  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up() {
    Schema::create('rpt_uw_line_item_revenue', function (Blueprint $table) {
      $table->bigIncrements('id');
      $table->integer('item_id');
      $table->integer('order_id')->index();
      $table->integer('user_id');
      $table->smallInteger('package_id');
      $table->string('package_name',100);
      $table->integer('course_id');
      $table->integer('duration');
      $table->boolean('is_renewal');
      $table->boolean('is_sim');
      $table->tinyInteger('form_id');
      $table->boolean('is_trial');
      $table->tinyInteger('qbank_id');
      $table->string('qbank_name');
      $table->tinyInteger('top_level_product_id');
      $table->string('top_level_product_name');
      $table->tinyInteger('top_level_prodct_group_id');
      $table->string('top_level_prodct_group_name');
      $table->decimal('sub_total',9,2);
      $table->decimal('promo_total',9,2);
      $table->decimal('partner_discount_total',9,2);
      $table->decimal('credit_amount',9,2);
      $table->integer('quantity');
      $table->decimal('sales_tax',9,2);
      $table->decimal('credit_sales_tax',9,2);
      $table->decimal('order_total',9,2);
      $table->integer('free_trial')->nullable();
      $table->integer('ft_time_diff')->nullable();
      $table->string('ft_time_group', 20)->nullable();
      $table->string('promotion_type', 20)->nullable();
      $table->string('promotion', 50)->nullable();
      $table->string('promo_description',200)->nullable();
      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down() {
    Schema::table('rpt_uw_line_item_revenue', function (Blueprint $table) {
      //
    });
  }
}
