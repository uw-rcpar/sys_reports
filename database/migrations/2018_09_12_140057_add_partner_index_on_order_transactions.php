<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPartnerIndexOnOrderTransactions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('rpt_order_transactions', function (Blueprint $table) {
            $table->index('partner_nid');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('rpt_order_transactions', function (Blueprint $table) {
            $table->dropIndex('rpt_order_transactions_partner_nid_index');
        });
    }
}
