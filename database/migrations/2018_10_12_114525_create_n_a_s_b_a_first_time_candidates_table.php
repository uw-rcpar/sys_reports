<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNASBAFirstTimeCandidatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rpt_nasba_firsttime_candidates', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();

            $table->integer('year');
            $table->string('state_abbrev');
            $table->string('state_univ');
            $table->integer('cohort');
            $table->integer('juris_cohort');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rpt_nasba_firsttime_candidates');
    }
}
