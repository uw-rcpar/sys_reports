<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUserSchoolNameToUwOrderTransactions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('rpt_uw_order_transactions', function (Blueprint $table) {
          // Rename recently created school name - adapt to uploaded csv structure
          $table->renameColumn('school_name', 'user_school_name');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('rpt_uw_order_transactions', function (Blueprint $table) {
          // Revert up()
          $table->renameColumn('user_school_name' , 'school_name');
        });
    }
}
