<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddingBundlesColToOrdersTransac extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('rpt_order_transactions', function (Blueprint $table) {
            $table->string('bundles')->nullable()->after('package');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('rpt_order_transactions', function (Blueprint $table) {
            $table->dropColumn('bundles');
        });
    }
}
