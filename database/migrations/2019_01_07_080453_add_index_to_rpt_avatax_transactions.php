<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIndexToRptAvataxTransactions extends Migration {

  private $indexColumns = ['DocumentCode', 'ItemCode', 'JurisdictionTypeDescription'];

  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up() {
    // Adds unique index
    Schema::table('rpt_avatax_transactions', function (Blueprint $table) {
      $table->unique($this->indexColumns, 'code_item_jurisdiction');
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down() {
    // Drop Index created on up() method
    Schema::table('rpt_avatax_transactions', function (Blueprint $table) {
      $table->dropIndex('code_item_jurisdiction');
    });
  }
}
