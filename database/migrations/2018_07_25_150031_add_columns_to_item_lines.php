<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsToItemLines extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Removing unused table.
        Schema::dropIfExists('order_lines');

        Schema::table('rpt_line_item_revenue', function (Blueprint $table) {
            $table->dateTime('created_date')->index();
            $table->dateTime('modified_date')->index();
            $table->string('sku');
            $table->string('name');
            $table->integer('taxable');
            $table->integer('taxed');
            $table->integer('total');
            $table->integer('shipping_part');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('rpt_line_item_revenue', function (Blueprint $table) {
            $table->dropColumn('created_date');
            $table->dropColumn('modified_date');
            $table->dropColumn('sku');
            $table->dropColumn('name');
            $table->dropColumn('taxable');
            $table->dropColumn('taxed');
            $table->dropColumn('total');
            $table->dropColumn('shipping_part');
        });

        // Recreate table removed on the migration.
        Schema::create('order_lines', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('drupal_order_id')->index();
            $table->string('type');
            $table->unsignedInteger('drupal_line_item_id');
            $table->string('label');
            $table->string('product_title');
            $table->string('product_type');
            $table->unsignedInteger('quantity');
            $table->unsignedInteger('drupal_created')->index();
            $table->unsignedInteger('drupal_changed')->index();
            $table->string('tax_rate');
            $table->string('currency_code');
            $table->float('taxable');
            $table->float('taxed');
            $table->float('total');
            $table->float('total_no_tax');
            $table->float('shipping_part');
            $table->timestamps();
        });

    }
}
