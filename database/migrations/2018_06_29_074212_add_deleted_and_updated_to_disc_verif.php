<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDeletedAndUpdatedToDiscVerif extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('rpt_discount_verifications', function (Blueprint $table) {
            $table->unsignedInteger('deleted');
            $table->dateTime('drupal_updated_at');
        });
        DB::table('rpt_discount_verifications')->update(array('deleted' => 0));
        DB::table('rpt_discount_verifications')->update(
            array('drupal_updated_at' => DB::raw('drupal_created_at'))
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('rpt_discount_verifications', function (Blueprint $table) {
            $table->dropColumn('deleted');
            $table->dropColumn('drupal_updated_at');
        });
    }
}
