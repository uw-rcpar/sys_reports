<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaidTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rpt_paid_transactions', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();

            // TODO: Shoudn't we add the Drupal transaction id here?
            $table->unsignedInteger('user_id')->index();
            $table->string('mail');
            $table->unsignedInteger('order_id')->index();
            $table->date('create_date')->index();
            $table->unsignedInteger('create_year');
            $table->unsignedInteger('create_month');
            $table->unsignedInteger('create_day');
            $table->string('payment_method');
            $table->string('channel');
            $table->string('channel3');
            $table->string('promotion_type');
            $table->string('promotion');
            $table->longText('promo_description');
            $table->string('free_trial');
            $table->bigInteger('ft_time_diff');
            $table->string('ft_time_group');
            $table->string('country');
            $table->string('state');
            $table->string('st');
            $table->string('market');
            $table->string('region');
            $table->string('zip');
            $table->integer('partner_nid');
            $table->string('partner_title');
            $table->string('intacct');
            $table->unsignedInteger('college_state_tid');
            $table->string('college_name');
            $table->string('college_state_abbrv');
            $table->string('college_state');
            $table->decimal('total_amount', 15, 4);

            $table->index(['user_id', 'order_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rpt_paid_transactions');
    }
}
