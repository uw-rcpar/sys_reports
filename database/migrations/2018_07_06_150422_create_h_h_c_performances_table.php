<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHHCPerformancesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rpt_hhc_performance', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('uid')->index();
            $table->string('user');
            $table->string('package');
            $table->string('user_group');
            $table->unsignedInteger('cid');
            $table->string('moderator');
            $table->date('date');
            $table->date('answer_date');
            $table->string('thread');
            $table->string('year');
            $table->string('month');
            $table->string('day');
            $table->string('timediff');
            $table->string('title');
            $table->string('section_chapter');
            $table->string('section');
            $table->longText('moderator_question');
            $table->longText('comment_body_value');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rpt_hhc_performance');
    }
}
