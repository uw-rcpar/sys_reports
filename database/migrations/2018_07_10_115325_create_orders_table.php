<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('drupal_order_id')->index();
            $table->string('order_number');
            $table->string('type');
            $table->unsignedInteger('uid')->index();
            $table->string('status');
            $table->unsignedInteger('drupal_created')->index();
            $table->unsignedInteger('drupal_changed')->index();
            $table->unsignedInteger('placed');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
