<?php

  use Illuminate\Support\Facades\Schema;
  use Illuminate\Database\Schema\Blueprint;
  use Illuminate\Database\Migrations\Migration;

  class CreatePartnerEmailsTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     *
     */
    public function up() {
      Schema::create('rpt_partner_emails', function (Blueprint $table) {
        $table->increments('id');
        $table->unsignedInteger('partner_email_id')->index();
        $table->unsignedInteger('partner_nid');
        $table->integer('gid')->index();
        $table->string('email');
        $table->string('status')->nullable();
        $table->tinyInteger('count');
        $table->string('campus_state')->nullable();
        $table->date('create_date');
        $table->integer('create_year');
        $table->integer('create_month');
        $table->integer('create_day');
        $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
      Schema::dropIfExists('rpt_partner_emails');
    }
  }
