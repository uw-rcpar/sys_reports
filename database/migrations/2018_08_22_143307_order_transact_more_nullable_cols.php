<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class OrderTransactMoreNullableCols extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('rpt_order_transactions', function (Blueprint $table) {
            $table->string('partner_title')->nullable()->change();
            $table->string('intacct')->nullable()->change();

            $table->string('college_state_tid')->nullable()->change();
            $table->string('free_trial')->nullable()->change();
            $table->bigInteger('ft_time_diff')->nullable()->change();
            $table->string('ft_time_group')->nullable()->change();
            $table->decimal('promo_total')->nullable()->change();
            $table->decimal('partner_discount_total')->nullable()->change();
            $table->decimal('refund_amount', 15, 2)->nullable()->change();
            $table->integer('account_id')->nullable()->change();
            $table->string('account_name')->nullable()->change();

            $table->decimal('shipping', 15, 2)->nullable()->change();
            $table->decimal('taxes', 15, 2)->nullable()->change();
            $table->string('college_state')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('rpt_order_transactions', function (Blueprint $table) {
            $table->string('partner_title')->nullable(false)->change();
            $table->string('intacct')->nullable(false)->change();

            $table->string('college_state_tid')->nullable(false)->change();
            $table->string('free_trial')->nullable(false)->change();
            $table->bigInteger('ft_time_diff')->nullable(false)->change();
            $table->string('ft_time_group')->nullable(false)->change();
            $table->decimal('promo_total')->nullable(false)->change();
            $table->decimal('partner_discount_total')->nullable(false)->change();
            $table->decimal('refund_amount', 15, 2)->nullable(false)->change();
            $table->integer('account_id')->nullable(false)->change();
            $table->string('account_name')->nullable(false)->change();

            $table->decimal('shipping', 15, 2)->nullable()->change();
            $table->decimal('taxes', 15, 2)->nullable()->change();
            $table->string('college_state')->nullable()->change();
        });
    }
}
