<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenamingProfileFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('rpt_user_profile', function (Blueprint $table) {
            $table->renameColumn('customer_address_country', 'ship_country');
            $table->renameColumn('customer_address_administrative_area', 'ship_state');
            $table->renameColumn('locality', 'ship_city');
            $table->renameColumn('dependent_locality', 'ship_dependent_locality');
            $table->renameColumn('thoroughfare', 'ship_thoroughfare');
            $table->renameColumn('premise', 'ship_premise');
            $table->renameColumn('postal_code', 'ship_zip');

            $table->renameColumn('when_plan_to_first_sit_for_exam', 'first_exam_sit');

            // We also need to remove this columns:
            $table->dropColumn('sub_administrative_area');
            $table->dropColumn('sub_premise');
            $table->dropColumn('organisation_name');

            // And add this others:
            $table->string('billing_country');
            $table->string('billing_state');
            $table->string('billing_city');
            $table->string('billing_dependent_locality');
            $table->string('billing_thoroughfare');
            $table->string('billing_premise');
            $table->string('billing_zip');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('rpt_user_profile', function (Blueprint $table) {
            $table->renameColumn('ship_country', 'customer_address_country');
            $table->renameColumn('ship_state', 'customer_address_administrative_area');
            $table->renameColumn('ship_city', 'locality');
            $table->renameColumn('ship_dependent_locality', 'dependent_locality');
            $table->renameColumn('ship_thoroughfare', 'thoroughfare');
            $table->renameColumn('ship_premise', 'premise');
            $table->renameColumn('ship_zip', 'postal_code');
            $table->renameColumn('first_exam_sit', 'when_plan_to_first_sit_for_exam');

            $table->string('sub_administrative_area');
            $table->string('sub_premise');
            $table->string('organisation_name');

            $table->dropColumn('billing_country');
            $table->dropColumn('billing_state');
            $table->dropColumn('billing_city');
            $table->dropColumn('billing_dependent_locality');
            $table->dropColumn('billing_thoroughfare');
            $table->dropColumn('billing_premise');
            $table->dropColumn('billing_zip');
        });
    }
}
