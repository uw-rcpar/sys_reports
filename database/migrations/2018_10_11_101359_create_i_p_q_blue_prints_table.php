<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIPQBluePrintsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rpt_ipq_blueprints', function (Blueprint $table) {
          $table->increments('id');
          $table->timestamps();
          $table->string('effective_date');
          $table->string('computed_key')->index();
          $table->string('section');
          $table->string('area_id');
          $table->string('area_title');
          $table->string('group_id');
          $table->string('group_title');
          $table->string('topic_id');
          $table->string('topic_title');
          $table->string('task_id');
          $table->string('task_skill');
          $table->string('representative_task');
          $table->string('changed')->default('no');
          $table->string('change_description');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rpt_ipq_blueprints');
    }
}
