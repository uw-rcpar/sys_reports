<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;


class CreateAvataxView extends Migration {
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up() {
    //
    DB::statement($this->createView());

  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down() {
    DB::statement($this->dropView());
  }


  private function createView() {
    return "
      CREATE  VIEW avatax
      AS SELECT
         substr(rpt_avatax_transactions.DocumentCode,4) AS order_id,
         rpt_avatax_transactions.ItemCode AS sku,if((rpt_avatax_transactions.LineTaxableAmount > '0.00'),rpt_avatax_transactions.LineTaxableAmount,
         rpt_avatax_transactions.NonTaxableAmount) AS taxable,round(sum(rpt_avatax_transactions.TaxAmount),2) AS tax,round((if((rpt_avatax_transactions.LineTaxableAmount > '0.00'),rpt_avatax_transactions.LineTaxableAmount,
         rpt_avatax_transactions.NonTaxableAmount) + round(sum(rpt_avatax_transactions.TaxAmount),2)),2) AS total
      FROM rpt_avatax_transactions group by rpt_avatax_transactions.DocumentCode,rpt_avatax_transactions.ItemCode";
  }

  private function dropView() {
    return "DROP VIEW IF EXISTS avatax";
  }

}
