<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReferralsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rpt_referral', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('uid')->index();
            $table->unsignedInteger('referring_uid')->index();
            $table->unsignedInteger('order_id')->index();
            $table->dateTime('create_date');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rpt_referral');
    }
}
