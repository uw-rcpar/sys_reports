<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAvataxSummaryTable extends Migration {
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up() {
    Schema::create('avatax_summary', function (Blueprint $table) {

      //cols
      $table->increments('id')->index();
      $table->integer('order_id');
      $table->string('sku', 40);
      $table->decimal('taxable', 9, 2);
      $table->decimal('tax', 9, 2);
      $table->decimal('total', 9, 2);

      // Indexes:
      $table->unique(['order_id', 'sku'], 'order_sku');
    });

    DB::statement("
      INSERT IGNORE into avatax_summary (order_id, sku, taxable,tax, total)
    
      SELECT
        substr(rpt_avatax_transactions.DocumentCode,4) AS order_id,
        rpt_avatax_transactions.ItemCode AS sku,
        if((rpt_avatax_transactions.LineTaxableAmount > '0.00'),rpt_avatax_transactions.LineTaxableAmount, rpt_avatax_transactions.NonTaxableAmount) AS taxable,
        round(sum(rpt_avatax_transactions.TaxAmount),2) AS tax,
      
      round((if((rpt_avatax_transactions.LineTaxableAmount > '0.00'),rpt_avatax_transactions.LineTaxableAmount,
        rpt_avatax_transactions.NonTaxableAmount) + round(sum(rpt_avatax_transactions.TaxAmount),2)),2) AS total
      FROM rpt_avatax_transactions 
      GROUP BY rpt_avatax_transactions.DocumentCode,rpt_avatax_transactions.ItemCode
      ORDER BY rpt_avatax_transactions.id");


  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down() {
    Schema::drop('avatax_summary');
  }
}
