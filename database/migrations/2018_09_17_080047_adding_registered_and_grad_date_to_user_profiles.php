<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddingRegisteredAndGradDateToUserProfiles extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('rpt_user_profile', function (Blueprint $table) {
            $table->date('grad_date')->nullable()->after('did_graduate_college');
            $table->date('registration_date')->nullable()->after('legacy_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('rpt_user_profile', function (Blueprint $table) {
            $table->dropColumn('grad_date');
            $table->dropColumn('registration_date');
        });
    }
}
