<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddReferredDestinationToRptReferral extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('rpt_referral', function (Blueprint $table) {
        $table->string('referred_destination')->nullable();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('rpt_referral', function (Blueprint $table) {
        $table->dropColumn('referred_destination');
      });
    }
}
