<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_transactions', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('transaction_id')->index();
            $table->unsignedInteger('uid')->index();
            $table->unsignedInteger('order_id')->index();
            $table->string('payment_method')->index();
            $table->string('instance_id');
            $table->string('remote_id')->index();
            $table->string('message');
            $table->integer('amount');
            $table->string('currency_code');
            $table->string('status');
            $table->string('remote_status');
            $table->longText('payload');
            $table->string('drupal_created');
            $table->string('drupal_changed');
            $table->longText('data');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_transactions');
    }
}
