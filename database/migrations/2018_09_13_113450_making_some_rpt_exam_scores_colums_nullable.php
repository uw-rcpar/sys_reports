<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MakingSomeRptExamScoresColumsNullable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('rpt_exam_scores', function (Blueprint $table) {
            $table->string('campus_state')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('rpt_exam_scores', function (Blueprint $table) {
            $table->string('campus_state')->nullable(false)->change();
        });
    }
}
