<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddingIndexToRptAvataxTransactions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('rpt_avatax_transactions', function (Blueprint $table) {
            $table->index(['LineTaxDetailId', 'DocumentId']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('rpt_avatax_transactions', function (Blueprint $table) {
            $table->dropIndex(['LineTaxDetailId', 'DocumentId']);
        });
    }
}
