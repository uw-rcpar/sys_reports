<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddingCourseTypeToLecStats extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('rpt_lecture_statistics', function (Blueprint $table) {
            $table->string('course_type')->after('section');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('rpt_lecture_statistics', function (Blueprint $table) {
            $table->dropColumn('course_type');
        });
    }
}
