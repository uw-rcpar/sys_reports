<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class OrderTransactYetMoreNullableCols extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('rpt_order_transactions', function (Blueprint $table) {
            $table->date('grad_date')->nullable()->change();
            $table->integer('grad_year')->nullable()->change();
            $table->integer('grad_month')->nullable()->change();

            $table->integer('college_state')->nullable()->change();
            $table->integer('zip')->nullable()->change();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('rpt_order_transactions', function (Blueprint $table) {
            $table->date('grad_date')->nullable(false)->change();
            $table->integer('grad_year')->nullable(false)->change();
            $table->integer('grad_month')->nullable(false)->change();

            $table->integer('college_state')->nullable(false)->change();
            $table->integer('zip')->nullable(false)->change();
        });
    }
}
