<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenamingAvgTotalDurationToAvgVideoTotalDuration extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('rpt_lecture_statistics', function (Blueprint $table) {
            $table->renameColumn('avg_total_duration', 'avg_video_total_duration');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('rpt_lecture_statistics', function (Blueprint $table) {
          $table->renameColumn('avg_video_total_duration', 'avg_total_duration');
        });
    }
}
