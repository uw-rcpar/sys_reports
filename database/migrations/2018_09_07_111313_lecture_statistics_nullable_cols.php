<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class LectureStatisticsNullableCols extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('rpt_lecture_statistics', function (Blueprint $table) {
            $table->float('avg_total_duration')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('rpt_lecture_statistics', function (Blueprint $table) {
          $table->float('avg_total_duration')->nullable(false)->change();
        });
    }
}
