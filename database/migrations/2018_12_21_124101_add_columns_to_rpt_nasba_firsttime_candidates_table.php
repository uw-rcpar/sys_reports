<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsToRptNasbaFirsttimeCandidatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('rpt_nasba_firsttime_candidates', function (Blueprint $table) {
          $table->string('market');
          $table->string('region');
          $table->string('univ_sections');
          $table->string('univ_ft_sections');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('rpt_nasba_firsttime_candidates', function (Blueprint $table) {
          $table->dropColumn('market');
          $table->dropColumn('region');
          $table->dropColumn('univ_sections');
          $table->dropColumn('univ_ft_sections');
        });
    }
}
