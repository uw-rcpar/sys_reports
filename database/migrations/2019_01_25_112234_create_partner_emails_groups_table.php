<?php

  use Illuminate\Support\Facades\Schema;
  use Illuminate\Database\Schema\Blueprint;
  use Illuminate\Database\Migrations\Migration;

  class CreatePartnerEmailsGroupsTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     *
     */
    public function up() {
      Schema::create('rpt_partner_emails_groups', function (Blueprint $table) {
        $table->increments('id');
        $table->unsignedInteger('group_id');
        $table->unsignedInteger('partner_nid');
        $table->string('group_name');
        $table->dateTime('start_date');
        $table->dateTime('end_date');
        $table->dateTime('create_date');
        $table->integer('override_expiration');
        $table->string('class_name')->nullable();
        $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
      Schema::dropIfExists('rpt_partner_emails_groups');
    }
  }
