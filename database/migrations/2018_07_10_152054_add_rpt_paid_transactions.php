<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRptPaidTransactions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('rpt_paid_transactions', function(Blueprint $table) {
        $table->string('ship_state')->after('zip');
        $table->string('ship_zip')->after('ship_state');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('rpt_paid_transactions', function(Blueprint $table) {
        $table->dropColumn('ship_state');
        $table->dropColumn('ship_zip');
      });
    }
}
