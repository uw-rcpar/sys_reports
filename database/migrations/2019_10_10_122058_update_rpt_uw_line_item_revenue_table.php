<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateRptUwLineItemRevenueTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('rpt_uw_line_item_revenue', function (Blueprint $table) {
          $table->string('course_name',100);
          $table->index(['order_id','item_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('rpt_uw_line_item_revenue', function (Blueprint $table) {
          $table->removeColumn('course_name');
        });
    }
}
