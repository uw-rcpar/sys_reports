<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameRptPaidTransactions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Removing unused table.
        Schema::dropIfExists('order_transactions');

        Schema::rename('rpt_paid_transactions', 'rpt_order_transactions');

        Schema::table('rpt_order_transactions', function (Blueprint $table) {
            $table->unsignedInteger('transaction_id')->index();
            $table->string('order_status');
            $table->date('refund_date');
            $table->decimal('refund_amount', 15, 2);
            $table->decimal('net_order_revenue', 15, 2);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::create('order_transactions', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('transaction_id')->index();
            $table->unsignedInteger('uid')->index();
            $table->unsignedInteger('order_id')->index();
            $table->string('payment_method')->index();
            $table->string('instance_id');
            $table->string('remote_id')->index();
            $table->string('message');
            $table->integer('amount');
            $table->string('currency_code');
            $table->string('status');
            $table->string('remote_status');
            $table->longText('payload');
            $table->string('drupal_created');
            $table->string('drupal_changed');
            $table->longText('data');
            $table->timestamps();
        });

        Schema::table('rpt_order_transactions', function (Blueprint $table) {
            $table->dropColumn('transaction_id');
            $table->dropColumn('order_status');
            $table->dropColumn('refund_date');
            $table->dropColumn('refund_amount');
            $table->dropColumn('net_order_revenue');
        });

        Schema::rename('rpt_order_transactions', 'rpt_paid_transactions');
    }
}
