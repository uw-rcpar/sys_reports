<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAffirmTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rpt_affirm_transactions', function (Blueprint $table) {
            $table->increments('id');
            $table->date('date');
            $table->date('charge_created_date');
            $table->string('charge_id');
            $table->string('transaction_id');
            $table->unsignedInteger('order_id')->index();
            $table->string('event_type');
            $table->float('sales');
            $table->float('refunds');
            $table->float('fees');
            $table->float('total_settled');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rpt_affirm_transactions');
    }
}
