<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class HhcPerformanceNullableCols extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('rpt_hhc_performance', function (Blueprint $table) {
            $table->string('section_chapter')->nullable()->change();
            $table->string('section')->nullable()->change();
            $table->unsignedInteger('cid')->nullable()->change();
            $table->date('answer_date')->nullable()->change();
            $table->string('thread')->nullable()->change();
            $table->string('timediff')->nullable()->change();
            $table->longText('comment_body_value')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('rpt_hhc_performance', function (Blueprint $table) {
            $table->string('section_chapter')->nullable(false)->change();
            $table->string('section')->nullable(false)->change();
            $table->unsignedInteger('cid')->nullable(false)->change();
            $table->date('answer_date')->nullable(false)->change();
            $table->string('thread')->nullable(false)->change();
            $table->string('timediff')->nullable(false)->change();
            $table->longText('comment_body_value')->nullable(false)->change();
        });
    }
}
