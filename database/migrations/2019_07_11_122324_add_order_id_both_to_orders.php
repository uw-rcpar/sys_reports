<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddOrderIdBothToOrders extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up() {
    Schema::table('rpt_order_transactions', function (Blueprint $table) {
      $table->string('order_id_both')->nullable();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down() {
    Schema::table('rpt_order_transactions', function (Blueprint $table) {
      $table->dropColumn('order_id_both');
    });
  }
}
