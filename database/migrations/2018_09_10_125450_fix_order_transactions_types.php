<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FixOrderTransactionsTypes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('rpt_order_transactions', function (Blueprint $table) {
          $table->unsignedInteger('college_state_tid')->nullable()->change();
          $table->string('college_state', 200)->nullable()->change();
          $table->string('zip')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('rpt_order_transactions', function (Blueprint $table) {
          $table->string('college_state_tid')->nullable()->change();
          $table->integer('college_state')->nullable()->change();
          $table->integer('zip')->nullable()->change();
        });
    }
}
