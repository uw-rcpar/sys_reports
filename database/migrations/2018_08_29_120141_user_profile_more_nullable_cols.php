<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UserProfileMoreNullableCols extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('rpt_user_profile', function (Blueprint $table) {
            $table->date('aud_exam_date')->nullable()->change();
            $table->date('bec_exam_date')->nullable()->change();
            $table->date('far_exam_date')->nullable()->change();
            $table->date('reg_exam_date')->nullable()->change();

            $table->string('first_name')->nullable()->change();
            $table->string('last_name')->nullable()->change();
            $table->string('legacy_id')->nullable()->change();
            $table->string('roles')->nullable()->change();
            $table->string('phone')->nullable()->change();
            $table->text('courses_taught')->nullable()->change();
            $table->date('first_exam_sit')->nullable()->change();
            $table->string('user_profile_country')->nullable()->change();
            $table->string('user_profile_state')->nullable()->change();
            $table->string('campus')->nullable()->change();
            $table->string('billing_premise')->nullable()->change();

            // tinyInteger type give problems here, however boolean type is equivalent
            // on mysql
            // https://github.com/laravel/framework/issues/8840
            $table->boolean('archive_synced')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('rpt_user_profile', function (Blueprint $table) {
            $table->date('aud_exam_date')->nullable(false)->change();
            $table->date('bec_exam_date')->nullable(false)->change();
            $table->date('far_exam_date')->nullable(false)->change();
            $table->date('reg_exam_date')->nullable(false)->change();

            $table->string('first_name')->nullable(false)->change();
            $table->string('last_name')->nullable(false)->change();
            $table->string('legacy_id')->nullable(false)->change();
            $table->string('roles')->nullable(false)->change();
            $table->string('phone')->nullable(false)->change();
            $table->text('courses_taught')->nullable(false)->change();
            $table->date('first_exam_sit')->nullable(false)->change();
            $table->string('user_profile_country')->nullable(false)->change();
            $table->string('user_profile_state')->nullable(false)->change();
            $table->string('campus')->nullable(false)->change();
            $table->string('billing_premise')->nullable(false)->change();

            $table->boolean('archive_synced')->nullable(false)->change();
        });
    }
}
