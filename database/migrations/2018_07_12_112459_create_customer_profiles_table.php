<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomerProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customer_profiles', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('uid')->index();
            $table->string('country')->index();
            $table->string('administrative_area');
            $table->string('sub_administrative_area');
            $table->string('locality');
            $table->string('dependent_locality');
            $table->string('postal_code');
            $table->string('thoroughfare');
            $table->string('premise');
            $table->string('sub_premise');
            $table->string('organisation_name');
            $table->string('name_line');
            $table->string('address_first_name');
            $table->string('address_last_name');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customer_profiles');
    }
}
