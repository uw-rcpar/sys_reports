<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStatsIPQChaptersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rpt_stats_ipq_chapters', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('chapter_id')->index();
            $table->decimal('trending_average', 25, 4);
            $table->decimal('average', 25, 4);
            $table->integer('total_questions')->index();
            $table->integer('distinct_questions');
            $table->decimal('sd', 25, 4);
            $table->string('groupname');
            $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rpt_stats_ipq_chapters');
    }
}
