<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserEntitlementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rpt_user_entitlements', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();

            $table->unsignedInteger('nid');
            $table->unsignedInteger('uid')->index();
            $table->unsignedInteger('order_id')->nullable()->index();
            $table->date('created_date')->nullable();
            $table->date('expiration_date')->nullable();
            $table->date('orig_expiration_date')->nullable();
            $table->tinyInteger('status');
            $table->string('bundle')->nullable();
            $table->string('sku')->nullable();
            $table->unsignedInteger('product_type_tid');
            $table->string('course_type');
            $table->unsignedInteger('course_section_tid')->nullable();
            $table->string('course_section')->nullable();
            $table->unsignedInteger('default_duration')->nullable();
            $table->string('default_interval')->nullable();
            $table->unsignedInteger('extended_duration')->nullable();
            $table->string('extended_interval')->nullable();
            $table->string('content_restricted')->nullable();
            $table->unsignedInteger('unlimited_access')->nullable();
            $table->unsignedInteger('mobile_offline_access')->nullable();
            $table->string('hhc_access')->nullable();
            $table->tinyInteger('cram_active');
            $table->tinyInteger('ipq_access')->nullable();
            $table->unsignedInteger('partner_id')->nullable();
            $table->string('exam_version')->nullable();
            $table->unsignedInteger('activation_delayed')->nullable();
            $table->string('serial_number', 55)->nullable();
            $table->unsignedInteger('allowed_installs')->nullable();
            $table->string('install_dates')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rpt_user_entitlements');
    }
}
