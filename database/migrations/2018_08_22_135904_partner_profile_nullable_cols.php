<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PartnerProfileNullableCols extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('rpt_partner_profile', function (Blueprint $table) {
            $table->integer('account_id')->nullable()->change();
            $table->string('account_name')->nullable()->change();
            $table->string('abbreviated_name')->nullable()->change();
            $table->string('intacct_cid')->nullable()->change();
            $table->string('partner_type')->nullable()->change();
            $table->string('billing_type')->nullable()->change();
            $table->integer('account_executive_uid')->nullable()->change();
            $table->longText('description')->nullable()->change();
            $table->longText('partner_terms')->nullable()->change();
            $table->boolean('act_access')->nullable()->change();
            $table->boolean('monitoring_access')->nullable()->change();
            $table->integer('monitoring_uid')->nullable()->change();
            $table->integer('archive_pid')->nullable()->change();
            $table->string('fixed_expiration')->nullable()->change();
            $table->decimal('general_discount', 25, 4)->nullable()->change();
            $table->decimal('aud_discount', 25, 4)->nullable()->change();
            $table->decimal('bec_discount', 25, 4)->nullable()->change();
            $table->decimal('far_discount', 25, 4)->nullable()->change();
            $table->decimal('reg_discount', 25, 4)->nullable()->change();
            $table->decimal('bundled_price', 25, 4)->nullable()->change();
            $table->boolean('email_validation')->nullable()->change();
            $table->boolean('exempt_sales_tax')->nullable()->change();
            $table->boolean('flatrate_shipping')->nullable()->change();
            $table->decimal('shipping_rate', 25, 4)->nullable()->change();
            $table->decimal('partial_shipping_rate', 25, 4)->nullable()->change();
            $table->boolean('international')->nullable()->change();
            $table->boolean('show_price')->nullable()->change();
            $table->boolean('show_retail_price')->nullable()->change();
            $table->string('shipping_flow')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('rpt_partner_profile', function (Blueprint $table) {
            $table->integer('account_id')->nullable(false)->change();
            $table->string('account_name')->nullable(false)->change();
            $table->string('abbreviated_name')->nullable(false)->change();
            $table->string('intacct_cid')->nullable(false)->change();
            $table->string('partner_type')->nullable(false)->change();
            $table->string('billing_type')->nullable(false)->change();
            $table->integer('account_executive_uid')->nullable(false)->change();
            $table->longText('description')->nullable(false)->change();
            $table->longText('partner_terms')->nullable(false)->change();
            $table->boolean('act_access')->nullable(false)->change();
            $table->boolean('monitoring_access')->nullable(false)->change();
            $table->integer('monitoring_uid')->nullable(false)->change();
            $table->integer('archive_pid')->nullable(false)->change();
            $table->string('fixed_expiration')->nullable(false)->change();
            $table->decimal('general_discount', 25, 4)->nullable(false)->change();
            $table->decimal('aud_discount', 25, 4)->nullable(false)->change();
            $table->decimal('bec_discount', 25, 4)->nullable(false)->change();
            $table->decimal('far_discount', 25, 4)->nullable(false)->change();
            $table->decimal('reg_discount', 25, 4)->nullable(false)->change();
            $table->decimal('bundled_price', 25, 4)->nullable(false)->change();
            $table->boolean('email_validation')->nullable(false)->change();
            $table->boolean('exempt_sales_tax')->nullable(false)->change();
            $table->boolean('flatrate_shipping')->nullable(false)->change();
            $table->decimal('shipping_rate', 25, 4)->nullable(false)->change();
            $table->decimal('partial_shipping_rate', 25, 4)->nullable(false)->change();
            $table->boolean('international')->nullable(false)->change();
            $table->boolean('show_price')->nullable(false)->change();
            $table->boolean('show_retail_price')->nullable(false)->change();
            $table->string('shipping_flow')->nullable(false)->change();
        });
    }
}
