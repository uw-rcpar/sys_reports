<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class OrdersNullableCols extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('rpt_orders', function (Blueprint $table) {
            $table->unsignedInteger('partner_id')->nullable()->change();
            $table->integer('shipping_total')->nullable()->change();
            $table->integer('tax_total')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('rpt_orders', function (Blueprint $table) {
            $table->unsignedInteger('partner_id')->nullable(false)->change();
            $table->integer('shipping_total')->nullable(false)->change();
            $table->integer('tax_total')->nullable(false)->change();
        });
    }
}
