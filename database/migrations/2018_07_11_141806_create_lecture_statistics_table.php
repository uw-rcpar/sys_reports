<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLectureStatisticsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rpt_lecture_statistics', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('uid')->index();
            $table->string('section')->index();
            $table->unsignedInteger('videos_watched');
            $table->unsignedInteger('videos_completed');
            $table->unsignedInteger('notes');
            $table->unsignedInteger('bookmarks');
            $table->unsignedInteger('highlights');
            $table->string('group')->index();
            $table->float('avg_perc_complete_finished');
            $table->float('avg_total_duration');
            $table->date('first_date');
            $table->date('last_date');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lecture_statistics');
    }
}
