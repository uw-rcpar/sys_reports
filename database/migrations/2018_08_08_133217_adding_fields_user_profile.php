<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddingFieldsUserProfile extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('rpt_user_profile', function (Blueprint $table) {
            $table->renameColumn('address_first_name', 'first_name');
            $table->renameColumn('address_last_name', 'last_name');

            $table->string('roles')->after('uid');
            $table->string('phone')->after('uid');
            $table->text('courses_taught')->after('uid');
            $table->tinyInteger('did_graduate_college')->after('uid');
            $table->string('college_name')->after('uid');

            $table->string('legacy_id')->after('uid');
            $table->tinyInteger('status')->after('uid');
            $table->string('username')->after('uid');

            $table->date('when_plan_to_first_sit_for_exam');

            $table->renameColumn('country', 'customer_address_country');
            $table->renameColumn('administrative_area', 'customer_address_administrative_area');

            $table->string('user_profile_country');
            $table->string('user_profile_state');
            $table->string('campus');
            $table->date('aud_exam_date');
            $table->date('bec_exam_date');
            $table->date('far_exam_date');
            $table->date('reg_exam_date');
            $table->tinyInteger('archive_synced');
            $table->string('sync_stage');

            $table->dropColumn('name_line');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('rpt_user_profile', function (Blueprint $table) {
            $table->renameColumn('first_name', 'address_first_name');
            $table->renameColumn('last_name', 'address_last_name');

            $table->dropColumn('roles');
            $table->dropColumn('phone');
            $table->dropColumn('courses_taught');
            $table->dropColumn('did_graduate_college');
            $table->dropColumn('college_name');

            $table->dropColumn('legacy_id');
            $table->dropColumn('status');
            $table->dropColumn('username');

            $table->dropColumn('when_plan_to_first_sit_for_exam');

            $table->renameColumn('customer_address_country', 'country');
            $table->renameColumn('customer_address_administrative_area', 'administrative_area');

            $table->dropColumn('user_profile_country');
            $table->dropColumn('user_profile_state');
            $table->dropColumn('campus');
            $table->dropColumn('aud_exam_date');
            $table->dropColumn('bec_exam_date');
            $table->dropColumn('far_exam_date');
            $table->dropColumn('reg_exam_date');
            $table->dropColumn('archive_synced');
            $table->dropColumn('sync_stage');

            $table->string('name_line');
        });
    }
}
