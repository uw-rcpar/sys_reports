<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderLinesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_lines', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('drupal_order_id')->index();
            $table->string('type');
            $table->unsignedInteger('drupal_line_item_id');
            $table->string('label');
            $table->string('product_title');
            $table->string('product_type');
            $table->unsignedInteger('quantity');
            $table->unsignedInteger('drupal_created')->index();
            $table->unsignedInteger('drupal_changed')->index();
            $table->string('tax_rate');
            $table->string('currency_code');
            $table->float('taxable');
            $table->float('taxed');
            $table->float('total');
            $table->float('total_no_tax');
            $table->float('shipping_part');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_lines');
    }
}
