<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UwOrderTransactionsNewFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('rpt_uw_order_transactions', function (Blueprint $table) {
            //
            $table->renameColumn('partner_title','partner_name');
            $table->smallInteger('response_code')->change();
            $table->smallInteger('purchase_type')->change();
            $table->integer('sf_id_school_id')->change();
            $table->string('transaction_id', 20)->nullable();
            $table->dateTime('date_payment_received')->nullable();
            $table->smallInteger('transaction_type_id')->nullable();
            $table->integer('sales_user_id')->nullable();
            $table->smallInteger('invoice_type')->nullable();
            $table->integer('internal_sales_user_id')->nullable();
            $table->string('invoice_number',20)->nullable();
            $table->string('invoice_net_duration',10)->nullable();
            $table->smallInteger('invoice_category')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('uw_order_transactions', function (Blueprint $table) {
            //
        });
    }
}
