<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RptOrderTransactions extends Migration
{
    /**
     * Run the migrations.
     *uw_sf_id
     * @return void
     */
    public function up()
    {
      Schema::table('rpt_order_transactions', function (Blueprint $table) {
        $table->string('uw_sf_id')->nullable();
        $table->string('uw_partner_name')->nullable();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('rpt_order_transactions', function (Blueprint $table) {
        $table->dropColumn('uw_sf_id');
        $table->dropColumn('uw_partner_name');
      });
    }
}
