<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUwOrderIdToOrders extends Migration {
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up() {
    Schema::table('rpt_order_transactions', function (Blueprint $table) {
      $table->string('uw_order_id')->nullable();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down() {
    Schema::table('rpt_order_transactions', function (Blueprint $table) {
      $table->dropColumn('uw_order_id');
    });
  }
}
