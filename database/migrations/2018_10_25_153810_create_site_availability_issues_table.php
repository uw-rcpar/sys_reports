<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSiteAvailabilityIssuesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rpt_site_availability_issues', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string('monitor');
            $table->dateTime('start_time');
            $table->dateTime('end_time');
            $table->string('duration');
            $table->string('comments');
            $table->index(['start_time', 'end_time']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rpt_site_availability_issues');
    }
}
