<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUwUiToUserProfile extends Migration {
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up() {
    Schema::table('rpt_user_profile', function (Blueprint $table) {
      $table->string('uw_uid')->nullable();

    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down() {
    Schema::table('rpt_user_profile', function (Blueprint $table) {
      $table->dropColumn('uw_uid');
    });
  }
}
