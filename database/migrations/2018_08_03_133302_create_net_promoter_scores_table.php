<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNetPromoterScoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rpt_nps', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('uid');
            $table->dateTime('create_date');
            $table->tinyInteger('score');
            $table->text('comment');
            $table->string('purchaser_type');
            $table->string('skus');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rpt_nps');
    }
}
