<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemovingAvgPercCompleteFinishedFromLectureStats extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('rpt_lecture_statistics', function (Blueprint $table) {
            $table->dropColumn('avg_perc_complete_finished');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('rpt_lecture_statistics', function (Blueprint $table) {
            $table->float('avg_perc_complete_finished')->nullable();
        });
    }
}
