<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UwOrderTransactionsNewFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('rpt_uw_order_transactions', function (Blueprint $table) {
            //
            $table->string('response_code')->nullable();
            $table->string('school_name')->nullable();
            $table->string('purchase_type')->nullable();
            $table->string('sf_id_school_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('rpt_uw_order_transactions', function (Blueprint $table) {
          $table->dropColumn('sf_id_school_id');
          $table->dropColumn('purchase_type');
          $table->dropColumn('school_name');
          $table->dropColumn('response_code');
        });
    }
}
