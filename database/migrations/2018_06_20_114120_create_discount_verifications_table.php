<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDiscountVerificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rpt_discount_verifications', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('uid')->index();
            $table->string('coupon_code');
            $table->string('discount_type');
            $table->unsignedInteger('uploaded_fid');
            $table->string('previous_course');
            $table->unsignedInteger('status');
            $table->unsignedInteger('used');
            $table->dateTime('drupal_created_at');
            $table->timestamps();

            $table->index(['uid', 'drupal_created_at']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rpt_discount_verifications');
    }
}
