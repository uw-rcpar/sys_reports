<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAccountColsToRptOrderTransactions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('rpt_order_transactions', function (Blueprint $table) {
            $table->unsignedInteger('account_id');
            $table->string('account_name');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('rpt_order_transactions', function (Blueprint $table) {
            $table->dropColumn('account_id');
            $table->dropColumn('account_name');
        });
    }
}
