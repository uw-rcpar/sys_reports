<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRptPaidTransactionsTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('rpt_paid_transactions', function (Blueprint $table) {
      $table->increments('id')->index();
      $table->unsignedInteger('user_id')->index();
      $table->string('mail',254);
      $table->unsignedInteger('order_id')->index();
      $table->date('create_date')->index();
      $table->integer('create_year');
      $table->integer('create_month');
      $table->integer('create_day');
      $table->string('graduated', 13);
      $table->date('grad_date');
      $table->integer('grad_year');
      $table->integer('grad_month');
      $table->string('country', 44);
      $table->string('state', 14);
      $table->string('st', 4);
      $table->string('market', 50);
      $table->string('region', 50);
      $table->string('zip', 50);
      $table->unsignedInteger('partner_nid');
      $table->string('partner_title', 255);
      $table->string('intacct', 255);
      $table->unsignedInteger('college_state_tid');
      $table->string('college_name', 200);
      $table->string('college_state_abbrv', 10);
      $table->string('college_state', 200);
      $table->string('promotion_type', 255);
      $table->string('promotion', 255);
      $table->longText('promo_description');
      $table->string('free_trial', 11);
      $table->bigInteger('ft_time_diff');
      $table->string('ft_time_group', 10);
      $table->string('channel', 50);
      $table->string('channel3', 50);
      $table->string('lead_status_all', 50);
      $table->string('package', 25);
      $table->string('payment_method', 25);
      $table->decimal('sub_total', 15, 2);
      $table->decimal('promo_total', 15, 2);
      $table->decimal('partner_discount_total', 15, 2);
      $table->decimal('shipping', 15, 2);
      $table->decimal('taxes', 15, 2);
      $table->decimal('order_total', 15, 2);
      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('rpt_paid_transactions');
  }
}
